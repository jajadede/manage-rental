package com.jajadede.manage.rental.controller;

import com.jajadede.manage.rental.service.AddressService;
import com.jajadede.manage.rental.service.LeaseService;
import com.jajadede.manage.rental.service.PeopleService;
import com.jajadede.manage.rental.service.PropertyService;
import com.jajadede.manage.rental.service.SummaryService;
import com.jajadede.manage.rental.validator.SummaryChoiceValidator;
import com.jajadede.manage.rental.wrapper.SummaryChoice;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class HomeController {

    @Autowired
    private PropertyService propertyService;
    @Autowired
    private AddressService addressService;
    @Autowired
    private PeopleService peopleService;
    @Autowired
    private LeaseService leaseService;
    @Autowired
    private SummaryService summaryService;
    @Autowired
    private SummaryChoiceValidator summaryChoiceValidator;
    
    @RequestMapping("/")
    public String welcome(Model model) {
        loadStatistics(model);
        model.addAttribute("summaryChoice", new SummaryChoice());        
        return "welcome";
    }
    
    @RequestMapping(value = "/summary/findByYearMonth", method = RequestMethod.POST)
    public String findByYearMonth(
            @ModelAttribute("summaryChoice") @Valid SummaryChoice summaryChoice, 
            BindingResult result,
            Model model) {
        if (result.hasErrors()) {
            loadStatistics(model);
            model.addAttribute("summaryChoice", summaryChoice);
            return "welcome";
        }
        List<Integer> ym = Arrays.asList(summaryChoice.getYearMonth().split("-")).
                stream().map(s -> {return (Integer) Integer.parseInt(s);}).collect(Collectors.toList());
        model.addAttribute("rentalActivityList", summaryService.findByYearMonth(ym.get(0),ym.get(1)) );
        loadStatistics(model);
        model.addAttribute("summaryChoice", summaryChoice);
        return "welcome";
    }
    
    private void loadStatistics(Model model) {
        Long numProperties = propertyService.countAll();
        Long numLonelyAddress = addressService.countLonely();
        Long numPeople = peopleService.countAll();
        Long numLease = leaseService.countAll();
        Long numAddressWithNoProperty = addressService.countAllWithNoProperty();
        Long numAddressWithNoPeople = addressService.countAllWithNoPeople();
        Long numPeopleWithNoActiveLease = peopleService.countAllWithNoActiveLease();
        Long numPropertyWithNoActiveLease = propertyService.countAllWithNoActiveLease();
        model.addAttribute("numProperties", numProperties);
        model.addAttribute("numLonelyAddress", numLonelyAddress);
        model.addAttribute("numPeople", numPeople);
        model.addAttribute("numLease", numLease);
        model.addAttribute("numPeopleWithNoActiveLease", numPeopleWithNoActiveLease);
        model.addAttribute("numPropertyWithNoActiveLease", numPropertyWithNoActiveLease);
        model.addAttribute("numAddressWithNoProperty", numAddressWithNoProperty);
        model.addAttribute("numAddressWithNoPeople", numAddressWithNoPeople);
        model.addAttribute("yearMonthList", summaryService.findYearMonthList(0, true));
    }

    @InitBinder
    public void initialseBinder(WebDataBinder binder) {
        binder.addValidators(summaryChoiceValidator);
        binder.setAllowedFields( //TODO complate the white list of fileds
                "*"
        );
    }    
}
