/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.service.impl;

import com.jajadede.manage.rental.domain.Event;
import com.jajadede.manage.rental.domain.Lease;
import com.jajadede.manage.rental.domain.LeaseStatus;
import com.jajadede.manage.rental.domain.TransactionPurpose;
import com.jajadede.manage.rental.domain.repository.LeaseRepository;
import com.jajadede.manage.rental.service.LeaseService;
import java.math.BigDecimal;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Jerry
 */
@Service ("leaseService")
public class LeaseServiceImpl implements LeaseService {
    private static final Logger LOG = LogManager.getLogger(LeaseServiceImpl.class.getName());
    
    @Autowired
    private LeaseRepository leaseRepository;
     
    @Override
    public void update(Lease lease) {
        if (lease.getEvents() != null) lease.getEvents().forEach(e -> {
            e.setLease(lease);
            applyBusinessLogicForAdd(e);
        });
        leaseRepository.update(lease);
    }

    @Override
    public List<Lease> findAll() {
        return leaseRepository.findAll();
    }

    @Override
    public Lease findById(Long id) {
        return leaseRepository.findById(id);
    }

    @Override
    public Lease create(Lease lease) {
        if (lease.getEvents() != null) lease.getEvents().forEach(e -> {
            e.setLease(lease);
            applyBusinessLogicForAdd(e);
        });
        leaseRepository.create(lease);      
        return lease;
    }

    @Override
    public void delete(Lease lease) {
        leaseRepository.delete(lease);
    }

    @Override
    public Long countAll() {
        return leaseRepository.countAll();
    }

    @Override
    public void deleteEventById(Long eventId) {
        deleteEvent(leaseRepository.findEventById(eventId));        
    }

    @Override
    public void deleteEvent(Event event) {
        applyBusinessLogicForDelete(event);        
        leaseRepository.deleteEvent(event);
    }
    
    @Override
    public void addEvent(Event event) {
        applyBusinessLogicForAdd(event);
        leaseRepository.addEvent(event);
    }
  
    /**
     * Calculates the default value for transaction obj in the event
     * 
     * @param event the event to calculate
     */
    private void applyBusinessLogicForAdd(Event event) {
        if (event.getLease() == null) {
            LOG.error("event.getLease is null");
            return;
        }        
        if (event.getTransaction().getEffectivedate() == null) {
            event.getTransaction().setEffectivedate(event.getActiondate());
        }        
        switch (event.getAction()) {
            case PAY_RENT:
            case MOVE_IN:
                event.getTransaction().setPurpose(TransactionPurpose.RENT);
                break;
            case RESERVE:
                event.getTransaction().setPurpose(TransactionPurpose.DEPOSIT);
                break;
            case MOVE_OUT:               
            case CANCEL_RESERVATION:
                event.getTransaction().setPurpose(TransactionPurpose.REFUND);
                event.getLease().setStatus(LeaseStatus.INACTIVE);
                if (event.getTransaction() != null) {
                    BigDecimal amount = event.getTransaction().getAmount();
                    if (amount == null) event.getTransaction().setAmount(BigDecimal.ZERO);
                    else if (amount.compareTo(BigDecimal.ZERO) > 0) event.getTransaction().setAmount(amount.negate());
                }
                break;
            default:
                LOG.error("Action is invalid: " + event.getAction());
        }
    }
    
    private void applyBusinessLogicForDelete(Event event) {
        if (event.getTransaction().getEffectivedate() == null) {
            event.getTransaction().setEffectivedate(event.getActiondate());
        } 
        switch (event.getAction()) {
            case PAY_RENT:;
            case MOVE_IN:
            case RESERVE:             
                break;
            case MOVE_OUT:
            case CANCEL_RESERVATION:
                event.getLease().setStatus(LeaseStatus.ACTIVE);
                break;
            default:
                LOG.error("Action is invalid: " + event.getAction());
        }
    }
}
