package com.jajadede.manage.rental.validator;

import com.jajadede.manage.rental.domain.Action;
import com.jajadede.manage.rental.domain.Event;
import com.jajadede.manage.rental.domain.Lease;
import com.jajadede.manage.rental.service.AddressService;
import com.jajadede.manage.rental.service.LeaseService;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author Jerry
 */
@Component
public class LeaseValidator implements Validator {

    private static final Logger LOG = LogManager.getLogger(LeaseValidator.class.getName());

    @Autowired
    AddressService addressService;
    @Autowired
    LeaseService leaseService;

    @Override
    public boolean supports(Class<?> clazz) {
        return Lease.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Lease lease = (Lease) target;

        // common check
        validateEvents(lease, errors);
        validateDates(lease, errors);
        validateTenants(lease, errors);
        validateProperty(lease, errors);
        // check against db
    }

    private void validateEvents(Lease lease, Errors errors) {
        List<Event> events = lease.getEvents();
        if ((events.size() >= 1) && (events.get(0).getAction() != Action.RESERVE)) {
            errors.rejectValue(
                    "events[0].action",
                    "validator.LeaseValidator.events.ReserveFirst.message");
            LOG.debug("Error found, validator.LeaseValidator.events.ReserveFirst.message is set.");
        }

        if (events.size() >= 3) {
            for (int i = 1; i < events.size() - 2; i++) {
                if ((events.get(i).getAction() == Action.MOVE_OUT)
                        || (events.get(i).getAction() == Action.CANCEL_RESERVATION)) {
                    errors.rejectValue(
                            "events[" + (events.size() - 1) + "].action",
                            "validator.LeaseValidator.events.MoveoutAndCancelPostionInvalid.message");
                    LOG.debug("Error found, validator.LeaseValidator.events.MoveoutAndCancelPostionInvalid.message is set.");
                }
            }
        }
        
        if (events.size() >=2 && events.stream().filter(e -> {return e.getAction()==Action.RESERVE;}).collect(Collectors.toList()).size() > 1  ) {
            errors.rejectValue(
                    "events",
                    "validator.LeaseValidator.events.reserve.NoDupAllowed.message");
            LOG.debug("Error found, validator.LeaseValidator.events.reserve.NoDupAllowed.message is set.");            
        } 
        
        if (events.size() >=2 && events.stream().filter(e -> {return e.getAction()==Action.CANCEL_RESERVATION;}).collect(Collectors.toList()).size() > 1  ) {
            errors.rejectValue(
                    "events",
                    "validator.LeaseValidator.events.cancel.NoDupAllowed.message");
            LOG.debug("Error found, validator.LeaseValidator.events.cancel.NoDupAllowed.message is set.");            
        }    
        
        if (events.size() >=2 && events.stream().filter(e -> {return e.getAction()==Action.MOVE_OUT;}).collect(Collectors.toList()).size() > 1  ) {
            errors.rejectValue(
                    "events",
                    "validator.LeaseValidator.events.moveout.NoDupAllowed.message");
            LOG.debug("Error found, validator.LeaseValidator.events.moveout.NoDupAllowed.message is set.");
        }         
        
        if (events.stream().map(Event::getActiondate).collect(Collectors.toSet()).size() < events.size() ) {
            errors.rejectValue(
                    "events",
                    "validator.LeaseValidator.events.actiondate.NoDupAllowed.message");
            LOG.debug("Error found, validator.LeaseValidator.events.actiondate.NoDupAllowed.message is set.");            
        }        
    }

    private void validateDates(Lease lease, Errors errors) {
        if (lease.getEffectivedate().after(lease.getEnddate())) {
                    errors.rejectValue(
                            "effectivedate",
                            "validator.LeaseValidator.events.EffectiveDateShouldBeEarlier.message");
                    LOG.debug("Error found, validator.LeaseValidator.events.EffectiveDateShouldBeEarlier.message is set.");            
        }
    }   

    private void validateTenants(Lease lease, Errors errors) {
        if (!lease.getTenants().stream().anyMatch(t -> t.getId() != null)) {
                    errors.rejectValue(
                            "",
                            "validator.LeaseValidator.tenants.NotEmpty.message");
                    LOG.debug("Error found, validator.LeaseValidator.tenants.NotEmpty.message is set.");            
        }
    } 
    
    private void validateProperty(Lease lease, Errors errors) {
        if (lease.getProperty().getId() == null) {
                    errors.rejectValue(
                            "property",
                            "validator.LeaseValidator.property.NotEmpty.message");
                    LOG.debug("Error found, validator.LeaseValidator.property.NotEmpty.message is set.");            
        }
    }    
}
