package com.jajadede.manage.rental.controller;

import com.jajadede.manage.rental.domain.Action;
import com.jajadede.manage.rental.domain.Event;
import com.jajadede.manage.rental.domain.Lease;
import com.jajadede.manage.rental.domain.LeaseStatus;
import com.jajadede.manage.rental.domain.People;
import com.jajadede.manage.rental.domain.Property;
import com.jajadede.manage.rental.domain.Transaction;
import com.jajadede.manage.rental.domain.TransactionMethod;
import com.jajadede.manage.rental.domain.TransactionPurpose;
import com.jajadede.manage.rental.service.AddressService;
import com.jajadede.manage.rental.service.LeaseService;
import com.jajadede.manage.rental.service.PeopleService;
import com.jajadede.manage.rental.service.PropertyService;
import com.jajadede.manage.rental.validator.LeaseValidator;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LeaseController {

    private static final Logger LOG = LogManager.getLogger(LeaseController.class.getName());

    @Autowired
    private LeaseService leaseService;    
    @Autowired
    private AddressService addressService;
    @Autowired
    private PropertyService propertyService;
    @Autowired
    private PeopleService peopleService;    

    @Autowired
    private LeaseValidator leaseValidator;
    
    @RequestMapping("/lease/list")
    public String list(Model model) {
        model.addAttribute("leases", leaseService.findAll());
        return "leases";
    }

    @RequestMapping(value = "/lease/add", method = RequestMethod.GET)
    public String addFormGet(Model model) {
        model.addAttribute("lease", createNewLease());
        setAddPageAttributes(model);
        return "leaseAdd";
    }

    @RequestMapping(value = "/lease/add", method = RequestMethod.POST)
    public String addFormProcess(
            @ModelAttribute("lease") @Valid Lease lease,
            BindingResult result,
            Model model) {
        LOG.debug("new lease got from request: " + lease);
        if (result.hasErrors()) {
            model.addAttribute("lease", lease);
            setAddPageAttributes(model);            
            return "leaseAdd";
        }
        try {
            leaseService.create(lease);
            model.addAttribute("createSuccess", "");
        }
        catch (Exception e) {
            LOG.error(e);
            model.addAttribute("createError", e.getMessage());
        }           
        return list(model);
    }

    @RequestMapping(value = "/lease/edit", method = RequestMethod.GET)
    public String editFormGet(@RequestParam("id") Long leaseId, Model model) {
        Lease lease = leaseService.findById(leaseId);
        model.addAttribute("lease", lease);
        setAddPageAttributes(model);
        setEditPageExclusiveAttributes(model, lease);
        return "lease";
    }
    
    @RequestMapping(value = "/lease/edit", method = RequestMethod.POST)
    public String editFormProcess(
            @ModelAttribute("lease") @Valid Lease lease,              
            BindingResult result, 
            Model model) {
        if (result.hasErrors()) {
            model.addAttribute("lease", lease);
            setAddPageAttributes(model);
            setEditPageExclusiveAttributes(model, lease);
            return "lease";
        }
        try {
            leaseService.update(lease);
            model.addAttribute("updateSuccess", "");
        }
        catch (Exception e) {
            LOG.error(e);
            model.addAttribute("updateError", e.getMessage());
        }
        return list(model);
    }
    
    @RequestMapping(value = "/lease/delete", method = RequestMethod.GET)
    public String deleteById(@RequestParam("id") Long leaseId, Model model) {
        try {
            Lease lease = new Lease(leaseId, 4);
            leaseService.delete(lease);
            model.addAttribute("deleteSuccess", "");
        }
        catch (Exception e) {
            LOG.error(e);
            model.addAttribute("deleteError", e.getMessage());
        }
        return list(model);
    }     

    @InitBinder
    public void initialseBinder(WebDataBinder binder) {
        binder.addValidators(leaseValidator);
        binder.setAllowedFields( //TODO complate the white list of fileds
                "*"
        );
    }
    
    private Lease createNewLease() {
        Lease lease = new Lease();
        Event event = new Event();
        event.setActiondate(new Date());
        event.setAction(Action.RESERVE);
        event.addTransaction(new Transaction());
        lease.addEvent(event);
        lease.getTenants().add(new People());
        lease.getTenants().add(new People());
        return lease;
    }
    
    private void setAddPageAttributes(Model model) {
        model.addAttribute("properties", propertyService.findAllWithNoActiveLease());
        model.addAttribute("peoples", peopleService.findAllWithNoActiveLease());
        model.addAttribute("actions", Action.values());        
        model.addAttribute("methods", TransactionMethod.values());
        model.addAttribute("purposes", TransactionPurpose.values());
        model.addAttribute("statuses", LeaseStatus.values());
    }
    
    private void setEditPageExclusiveAttributes(Model model, Lease lease) {
        // need to reload editing lesae from db, otherwise only the property id and tenants id are sent over from the edit page
        Lease dblease = leaseService.findById(lease.getId());
        List<Property> properties = (List<Property>) model.asMap().get("properties");
        properties.add(dblease.getProperty());
        List<People> peoples = (List<People>) model.asMap().get("peoples");
        peoples.addAll(dblease.getTenants());
    }
//    @ModelAttribute("actions")
//    public List<Action> populateActions()
//    {
//        return Arrays.asList(Action.values());
//    }    
//    
//    @ModelAttribute("methods")
//    public TransactionMethod[] populateMethods()
//    {
//        return TransactionMethod.values();
//    } 
//
//    @ModelAttribute("purposes")
//    public TransactionPurpose[] populatePurposes()
//    {
//        return TransactionPurpose.values();
//    }     
}
