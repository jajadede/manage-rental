/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.service.impl;

import com.jajadede.manage.rental.domain.Address;
import com.jajadede.manage.rental.domain.Property;
import com.jajadede.manage.rental.domain.Rent;
import com.jajadede.manage.rental.domain.repository.AddressRepository;
import com.jajadede.manage.rental.domain.repository.PropertyRepository;
import com.jajadede.manage.rental.domain.repository.RentRepository;
import com.jajadede.manage.rental.exception.DeletingLastRentException;
import com.jajadede.manage.rental.service.PropertyService;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Jerry
 */
@Service ("propertyService")
public class PropertyServiceImpl implements PropertyService {
    private static final Logger LOG = LogManager.getLogger(PropertyServiceImpl.class.getName());
    
    @Autowired
    private PropertyRepository propertyRepository;
    
    @Autowired
    private AddressRepository addressRepository;
    
    @Autowired
    private RentRepository rentRepository;    
    
    @Override
    public void update(Property property) {
        try {
            LOG.info("property to update: " + property);
            Address address = property.getAddress();
            if (address == null || address.getId() == null) {
                LOG.error("Nothing done. Try to update property with null address or null address id, address=" + address);
                return;
            }
            Address existingAddress = addressRepository.findById(address.getId());
            if (existingAddress == null) {
                LOG.error("Nothing done. Try to update property with an non-existing address, address=" + address);
                return;
            }

            // transfer changes to the db property obj
            Property propertyExist = propertyRepository.findById(property.getId());
            LOG.debug("property exist found in db: " + propertyExist);
            property.transferChangeTo(propertyExist);
            LOG.debug("property exist altered before update: " + propertyExist);
            propertyRepository.update(propertyExist);
//            LOG.debug("property exist found in db after update: " + propertyRepository.findById(property.getId()));
        } catch (Exception e) {
            LOG.error(e);
        }
    }

    @Override
    public List<Property> findAll() {
        List<Property> props = propertyRepository.findAll();
        return props;
    }

    @Override
    public Property findById(Long id) {
        return propertyRepository.findById(id);
    }

    @Override
    public Property create(Property property) {
        LOG.info("property to create: " + property);
        Address newAddress = property.getAddress();
        // make sure the new address is saved before saving the new property
        if (property.getAddress().getId() == null) {
            addressRepository.create(newAddress);
        }
        else {
            Address existingAddress = addressRepository.findById(property.getAddress().getId());
            LOG.info("existing address is: " + existingAddress + ", id=" + (existingAddress==null? null : existingAddress.getId()));
            if (existingAddress == null) {
                LOG.warn("new property with a non-existing address id");
                // consider it brand new address, remove id and create it
                newAddress.setId(null);
                newAddress = addressRepository.create(property.getAddress());                
                property.setAddress(newAddress);
            }
            else if (newAddress.equalsByValue(existingAddress)){
                property.setAddress(existingAddress);
            } 
            else {
                // modifying an existing address, so a new address should be created, 
                // remove the address id, db will give a new id
                property.getAddress().setId(null);
                newAddress = addressRepository.create(property.getAddress());                
                property.setAddress(newAddress);
            }
        }        
        List<Rent> rents = property.getRents();
        property.setRents(null);  // work around to so the property will save to db
        propertyRepository.create(property); // TODO check property is updated by DB
        property.setRents(rents);
        // save reference to the new rent, and save it
        if (property.getRents() != null) {
            for (Rent rent : property.getRents()) {
                if (rent.getId() == null) {
                    rent.setProperty(property);
                    rentRepository.create(rent);
                }
            }
        }
        LOG.info("property created: " + property);        
        return property;
    }

    @Override
    public void delete(Property property) {
        propertyRepository.delete(property);
    }

    @Override
    public void deleteRentByRentId(Long rentId) {
        Rent rent = rentRepository.findById(rentId);
        if (rentRepository.findAllSizeRelatedByOneId(rentId) == 1) {
            throw new DeletingLastRentException();
        }
        rentRepository.delete(rent);
    }

    @Override
    public void addRentByPropertyId(Long propertyId, Rent rent) {
        Property property = propertyRepository.findById(propertyId);
        rent.setProperty(property);
        rentRepository.create(rent);
    }

    @Override
    public Long countAll() {
        return propertyRepository.countAll();
    }

    @Override
    public List<Property> findAllWithNoActiveLease() {
        return propertyRepository.findAllWithNoActiveLease();
    }

    @Override
    public Long countAllWithNoActiveLease() {
        return propertyRepository.countAllWithNoActiveLease();
    }
    
    
}
