/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.wrapper;

import com.jajadede.manage.rental.domain.Action;
import com.jajadede.manage.rental.domain.TransactionMethod;
import com.jajadede.manage.rental.domain.TransactionPurpose;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Jerry
 */
public class RentalActivity {
    private Long propertyId;
    private Long leaseId;
    private Date actionDate;
    private Action action;
    private TransactionPurpose purpose;
    private Date effectiveDate;
    private BigDecimal amount;
    private TransactionMethod method;

    public RentalActivity(){
    }
    
    public RentalActivity(Long propertyId, Long leaseId, Date actionDate, Action action, TransactionPurpose purpose, Date effectiveDate, BigDecimal amount, TransactionMethod method){
        this.propertyId = propertyId;
        this.leaseId = leaseId;
        this.actionDate = actionDate;
        this.action = action;
        this.purpose = purpose;
        this.effectiveDate = effectiveDate;
        this.amount = amount;
        this.method = method;
    }
    
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("propertyId=").append(propertyId);
        sb.append("leaseId=").append(leaseId);
        sb.append("actionDate=").append(actionDate);
        sb.append("purpose=").append(purpose);
        sb.append("effectiveDate=").append(effectiveDate);
        sb.append("amount=").append(amount);
        sb.append("method=").append(method);
        return sb.toString();
    }

    public TransactionMethod getMethod() {
        return method;
    }

    public void setMethod(TransactionMethod method) {
        this.method = method;
    }

    
    public Long getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Long propertyId) {
        this.propertyId = propertyId;
    }

    public Long getLeaseId() {
        return leaseId;
    }

    public void setLeaseId(Long leaseId) {
        this.leaseId = leaseId;
    }

    public Date getActionDate() {
        return actionDate;
    }

    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public TransactionPurpose getPurpose() {
        return purpose;
    }

    public void setPurpose(TransactionPurpose purpose) {
        this.purpose = purpose;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    
    
}
