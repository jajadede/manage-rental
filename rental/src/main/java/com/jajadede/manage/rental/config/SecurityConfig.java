package com.jajadede.manage.rental.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;

@Configuration
@EnableWebSecurity
@Profile("dev")
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public AccessDeniedHandler accessDeniedHandler() {
        return new CustomAccessDeniedHandler();
    }

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        auth.inMemoryAuthentication().withUser("guest").password(encoder.encode("welcome")).roles("USER");
        auth.inMemoryAuthentication().withUser("admin").password(encoder.encode("myadminpass")).roles("USER", "ADMIN");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.formLogin().loginPage("/login").usernameParameter("userId").passwordParameter("password");
        http.formLogin().defaultSuccessUrl("/").failureUrl("/login?error");
        http.logout().logoutSuccessUrl("/login?logout");
//		http.exceptionHandling().accessDeniedPage("/login?accessDenied");
        http.exceptionHandling().accessDeniedHandler(accessDeniedHandler());
        http.authorizeRequests()
                .antMatchers("/**/login**").permitAll()
                .antMatchers("/**/delete").access("hasRole('ADMIN')")
                .antMatchers("/**/edit").access("hasRole('ADMIN')")
                .antMatchers("/**/eventdelete").access("hasRole('ADMIN')")
                .antMatchers("/**/rentdelete").access("hasRole('ADMIN')")
                .antMatchers("/**").access("hasRole('USER')");
        http.csrf().disable();
    }

}
