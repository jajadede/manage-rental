/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.service.impl;

import com.jajadede.manage.rental.domain.Address;
import com.jajadede.manage.rental.domain.repository.AddressRepository;
import com.jajadede.manage.rental.domain.repository.RentRepository;
import com.jajadede.manage.rental.service.AddressService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Jerry
 */
@Service ("addressService")
public class AddressServiceImpl implements AddressService {

    @Autowired
    private AddressRepository addressRepository;
    
    @Autowired
    private RentRepository rentRepository;    
    
    @Override
    public void update(Address address) {
        addressRepository.update(address);
    }

    @Override
    public List<Address> findAll() {
        List<Address> props = addressRepository.findAll();
        return props;
    }

    @Override
    public Address findById(Long id) {
        return addressRepository.findById(id);
    }

    @Override
    public Address create(Address address) {
        Address newAddress = addressRepository.create(address);
        return newAddress;
    }

    @Override
    public void delete(Address address) {
        addressRepository.delete(address);
    }

    @Override
    public Long countLonely() {
        return addressRepository.countLonely();
    }

    @Override
    public List<Address> findAllWithNoPeople() {
        return addressRepository.findAllWithNoPeople();
    }

    @Override
    public List<Address> findAllWithNoProperty() {
        return addressRepository.findAllWithNoProperty();
    }

    @Override
    public Long countAllWithNoPeople() {
        return addressRepository.countAllWithNoPeople();
    }

    @Override
    public Long countAllWithNoProperty() {
        return addressRepository.countAllWithNoProperty();
    }
    
}
