package com.jajadede.manage.rental.controller;

import com.jajadede.manage.rental.service.SummaryService;
import com.jajadede.manage.rental.wrapper.RentalActivity;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Jerry
 */
@RestController
@RequestMapping(value = "rest")
public class SummaryRestController {
    
    @Autowired
    SummaryService summaryService;
    
    @RequestMapping(value = "/summary/findTransactionEffectiveYearMonth/{withinYears}", method = RequestMethod.GET)
    public List<String> findTransactionEffectiveYearMonth(@PathVariable(value = "withinYears") int withinYears) {
        return summaryService.findYearMonthList(withinYears, true);
    }
    
    @RequestMapping(value = "/summary/findRentalActivityByYearMonth/{year}/{month}", method = RequestMethod.GET)
    public List<RentalActivity> findByYearMonth(@PathVariable(value = "year") int year, @PathVariable(value = "month") int month) {
        return summaryService.findByYearMonth(year, month);
    }     
}
