package com.jajadede.manage.rental.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.SortNatural;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author Jerry
 */
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class, 
        property = "id")
@Entity
@Table(name = "lease")
@NamedQueries({
        @NamedQuery(name=Lease.LEASE_FIND_BY_ID,
                query="select a from Lease a "
                        + "left join a.property "
                        + "left join fetch a.tenantsSet "
                        + "left join fetch a.eventsSet e "
                                               
                        + "where a.id = :ID "
                        )
        ,@NamedQuery(name=Lease.LEASE_FIND_ALL,
                query="select distinct a from Lease a "
                        + "left join a.property "
                        + "left join fetch a.tenantsSet "
                        + "left join fetch a.eventsSet " 
        )
        ,@NamedQuery(name=Lease.LEASE_COUNT_ALL,
                query="select count(a) from Lease a ")          
})
public class Lease extends AbstractEntity{
    private static final Logger LOG = LogManager.getLogger(Lease.class.getName());
    
    public static final String LEASE_FIND_BY_ID = "Lease.findById";
    public static final String LEASE_FIND_ALL = "Lease.findAll";
    public static final String LEASE_COUNT_ALL = "Lease.countAll";
        
    
    @Column (name = "status", length=10) @Enumerated(EnumType.STRING)
    private LeaseStatus status;
    @ManyToOne @JoinColumn (name = "property_id", foreignKey = @ForeignKey(name = "FK_LEASE_PEOPERTY"))    
    private Property property;
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}) 
    @JoinTable (name = "lease_people", 
            joinColumns={@JoinColumn(name="tenants_lease_id", referencedColumnName = "id")}, 
            foreignKey = @ForeignKey(name = "FK_LEASE_PEOPLE"), 
            inverseJoinColumns={@JoinColumn(name="tenants_people_id", referencedColumnName = "id")},
            inverseForeignKey = @ForeignKey(name = "FK_PEOPLE_LEASE"))
    private Set<People> tenantsSet = new HashSet<>();
    @Column (name = "effective_date") @Temporal(TemporalType.DATE) @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date effectivedate;
    @Column (name = "end_date") @Temporal(TemporalType.DATE) @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date enddate;
    @OneToMany (mappedBy = "lease", cascade= CascadeType.ALL, orphanRemoval = true) @SortNatural   
    private SortedSet<Event> eventsSet = new TreeSet<>();
    @Column (name = "comment")
    private String comment;
    @Transient
    private BigDecimal currentrent; // calculated value    
    /// act as data transfer objects
    @Transient
    private List<People> tenants = new ArrayList<>(); 
    @Transient
    private List<Event> events = new ArrayList<>(); 

    /**
     * Sets get populated by corresponding lists, do this before saving or updating
     */
    public void prepareSetsBeforeSave() {
        tenantsSet.clear();
        tenantsSet.addAll(tenants);
        tenants.clear();
        eventsSet.clear();
        eventsSet.addAll(events);
        events.clear();
    }
    
    /**
     * Lists get populated by corresponding sets, do this after loading;
     * Events list is sorted by action date as well.
     */
    public void prepareListsAfterLoad() {
        tenants.clear();
        tenants.addAll(tenantsSet);
        tenantsSet.clear();
        events.clear();
        events.addAll(eventsSet);
        eventsSet.clear();
    }
   
    /**
     * This set is for db only, never use it outside service layer
     * @return 
     */
    public Set<People> getTenantsSet() {
        return tenantsSet;
    }

    /**
     * This set is for db only, never use it outside service layer
     * @param tenantsSet 
     */
    public void setTenantsSet(Set<People> tenantsSet) {
        this.tenantsSet = tenantsSet;
    }

    /**
     * This set is for db only, never use it outside service layer
     * @return 
     */
    public Set<Event> getEventsSet() {
        return eventsSet;
    }

    /**
     * This set is for db only, never use it outside service layer
     * @param eventsSet
     */
    public void setEventsSet(SortedSet<Event> eventsSet) {
        this.eventsSet = eventsSet;
    }

    /**
     * create a lease with null id and 4 month lease, starting from 1st day of next month.
     */
    public Lease () {
        this(null, 4);        
    }
    
    /**
     * Create a lease with given id and given term length
     * @param id
     * @param termLength 
     */
    public Lease(Long id, int termLength) {
        this.id = id;
        
        Calendar today = Calendar.getInstance();
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        start.set(Calendar.YEAR, today.get(Calendar.YEAR));
        start.set(Calendar.MONTH, today.get(Calendar.MONTH)+1);
        start.set(Calendar.DAY_OF_MONTH, 1);
        end.setTime(start.getTime());
        end.add(Calendar.MONTH, termLength);
        end.add(Calendar.DAY_OF_MONTH, -1);
        effectivedate = start.getTime();
        enddate = end.getTime();
        // mark it active
        status = LeaseStatus.ACTIVE;
    }
    
    /**
     * This set is for db only, never use it outside service layer
     * @param e 
     */
    public void addEventToSet(Event e) {
        if (e != null) {
            getEventsSet().add(e);
            e.setLease(this);
        }
    }
    /**
     * This set is for db only, never use it outside service layer
     * @param e 
     */
    public void removeEventFromSet(Event e) {
        if (e != null) {
            getEventsSet().removeIf( myevent -> myevent.getId().equals(e.getId()));
            e.setLease(null);
        }
    }
    
    public void addEvent(Event e) {
        if (e != null) {
            getEvents().add(e);
            e.setLease(this);
        }
    }
    
    public void removeEvent(Event e) {
        if (e != null) {
            getEvents().removeIf( myevent -> Objects.equals(myevent.getId(), e.getId()));
            e.setLease(null);
        }
    }
    
    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash = 47 * hash + Objects.hashCode(this.getStatus());
        hash = 47 * hash + Objects.hashCode(this.getProperty());
        hash = 47 * hash + Objects.hashCode(this.getTenants());
        hash = 47 * hash + Objects.hashCode(this.getEffectivedate());
        hash = 47 * hash + Objects.hashCode(this.getEnddate());
        hash = 47 * hash + Objects.hashCode(this.getEvents());
        hash = 47 * hash + Objects.hashCode(this.getComment());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        // super class checked type already, casting is safe here
        final Lease other = (Lease) obj;
        // use method version of accessors for hibernate's proxies efficiency
        if (!Objects.equals(this.getStatus(), other.getStatus())) {
            return false;
        }
        if (!Objects.equals(this.getComment(), other.getComment())) {
            return false;
        }
        if (!Objects.equals(this.getProperty(), other.getProperty())) {
            return false;
        }
        if (!Objects.equals(this.getEffectivedate(), other.getEffectivedate())) {
            return false;
        }
        if (!Objects.equals(this.getEnddate(), other.getEnddate())) {
            return false;
        }
        return true;
    }

    public LeaseStatus getStatus() {
        return status;
    }

    public void setStatus(LeaseStatus status) {
        this.status = status;
    }

    public Property getProperty() {
        return property;
    }

    public void setProperty(Property property) {
        this.property = property;
    }

    public List<People> getTenants() {
        return tenants;
    }

    public void setTenants(List<People> tenants) {
        this.tenants = tenants;
    }

    public Date getEffectivedate() {
        return effectivedate;
    }

    public void setEffectivedate(Date effectivedate) {
        this.effectivedate = effectivedate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public BigDecimal getCurrentrent() {
        return currentrent;
    }

    public void setCurrentrent(BigDecimal currentrent) {
        this.currentrent = currentrent;
    }

    @Override
    public void transferChangeTo(Object t) {
        Lease other = (Lease) t;
        other.status = this.status;
        other.comment = this.comment;
        other.currentrent = this.currentrent;
        other.effectivedate = this.effectivedate;
        other.enddate = this.enddate;
    }    

    @Override
    public String toString() {
        return jsonString();
    }
    
    public String jsonString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{")
                .append("\"id\":\"").append(id).append("\"")
                .append(",").append("\"version\":\"").append(version).append("\"")
                .append(",").append("\"currentrent\":\"").append(currentrent).append("\"")
                .append(",").append("\"effectivedate\":\"").append(effectivedate).append("\"")
                .append(",").append("\"enddate\":\"").append(enddate).append("\"")              
                .append(",").append("\"comment\":\"").append(comment).append("\"");
        
        // property
        sb.append(",");
        sb.append("\"property\":");
        if (property != null) {
            sb.append(property.jsonString());
        }
        else {
            sb.append("null");
        }
        // events
        sb.append(",");
        sb.append("\"events\":");
        if (events != null) {
            try {
                events.iterator();
                sb.append("[");
                boolean isFirst = true;
                for (Event entry : events) {
                    if (!isFirst) {
                        sb.append(",");
                    }
                    sb.append(entry.jsonString());           
                    isFirst = false;
                }
                sb.append("]");
            } catch (Exception e) {
                LOG.error("events is not ready.");
                sb.append("\"lazily initialized\"");
            }            
        } else {
            sb.append("null");
        }
        // eventsSet
        sb.append(",");
        sb.append("\"eventsSet\":");
        if (eventsSet != null) {
            try {
                eventsSet.iterator();
                sb.append("[");
                boolean isFirst = true;
                for (Event entry : eventsSet) {
                    if (!isFirst) {
                        sb.append(",");
                    }
                    sb.append(entry.jsonString());           
                    isFirst = false;
                }
                sb.append("]");
            } catch (Exception e) {
                LOG.error("eventsSet is not ready.");
                sb.append("\"lazily initialized\"");
            }            
        } else {
            sb.append("null");
        }
        
        // tenants
        sb.append(",");
        sb.append("\"tenants\":");        
        if (tenants != null) {
            try {
                tenants.iterator();
                sb.append("[");
                boolean isFirst = true;
                for (People entry : tenants) {
                    if (!isFirst) {
                        sb.append(",");
                    }
            sb.append("{");
                    sb.append("\"id\":\"").append(entry.getId()).append("\"");
            sb.append("}");           
                    isFirst = false;
                }
                sb.append("]");
            } catch (Exception e) {
                LOG.error("tenants is not ready.");
                sb.append("\"lazily initialized\"");
            }            
        } else {
            sb.append("null");
        }        
        // tenantsSet
        sb.append(",");
        sb.append("\"tenantsSet\":");        
        if (tenantsSet != null) {
            try {
                tenantsSet.iterator();
                sb.append("[");
                boolean isFirst = true;
                for (People entry : tenantsSet) {
                    if (!isFirst) {
                        sb.append(",");
                    }
            sb.append("{");
                    sb.append("\"id\":\"").append(entry.getId()).append("\"");
            sb.append("}");           
                    isFirst = false;
                }
                sb.append("]");
            } catch (Exception e) {
                LOG.error("tenantsSet is not ready.");
                sb.append("\"lazily initialized\"");
            }            
        } else {
            sb.append("null");
        }
        
        sb.append("}").toString();        
        return sb.toString();
    }

    public String toTenantsShortString() {
        StringBuilder sb = new StringBuilder();
        if (tenants != null) {
            boolean isFirst = true;
            for (People t : tenants) {
                if (!isFirst) sb.append(", ");
                sb.append(t.getNickname());
                isFirst = false;
            }
        }
        return sb.toString();
    }   
    
    public String toTermString() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        LocalDate ldEnddate = LocalDate.parse(sdf.format(enddate));
        LocalDate ldEffectivedate = LocalDate.parse(sdf.format(effectivedate));
        Action lastAction = events.size() > 0 ? events.get(events.size()-1).getAction() : null;
        Date lastDay;
        if (null == lastAction) {
            // force last day to be effeive date -1, so the following logic will make the 'passed' variable to be 0
            Calendar c = Calendar.getInstance();
            c.setTime(effectivedate);
            c.add(Calendar.DATE, -1);
            lastDay = c.getTime();
        }
        else switch (lastAction) {
            case MOVE_OUT:
            case CANCEL_RESERVATION:
                lastDay = events.get(events.size()-1).getActiondate();
                break;
            default:
                lastDay = new Date();
                break;
        }
        LocalDate ldToday = LocalDate.parse(sdf.format(lastDay));
        long termlength = Period.between(ldEffectivedate, ldEnddate).plusMonths(1L).toTotalMonths();
        long passed = lastDay.before(effectivedate) ? 0 : Period.between(ldEffectivedate, ldToday).plusMonths(1L).toTotalMonths();
        passed = passed < 0 ? 0 : passed;
        StringBuilder sb = new StringBuilder();
        sb.append(passed).append("/").append(termlength);
        return sb.toString();
    }
}
