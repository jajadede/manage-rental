/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.controller;

import com.jajadede.manage.rental.domain.People;
import com.jajadede.manage.rental.service.PeopleService;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Jerry
 */
@RestController
@RequestMapping(value = "rest")
public class PeopleRestController {

    private static final Logger LOG = LogManager.getLogger(PeopleRestController.class.getName());

    @Autowired
    private PeopleService peopleService;
    
    @RequestMapping(value = "/people", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.CREATED)
    public void create(@RequestBody People people) {
        peopleService.create(people);
    }

    @RequestMapping(value = "/people/{peopleId}", method = RequestMethod.GET)
    public People read(@PathVariable(value = "peopleId") Long peopleId) {
        return peopleService.findById(peopleId);
    }
    
    @RequestMapping(value = "/people/list", method = RequestMethod.GET)
    public List<People> list() {
        return peopleService.findAll();
    }    

    @RequestMapping(value = "/people/{peopleId}", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    public void update(@PathVariable(value = "peopleId") Long peopleId,
            @RequestBody People people) {
        LOG.debug("inside update method...");
        people.setId(peopleId);
        peopleService.update(people);
    }

    @RequestMapping(value = "/people/{peopleId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    public void delete(@PathVariable(value = "peopleId") Long peopleId) {
        LOG.debug("inside delete method...");
        People people = new People();
        people.setId(peopleId);
        peopleService.delete(people);
    }   
}
