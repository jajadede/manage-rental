/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.domain.repository.impl;

import com.jajadede.manage.rental.domain.Address;
import com.jajadede.manage.rental.domain.People;
import com.jajadede.manage.rental.domain.repository.PeopleRepository;
import com.jajadede.manage.rental.service.AddressService;
import com.jajadede.manage.rental.util.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Jerry 
 */
@Repository("peopleRepository")
@Transactional
public class PeopleRepositoryImpl implements PeopleRepository {
    private static final Logger LOG = LogManager.getLogger(PeopleRepositoryImpl.class.getName()); 
    
    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private AddressService addressService;
    
    @Override
    @Transactional(readOnly = true)
    public List<People> findAll() {
        List<People> result = (List<People>) HibernateUtil.doWithinTransaction(sessionFactory, (session) -> {
            return session.getNamedQuery(People.PEOPLE_FIND_ALL).list();
        });
        if (result == null) {
            result = new ArrayList<>();
        }
        LOG.debug("Got result peoples from querying all peoples: " + result.toString());
        return result;
    }

    @Override
    @Transactional(readOnly = true)    
    public People findById(Long id) {
        return (People) HibernateUtil.doWithinTransaction(sessionFactory, (session) -> {
            return session.getNamedQuery(People.PEOPLE_FIND_BY_ID).setParameter("ID", id).uniqueResult();
        });
    }

    @Override
    public People create(People people) {    
        LOG.info("people to create: " + people);
        HibernateUtil.doWithinTransaction(sessionFactory, (Session session) -> {
            Address newAddress = people.getAddress();
            if (newAddress == null) {
                // doesn't matter
            }
            else if (newAddress.getId() == null) {
                session.save(newAddress);
            }
            else {
                Address dbaddress = session.get(Address.class, newAddress.getId());
                if (dbaddress == null ) {
                    session.save(newAddress);
                }
                else if (dbaddress.equalsByValue(newAddress)) {
                    newAddress = dbaddress;
                }
                else {
                    newAddress.setId(null);
                    session.save(newAddress);
                }
            }
            people.addAddress(newAddress);
            return session.save(people);
        });
//        Address newAddress = people.getAddress();
//        // make sure the new address is saved before saving the new people
//        if (newAddress != null && newAddress.getId() == null) {
//            addressService.create(newAddress);
//        }
//        else {
//            Address existingAddress = addressService.findById(people.getAddress().getId());
//            LOG.info("existing address is: " + existingAddress + ", id=" + (existingAddress==null? null : existingAddress.getId()));
//            if (existingAddress == null) {
//                LOG.warn("new people with a non-existing address id");
//                // consider it brand new address, remove id and create it
//                newAddress.setId(null);
//                newAddress = addressService.create(people.getAddress());                
//                people.setAddress(newAddress);
//            }
//            else if (newAddress.equalsByValue(existingAddress)){
//                people.setAddress(existingAddress);
//            } 
//            else {
//                // modifying an existing address, so a new address should be created, 
//                // remove the address id, db will give a new id
//                people.getAddress().setId(null);
//                newAddress = addressService.create(people.getAddress());                
//                people.setAddress(newAddress);
//            }
//        } 
//        sessionFactory.getCurrentSession().save(people);        
        LOG.debug("Created people with new id: " + people.getId());
        LOG.info("people created: " + people);
        return people;
    }

    @Override
    public void update(People people) {
        LOG.info("people to update: " + people);        
        HibernateUtil.doWithinTransaction(sessionFactory, (Session session) -> {
            People dbPeople = session.get(People.class, people.getId());
            people.transferChangeTo(dbPeople);
            people.getAddress().transferChangeTo(dbPeople.getAddress());
            return null;
        });     
//        try {

//            Address address = people.getAddress();
//            if (address == null || address.getId() == null) {
//                LOG.error("Nothing done. Try to update people with null address or null address id, address=" + address);
//                return;
//            }
//            Address existingAddress = addressService.findById(address.getId());
//            if (existingAddress == null) {
//                LOG.error("Nothing done. Try to update people with an non-existing address, address=" + address);
//                return;
//            }
//
//            // transfer changes to the db people obj
//            People peopleExist = findById(people.getId());
//            LOG.debug("people exist found in db: " + peopleExist);
//            people.transferChangeTo(peopleExist);
//            LOG.debug("people exist altered before update: " + peopleExist);
//            sessionFactory.getCurrentSession().merge(peopleExist);
////            LOG.debug("people exist found in db after update: " + peopleRepository.findById(people.getId()));
//        } catch (HibernateException e) {
//            LOG.error(e);
//        }        
        
        LOG.debug("Updated people with id: " + people.getId());
    }

    @Override
    public void delete(People people) {
        HibernateUtil.doWithinTransaction(sessionFactory, (Session session) -> {
            People db = session.get(People.class, people.getId());
            session.delete(db);
            return null;
        });
        LOG.debug("Deleted people with id: " + people.getId());
    }

    @Override
    public Long countAll() {
        return (Long) HibernateUtil.doWithinTransaction(sessionFactory, (Session session) -> { 
            return session.getNamedQuery(People.PEOPLE_COUNT_ALL).uniqueResult();
        });
    }

    @Override
    public List<People> findAllWithNoActiveLease() {
        List<People> result = (List<People>) HibernateUtil.doWithinTransaction(sessionFactory, (Session session) -> {
            Query query = session.createQuery("select p from People p "
                    + "where p.id not in "
                    + "(select distinct lp.id "
                    + "from Lease l join l.tenantsSet lp "
                    + "where l.status = 'ACTIVE')");
            return query.list();
        });
        LOG.debug("Got result from findAllWithNoActiveLease: " + result);
        return result;
    }

    @Override
    public Long countAllWithNoActiveLease() {
        Long result = (Long) HibernateUtil.doWithinTransaction(sessionFactory, (Session session) -> {
            Query query = session.createQuery("select count(p) from People p "
                    + "where p.id not in "
                    + "(select distinct lp.id "
                    + "from Lease l join l.tenantsSet lp "
                    + "where l.status = 'ACTIVE')");
            return (Long) query.uniqueResult();
        });  
        LOG.debug("Got result from countAllWithNoActiveLease: " + result); 
        return result;
    }

}
