/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.util;

import com.jajadede.manage.rental.domain.AbstractEntity;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 * @author Jerry
 */
public class HibernateUtil {
    private static final Logger LOG = LogManager.getLogger(HibernateUtil.class.getName());

    private HibernateUtil() 
    {
    }
    
    public static Object doWithinTransaction(SessionFactory sessionFactory, Function<Session,Object> process) {
        synchronized (sessionFactory) {
            Object result = null;
            try (Session session = sessionFactory.openSession()) {
                Transaction tx = session.beginTransaction();
                result = process.apply(session);
                tx.commit();
            } catch (HibernateException e) {
                LOG.error(e);
                Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
                if (tx.isActive()) {
                    tx.rollback();
                }
            }
            return result;
        }
    }
    
    /**
     * Transfers change between two sets containing same number of AbstracEntity or it's subclasses elements
     * @param fromSet
     * @param toSet
     */
    public static void transferChanges(
            Set<? extends AbstractEntity> fromSet,
            Set<? extends AbstractEntity> toSet) {
        if (fromSet.size() != fromSet.size()) {
            LOG.error("Two set must have the same size, but they are: "
                    + "fromSet.size()=" + fromSet.size()
                    + " toSet.size()=" + toSet.size());
            throw new RuntimeException("Two sets are not of the same size.");
        }
        if ((fromSet.size() > 0 && toSet.size() > 0)
                && (fromSet.iterator().next().getClass() != toSet.iterator().next().getClass())) {
            LOG.error("Two set must have the same element type, but they are: "
                    + "fromSet<" + fromSet.iterator().next().getClass() + ">"
                    + " toSet<" + toSet.iterator().next().getClass() + ">");
            throw new RuntimeException("Elemnts in two sets are not of the same type.");
        }
        Map<Long, AbstractEntity> fromMap = new ConcurrentHashMap<>();
        fromSet.forEach(s -> fromMap.put(s.getId(), s));
        Map<Long, AbstractEntity> toMap = new ConcurrentHashMap<>();
        toSet.forEach(s -> toMap.put(s.getId(), s));
        try {
            fromMap.keySet().forEach(key -> fromMap.get(key).transferChangeTo(toMap.get(key)));
        } catch (RuntimeException e) {
            LOG.error(e);
            throw e;
        }
    }
    
    public static int compareDates (Date one, Date other) {
        if (Objects.equals(one, other)) return 0;
        if (null == one || null==other) throw new RuntimeException("Cannot compare two dates with only one equals null.");
        if (one.before(other)) return -1;
        else if (one.after(other)) return 1;
        else return 0;
    }
}
