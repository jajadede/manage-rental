package com.jajadede.manage.rental.validator;

import com.jajadede.manage.rental.wrapper.SummaryChoice;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author Jerry
 */
@Component
public class SummaryChoiceValidator implements Validator {
    private static final Logger LOG = LogManager.getLogger(SummaryChoiceValidator.class.getName());
    
    @Override
    public boolean supports(Class<?> clazz) {
        return SummaryChoice.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        SummaryChoice choice = (SummaryChoice) target;
        
        // common check
        validateYearMonth(choice, errors);
    }
    
    private void validateYearMonth(SummaryChoice choice, Errors errors) {
        String ym = choice.getYearMonth();
        if (ym == null || ym.isEmpty()) {
                    errors.rejectValue(
                            "yearMonth",
                            "validator.SummaryChoiceValidator.yearMonth.CannotBeEmpty.message");
                    LOG.debug("Error found, validator.SummaryChoiceValidator.yearMonth.CannotBeEmpty.message is set.");               
        }
    }
}
