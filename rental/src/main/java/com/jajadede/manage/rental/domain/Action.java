package com.jajadede.manage.rental.domain;

/**
 *
 * @author Jerry
 */
public enum Action {
    PAY_RENT          ("Pay", "Pay rent for current month"),
    MOVE_IN           ("Move In", "Move in and pay rent"),
    MOVE_OUT          ("Move Out", "Move out and get refund "),
    RESERVE           ("Reserve", "Reserve and pay deposit"),
    CANCEL_RESERVATION("Cancel Resv", "Cancel reservation and get refund");

    private final String value;
    private final String key;

    Action(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
    
    public String getKey() {
        return this.key;
    }
}
