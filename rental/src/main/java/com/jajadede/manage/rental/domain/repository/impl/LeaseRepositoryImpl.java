package com.jajadede.manage.rental.domain.repository.impl;

import com.jajadede.manage.rental.domain.Event;
import com.jajadede.manage.rental.domain.Lease;
import com.jajadede.manage.rental.domain.People;
import com.jajadede.manage.rental.domain.Property;
import com.jajadede.manage.rental.domain.Transaction;
import com.jajadede.manage.rental.domain.repository.LeaseRepository;
import com.jajadede.manage.rental.util.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Jerry 
 */
@Repository("leaseRepository")
@Transactional
public class LeaseRepositoryImpl implements LeaseRepository {
    private static final Logger LOG = LogManager.getLogger(LeaseRepositoryImpl.class.getName()); 
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    @Transactional(readOnly = true)
    public List<Lease> findAll() {
        List<Lease> result = (List<Lease>) HibernateUtil.doWithinTransaction(sessionFactory, (session) -> {
            return session.getNamedQuery(Lease.LEASE_FIND_ALL).list();
        });
        if (result == null) {
            result = new ArrayList<>();
        }
        LOG.debug("Got result leases from querying all leases: " + result.toString());
        result.stream().forEach(lease -> lease.prepareListsAfterLoad());
        return result;
    }

    @Override
    @Transactional(readOnly = true)    
    public Lease findById(Long id) {
        Lease result = (Lease) HibernateUtil.doWithinTransaction(sessionFactory, (session) -> {
            return session.getNamedQuery(Lease.LEASE_FIND_BY_ID).setParameter("ID", id).uniqueResult();
        });
        result.prepareListsAfterLoad();
        return result;
    }

    @Override
    public Lease create(Lease lease) {    
        LOG.info("lease to create: " + lease);
        lease.prepareSetsBeforeSave();
        Lease result = (Lease) HibernateUtil.doWithinTransaction(sessionFactory, (Session session) -> {
            Lease dbLease = new Lease();
            lease.transferChangeTo(dbLease);
            // tenants
            Set<People> tenants = lease.getTenantsSet();            
            for (People p : tenants) {
                if (p.getId() != null) {
                    People dbpeople = session.get(People.class, p.getId());
                    dbLease.getTenantsSet().add(dbpeople);
                }
            }
            // property
            Property dbproperty = session.get(Property.class, lease.getProperty().getId());
            dbLease.setProperty(dbproperty);
            // event, just transafer one event
            Event dbEvent = new Event();
            dbEvent.addTransaction(new Transaction());
            lease.getEventsSet().iterator().next().transferChangeTo(dbEvent);
            dbLease.addEventToSet(dbEvent);
            session.save(dbLease);
            return dbLease;
        });       
        LOG.debug("Created lease with new id: " + result.getId());
        LOG.info("lease after create: " + result);
        return result;
    }

    @Override
    public void update(Lease lease) {
        LOG.info("lease to update: " + lease); 
        lease.prepareSetsBeforeSave();
        HibernateUtil.doWithinTransaction(sessionFactory, (Session session) -> {
            Lease dbLease = session.get(Lease.class, lease.getId());
            lease.transferChangeTo(dbLease);
            // tenants
            dbLease.getTenantsSet().clear();
            Set<People> tenants = lease.getTenantsSet();            
            for (People p : tenants) {
                if (p.getId() != null) {
                    People dbpeople = session.get(People.class, p.getId());
                    dbLease.getTenantsSet().add(dbpeople);
                }
            }
            // property
            Property dbproperty = session.get(Property.class, lease.getProperty().getId());
            dbLease.setProperty(dbproperty);
            // events
            HibernateUtil.transferChanges(lease.getEventsSet(), dbLease.getEventsSet());
            return null;
        });        
        LOG.debug("Updated lease with id: " + lease.getId());
    }

    @Override
    public void delete(Lease lease) {
        HibernateUtil.doWithinTransaction(sessionFactory, (Session session) -> {
            Lease db = session.get(Lease.class, lease.getId());
            session.delete(db);
            return null;
        });
        LOG.debug("Deleted lease with id: " + lease.getId());
    }

    @Override
    public Long countAll() {
        return (Long) HibernateUtil.doWithinTransaction(sessionFactory, (Session session) -> { 
            return session.getNamedQuery(Lease.LEASE_COUNT_ALL).uniqueResult();
        });
    }

    @Override
    public void deleteEventById(Long eventId) {
        HibernateUtil.doWithinTransaction(sessionFactory, (Session session) -> {
            Event db = session.get(Event.class, eventId);
            session.delete(db);
            return null;
        });
        LOG.debug("Deleted event with event id: " + eventId);        
    }

    @Override
    public void addEvent(Event event) {
        Long result = (Long) HibernateUtil.doWithinTransaction(sessionFactory, (Session session) -> {
            Long leaseId = event.getLease().getId();
            Lease db = session.get(Lease.class, leaseId);
            if (db == null) return -1L;
            if (db.getEventsSet() == null) {
                db.setEventsSet(new TreeSet<>());
            }
            Event newEvent = new Event();
            newEvent.addTransaction(new Transaction());
            event.transferChangeTo(newEvent);
            db.setStatus(event.getLease().getStatus());
            db.addEventToSet(newEvent);
            session.save(newEvent);
            return newEvent.getId();
        });
        LOG.debug("Added event with event id: " + result);          
    }

    @Override
    public void deleteEvent(Event event) {
        Long eventId = (Long) HibernateUtil.doWithinTransaction(sessionFactory, (Session session) -> {
            Event db = session.get(Event.class, event.getId());
            db.getLease().setStatus(event.getLease().getStatus());
            session.delete(db);
            return db.getId();
        });
        LOG.debug("Deleted event with event id: " + eventId);
    }
    
    @Override
    public Event findEventById(Long eventId) {
        Event event = (Event) HibernateUtil.doWithinTransaction(sessionFactory, (Session session) -> {
            Event db = session.get(Event.class, eventId);
            return db;
        });
        LOG.debug("Found event: " + event);
        return event;
    }

}
