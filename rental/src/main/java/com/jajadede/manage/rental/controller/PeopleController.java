package com.jajadede.manage.rental.controller;

import com.jajadede.manage.rental.domain.Address;
import com.jajadede.manage.rental.domain.Gender;
import com.jajadede.manage.rental.domain.People;
import com.jajadede.manage.rental.service.AddressService;
import com.jajadede.manage.rental.service.PeopleService;
import com.jajadede.manage.rental.validator.PeopleValidator;
import javax.persistence.PersistenceException;
import javax.validation.Valid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PeopleController {

    private static final Logger LOG = LogManager.getLogger(PeopleController.class.getName());

    @Autowired
    private PeopleService peopleService;    
    @Autowired
    private AddressService addressService;

    @Autowired
    private PeopleValidator peopleValidator;
    
    @RequestMapping("/people/list")
    public String list(Model model) {
        model.addAttribute("peoples", peopleService.findAll());
        return "peoples";
    }

    @RequestMapping(value = "/people/add", method = RequestMethod.GET)
    public String addFormGet(Model model) {
        People newPeople = new People();
        newPeople.setAddress(new Address());
        model.addAttribute("newPeople", newPeople);
        model.addAttribute("addresses", addressService.findAllWithNoProperty());
        model.addAttribute("genders", Gender.values());
        return "peopleAdd";
    }

    @RequestMapping(value = "/people/add", method = RequestMethod.POST)
    public String addFormProcess(
            @ModelAttribute("newPeople") @Valid People newPeople,
            BindingResult result,
            Model model) {
        LOG.debug("newPeople got from request: " + newPeople);
        if (result.hasErrors()) {
            model.addAttribute("newPeople", newPeople);
            model.addAttribute("addresses", addressService.findAllWithNoProperty());
            model.addAttribute("genders", Gender.values());            
            return "peopleAdd";
        }
        try {
            peopleService.create(newPeople);
            model.addAttribute("createSuccess", "");
        }
        catch (Exception e) {
            LOG.error(e);
            model.addAttribute("createError", e.getMessage());
        }   
        return list(model);
    }

    @RequestMapping(value = "/people/edit", method = RequestMethod.GET)
    public String editFormGet(@RequestParam("id") Long peopleId, Model model) {
        model.addAttribute("people", peopleService.findById(peopleId));
        model.addAttribute("genders", Gender.values());
        return "people";
    }
    
    @RequestMapping(value = "/people/edit", method = RequestMethod.POST)
    public String editFormProcess(
            @ModelAttribute("people") @Valid People people,              
            BindingResult result, 
            Model model) {
        if (result.hasErrors()) {
            model.addAttribute("people", people);
            model.addAttribute("genders", Gender.values());
            return "people";
        }
        try {
            peopleService.update(people);
            model.addAttribute("updateSuccess", "");
        }
        catch (Exception e) {
            LOG.error(e);
            model.addAttribute("updateError", e.getMessage());
        } 
        return list(model);
    }

    @RequestMapping(value = "/people/delete", method = RequestMethod.GET)
    public String deleteById(@RequestParam("id") Long peopleId, Model model) {
        try {
            People people = peopleService.findById(peopleId);
            peopleService.delete(people);
            model.addAttribute("deleteSuccess", "");
        }
        catch (PersistenceException e){
            LOG.error(e);
            model.addAttribute("deleteError", "this record is in use by lease. Delete the related lease first.");            
        }
        catch (Exception e) {
            LOG.error(e);
            model.addAttribute("deleteError", e.getMessage());
        }
        return list(model);
    }     

    @InitBinder
    public void initialseBinder(WebDataBinder binder) {
        binder.addValidators(peopleValidator);
        binder.setAllowedFields( //TODO complate the white list of fileds
                "*"
        );
    }
}
