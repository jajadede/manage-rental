package com.jajadede.manage.rental.domain.repository;

import com.jajadede.manage.rental.wrapper.RentalActivity;
import java.util.List;

/**
 *
 * @author Jerry
 */
public interface SummaryRepository {
    List<RentalActivity> findByYearMonth(int year, int month);
    /**
     * Find a list of the year-month String within the given years limit. When the given withinYear is not greater than 0, it is considered unlimited.
     * @param withinYears a integer to limit the time from today to search
     * @param desc descending or not
     * @return a list of yyyy-M string list
     */
    List<String> findYearMonthList(int withinYears, boolean desc);
}
