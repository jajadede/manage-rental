package com.jajadede.manage.rental.domain;

/**
 *
 * @author Jerry
 */
public enum LeaseStatus {
    ACTIVE      ("Active"),
    INACTIVE    ("Inactive");
    
    private final String value;
    
    LeaseStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
    
}
