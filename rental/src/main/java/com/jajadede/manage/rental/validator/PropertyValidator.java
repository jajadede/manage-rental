package com.jajadede.manage.rental.validator;

import com.jajadede.manage.rental.domain.Address;
import com.jajadede.manage.rental.domain.Property;
import com.jajadede.manage.rental.domain.Rent;
import com.jajadede.manage.rental.service.AddressService;
import com.jajadede.manage.rental.service.PropertyService;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author Jerry
 */
@Component
public class PropertyValidator implements Validator {

    private static final Logger LOG = LogManager.getLogger(PropertyValidator.class.getName());

    @Autowired
    AddressService addressService;
    @Autowired
    PropertyService propertyService;

    @Override
    public boolean supports(Class<?> clazz) {
        return Property.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Property property = (Property) target;

        // common check
        validateAddress(property, errors);
        validateRents(property, errors);
        validateCode(property, errors);

        // check against db
        validateUniqueAddressAndPropertyCode(property, errors);
        validateUniquePropertyIdAndRentEffectiveDate(property, errors);
    }

    private void validateUniqueAddressAndPropertyCode(Property property, Errors errors) {
        Address dbAddress = addressService.findById(property.getAddress().getId());
        if (dbAddress != null && dbAddress.getProperties() != null) {
            List<Property> dbProperties = dbAddress.getProperties();
            try {
                for (Property p : dbProperties) {
                    if (p.getId().equals(property.getId())) {
                        // if editing, skip itself
                        continue;
                    }
                    LOG.trace("property.code:" + property.getCode() + ", dbProperty.code=" + p.getCode());
                    if (p.getCode().equals(property.getCode())) {
                        errors.rejectValue(
                                "code",
                                "validator.PropertyValidator.UniqueAddressAndPropertyCode.message");
                        LOG.debug("Error found, validator.PropertyValidator.UniqueAddressAndPropertyCode.message is set.");
                    }
                }
            } catch (Exception e) {
                LOG.error(e);
            }
        }
    }

    private void validateUniquePropertyIdAndRentEffectiveDate(Property property, Errors errors) {
        List<Rent> rents = property.getRents();
        if (rents != null) {
            int i = 0;
            for (Rent r : rents) {
                if (Collections.frequency(rents, r) > 1) {
                    errors.rejectValue(
                            "rents[" + i + "].effectiveDate",
                            "validator.PropertyValidator.UniquePropertyIdAndRentEffectiveDate.message");
                    LOG.debug("Error found, validator.PropertyValidator.UniquePropertyIdAndRentEffectiveDate.message is set.");
                }
            }

        }
    }

    private void validateRents(Property property, Errors errors) {
        List<Rent> rents = property.getRents();
        int i = 0;
        for (Rent r : rents) {
            if (r.getRent() == null) {
                // null check
                errors.rejectValue(
                        "rents[" + i + "].rent",
                        "validator.PropertyValidator.RentIsNotNull.message");
                LOG.debug("Error found, validator.PropertyValidator.RentIsNotNull.message is set.");
            }
            if (r.getRent() != null && r.getRent().compareTo(BigDecimal.ZERO) <= 0) {
                errors.rejectValue(
                        "rents[" + i + "].rent",
                        "validator.PropertyValidator.RentIsGreaterThanZero.message");
                LOG.debug("Error found, validator.PropertyValidator.RentIsGreaterThanZero.message is set.");
            }
            if (r.getEffectiveDate() != null) {
                for (Rent otherrent : rents) {
                    if (otherrent == r) {
                        continue; // self
                    }
                    if (otherrent.getEffectiveDate().equals(r.getEffectiveDate())) {
                        errors.rejectValue(
                                "rents[" + i + "].effectiveDate",
                                "validator.PropertyValidator.RentsHasNoDupEffDate.message");
                        LOG.debug("Error found, validator.PropertyValidator.RentsHasNoDupEffDate.message is set.");
                    }
                }
            }
            i++;
        }
    }

    private void validateCode(Property property, Errors errors) {
        if (property.getCode() == null) {
            errors.rejectValue(
                    "code",
                    "validator.PropertyValidator.code.notnull.message");
            LOG.debug("Error found, validator.PropertyValidator.code.notnull.message is set.");
            return;
        }
        if (property.getCode().length() < 1 || property.getCode().length() > 50) {
            errors.rejectValue(
                    "code",
                    "validator.PropertyValidator.code.size.message");
            LOG.debug("Error found, validator.PropertyValidator.code.size.message is set.");
            return;
        }
    }

    private void validateAddress(Property property, Errors errors) {
        String checker = (property.getAddress().getNumber() == null ? "" : property.getAddress().getNumber())
                + property.getAddress().getStreet() == null ? "" : property.getAddress().getStreet();
        if (checker.isEmpty()) {
            errors.rejectValue(
                    "address",
                    "validator.PropertyValidator.address.NumStreetRequired.message");
            LOG.debug("Error found, validator.PropertyValidator.address.NumStreetRequired.message is set.");
        }
    }

}
