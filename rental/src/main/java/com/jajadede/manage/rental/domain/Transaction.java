package com.jajadede.manage.rental.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author Jerry
 */
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class, 
        property = "id")
@Entity
@Table (name = "transaction")
public class Transaction extends AbstractEntity{
    
    @OneToOne(fetch = FetchType.LAZY) @JoinColumn(name = "event_id", foreignKey = @ForeignKey(name = "FK_TRANSACTION_EVENT"))
    private Event event;
    @Column (name = "effective_date") @Temporal(TemporalType.DATE) @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date effectivedate;
    @Column (name = "purpose", length=20) @Enumerated(EnumType.STRING)   
    private TransactionPurpose purpose;
    @Column (name = "amount")
    private BigDecimal amount;
    @Column (name = "method", length=20) @Enumerated(EnumType.STRING)   
    private TransactionMethod method;
    @Column (name = "comment")
    private String comment;

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash = 97 * hash + Objects.hashCode(this.getEffectivedate());
        hash = 97 * hash + Objects.hashCode(this.getPurpose());
        hash = 97 * hash + Objects.hashCode(this.getAmount());
        hash = 97 * hash + Objects.hashCode(this.getMethod());
        hash = 97 * hash + Objects.hashCode(this.getComment());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        // super class checked type already, casting is safe here
        final Transaction other = (Transaction) obj;
        // use method version of accessors for hibernate's proxies efficiency
        if (!Objects.equals(this.getComment(), other.getComment())) {
            return false;
        }
        if (!Objects.equals(this.getEvent(), other.getEvent())) {
            return false;
        }
        if (!Objects.equals(this.getEffectivedate(), other.getEffectivedate())) {
            return false;
        }
        if (this.getPurpose() != other.getPurpose()) {
            return false;
        }
        if (!Objects.equals(this.getAmount(), other.getAmount())) {
            return false;
        }
        if (this.getMethod() != other.getMethod()) {
            return false;
        }
        return true;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Date getEffectivedate() {
        return effectivedate;
    }

    public void setEffectivedate(Date effectivedate) {
        this.effectivedate = effectivedate;
    }

    public TransactionPurpose getPurpose() {
        return purpose;
    }

    public void setPurpose(TransactionPurpose purpose) {
        this.purpose = purpose;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public TransactionMethod getMethod() {
        return method;
    }

    public void setMethod(TransactionMethod method) {
        this.method = method;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    
    @Override
    public String toString() {
        return jsonString();
    }
    
    public String toLongString(boolean includeEffectivedate) {
        StringBuilder sb = new StringBuilder();
        if (includeEffectivedate && effectivedate != null) sb.append(effectivedate).append(" ");
        if (purpose != null) sb.append(purpose.getValue()).append(" ");
        if (amount != null) sb.append("$").append(amount).append(" ");
        if (method != null) sb.append("(").append(method.getValue()).append(") ");
        if (comment != null) sb.append("Comment:").append(comment);
        return sb.toString();         
    }
    
    public String toLongString() {
       return toLongString(true);
    }
    
    public String toShortString() {
        StringBuilder sb = new StringBuilder();
        if (amount != null) {
            sb.append("$");
            if (purpose != null && purpose == TransactionPurpose.REFUND) {
//                sb.append("-"); // expecting business logic making it negative at 
            } else {
                sb.append("+");
            }
            sb.append(amount);
        }
        return sb.toString();
    }
    
    public String jsonString() {
        StringBuilder sb = new StringBuilder();    
        sb.append("{")
                .append("\"id\":\"").append(id).append("\"")
                .append(",").append("\"version\":\"").append(version).append("\"")
                .append(",").append("\"effectivedate\":\"").append(effectivedate).append("\"")
                .append(",").append("\"purpose\":\"").append(purpose).append("\"")
                .append(",").append("\"method\":\"").append(method).append("\"")
                .append(",").append("\"amount\":\"").append(amount).append("\"")
                .append(",").append("\"comment\":\"").append(comment).append("\"")
                ;
        
        // event
        sb.append(",");
        sb.append("\"event\":");
        if (event != null) {
            sb.append("{");
            sb.append("\"id\":\"").append(event.getId()).append("\"");
            sb.append("}");
        }
        else {
            sb.append("null");
        }
        
        sb.append("}").toString();
        return sb.toString();        
    }   
    
    @Override
    public void transferChangeTo(Object t) {
        Transaction other = (Transaction) t;
        other.amount = this.amount;
        other.comment = this.comment;
        other.effectivedate = this.effectivedate;
        other.method = this.method;
        other.purpose = this.purpose;
    }    
}
