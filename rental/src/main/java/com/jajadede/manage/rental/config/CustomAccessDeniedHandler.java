package com.jajadede.manage.rental.config;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.access.AccessDeniedHandler;

/**
 *
 * @author Jerry
 */
public class CustomAccessDeniedHandler implements AccessDeniedHandler {

    private static final Logger LOG = LogManager.getLogger(CustomAccessDeniedHandler.class.getName());

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            LOG.warn("User: " + auth.getName()
                    + " attempted to access the protected URL: "
                    + request.getRequestURI());
        }
        
        URI baseUri;
        String redirectUrl;
        try {
            baseUri = new URI(request.getRequestURI());
            redirectUrl = baseUri.resolve("./list?accessDenied").toString();
        } catch (URISyntaxException ex) {
            LOG.error(ex);
            redirectUrl = request.getContextPath() + "?accessDenied";
        }
        response.sendRedirect(redirectUrl);
    }

}
