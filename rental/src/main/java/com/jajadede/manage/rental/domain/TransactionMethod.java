package com.jajadede.manage.rental.domain;

/**
 *
 * @author Jerry
 */
public enum TransactionMethod {
    CASH            ("Cash"),
    E_TRANSFER      ("E-Transfer"),
    CHEQUE          ("Cheque");
    
    private final String value;
    
    TransactionMethod(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }    
}
