package com.jajadede.manage.rental.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jerry
 */
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class, 
        property = "id")
@XmlRootElement
@Entity
@Table (name = "people")
@NamedQueries({
        @NamedQuery(name=People.PEOPLE_FIND_BY_ID,
                query="select p from People p "
                        + "left join fetch p.address "
                        + "where p.id = :ID")
        ,@NamedQuery(name=People.PEOPLE_FIND_ALL,
                query="select distinct p from People p "
                        + "left join fetch p.address ")
        ,@NamedQuery(name=People.PEOPLE_COUNT_ALL,
                query="select count(p) from People p ")          
})
public class People extends AbstractEntity {
    private static final Logger LOG = LogManager.getLogger(People.class.getName());

    public static final String PEOPLE_FIND_BY_ID = "People.findById";
    public static final String PEOPLE_FIND_ALL = "People.findAll";
    public static final String PEOPLE_COUNT_ALL = "People.countAll";  
    
    @Column (name = "first_name")
    private String firstname;
    @Column (name = "last_name")
    private String lastname;
    @Column (name = "nick_name")
    private String nickname;
    @ManyToOne (cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn (name = "address_id", foreignKey = @ForeignKey(name = "FK_PEOPLE_ADDRESS"))
    private Address address;
    @Enumerated(EnumType.STRING)
    @Column (name = "gender", length=6)
    private Gender gender;
    @Column (name = "nationality")
    private String nationality;
    @Column (name = "position")
    private String position;
    @Column (name = "cell")
    private String cell;
    @Column (name = "email")
    private String email;
    @Column (name = "comment" )
    private String comment;

    public void addAddress(Address address) {
        if (address != null) {
            address.getPeoples().add(this);
            this.setAddress(address);
        }
    }
    
    public void removeAddress() {
        if (this.getAddress() != null) {
            for (People p :this.getAddress().getPeoples()) {
                if (!Objects.equals(p.getId(), this.getId())) {
                    this.getAddress().getPeoples().remove(p);
                    break;
                }
            }
            this.setAddress(null);
        }
    }
    
    public String getFullname() {
        if (lastname == null) lastname = "";
        if (firstname == null) firstname = "";
        if (lastname.isEmpty() && firstname.isEmpty()) {
            return "";
        } else {
            return lastname + ", " + firstname;
        }
    }
    
    public String getLongName() {
        if (nickname != null) return nickname + " " + getFullname();
        return getFullname();
    }
    
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getCell() {
        return cell;
    }

    public void setCell(String cell) {
        this.cell = cell;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash = 29 * hash + Objects.hashCode(this.getFirstname());
        hash = 29 * hash + Objects.hashCode(this.getLastname());
        hash = 29 * hash + Objects.hashCode(this.getNickname());
        hash = 29 * hash + Objects.hashCode(this.getAddress());
        hash = 29 * hash + Objects.hashCode(this.getGender());
        hash = 29 * hash + Objects.hashCode(this.getNationality());
        hash = 29 * hash + Objects.hashCode(this.getPosition());
        hash = 29 * hash + Objects.hashCode(this.getCell());
        hash = 29 * hash + Objects.hashCode(this.getEmail());
        hash = 29 * hash + Objects.hashCode(this.getComment());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        // super class checked type already, casting is safe here
        final People other = (People) obj;
        // use method version of accessors for hibernate's proxies efficiency
        if (!Objects.equals(this.getFirstname(), other.getFirstname())) {
            return false;
        }
        if (!Objects.equals(this.getLastname(), other.getLastname())) {
            return false;
        }
        if (!Objects.equals(this.getNickname(), other.getNickname())) {
            return false;
        }
        if (!Objects.equals(this.getNationality(), other.getNationality())) {
            return false;
        }
        if (!Objects.equals(this.getPosition(), other.getPosition())) {
            return false;
        }
        if (!Objects.equals(this.getCell(), other.getCell())) {
            return false;
        }
        if (!Objects.equals(this.getEmail(), other.getEmail())) {
            return false;
        }
        if (!Objects.equals(this.getComment(), other.getComment())) {
            return false;
        }
        if (!Objects.equals(this.getAddress(), other.getAddress())) {
            return false;
        }
        if (this.getGender() != other.getGender()) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return jsonString();
    }
    
    public String jsonString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{")
                .append("\"id\":\"").append(id).append("\"")
                .append(",").append("\"version\":\"").append(version).append("\"")
                .append(",").append("\"cell\":\"").append(cell).append("\"")
                .append(",").append("\"firstname\":\"").append(firstname).append("\"")
                .append(",").append("\"lastname\":\"").append(lastname).append("\"")
                .append(",").append("\"nickname\":\"").append(nickname).append("\"")
                .append(",").append("\"gender\":\"").append(gender).append("\"")
                .append(",").append("\"email\":\"").append(email).append("\"")
                .append(",").append("\"nationality\":\"").append(nationality).append("\"")
                .append(",").append("\"position\":\"").append(position).append("\"")                
                .append(",").append("\"comment\":\"").append(comment).append("\"");
        
        // address
        sb.append(",");
        sb.append("\"address\":");
        if (address != null) {
            sb.append(address.jsonString());
        }
        else {
            sb.append("null");
        }
        
        sb.append("}").toString();        
        return sb.toString();
    }
    
    @Override
    public void transferChangeTo(Object t) {
        People other = (People) t;
        other.cell = this.cell; 
        other.comment = this.comment;
        other.email = this.email;
        other.firstname = this.firstname;
        other.gender = this.gender;
        other.lastname = this.lastname;
        other.nationality = this.nationality;
        other.nickname = this.nickname;
        other.position = this.position;
    }  
}
