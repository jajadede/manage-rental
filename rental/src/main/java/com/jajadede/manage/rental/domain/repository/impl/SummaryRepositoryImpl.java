package com.jajadede.manage.rental.domain.repository.impl;

import com.jajadede.manage.rental.domain.Action;
import com.jajadede.manage.rental.domain.Event;
import com.jajadede.manage.rental.domain.Event_;
import com.jajadede.manage.rental.domain.Lease;
import com.jajadede.manage.rental.domain.Lease_;
import com.jajadede.manage.rental.domain.Property;
import com.jajadede.manage.rental.domain.Property_;
import com.jajadede.manage.rental.domain.Transaction;
import com.jajadede.manage.rental.domain.TransactionMethod;
import com.jajadede.manage.rental.domain.TransactionPurpose;
import com.jajadede.manage.rental.domain.Transaction_;
import com.jajadede.manage.rental.domain.repository.SummaryRepository;
import com.jajadede.manage.rental.util.HibernateUtil;
import com.jajadede.manage.rental.wrapper.RentalActivity;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Jerry
 */
@Repository ("summaryRepository")
@Transactional
public class SummaryRepositoryImpl implements SummaryRepository {
    private static final Logger LOG = LogManager.getLogger(SummaryRepositoryImpl.class.getName());
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    @Transactional(readOnly = true)
    public List<RentalActivity> findByYearMonth(int year, int month) {
        EntityManager em = sessionFactory.createEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<RentalActivity> criteria = builder.createQuery(RentalActivity.class);
        Root<Lease> root = criteria.from(Lease.class);
        Join<Lease, Property> propertyJoin = root.join(Lease_.property);
//        Join<Lease, People> tenantsJoin = root.join(Lease_.tenantsSet);
        Join<Lease, Event> eventsJoin = root.join(Lease_.eventsSet);
        Join<Event, Transaction> transactionJoin = eventsJoin.join(Event_.transaction);

        Path<Long> propertyId = propertyJoin.get(Property_.ID);
        Path<Long> leaseId = root.get(Lease_.id);
        Path<Date> actionDate = eventsJoin.get(Event_.actiondate);
        Path<Action> action = eventsJoin.get(Event_.action);
        Path<TransactionPurpose> purpose = transactionJoin.get(Transaction_.purpose);
        Path<Date> effectiveDate = transactionJoin.get(Transaction_.effectivedate);
        Path<BigDecimal> amount = transactionJoin.get(Transaction_.amount);
        Path<TransactionMethod> method = transactionJoin.get(Transaction_.method);

        // Long propertyId, Long leaseId, Date actionDate, Action action, TransactionPurpose purpose, Date effectiveDate, BigDecimal amount, TransactionMethod method
        criteria.select(builder.construct(RentalActivity.class, propertyId, leaseId, actionDate, action, purpose, effectiveDate, amount, method));
        criteria.where(builder.and(
                builder.equal(builder.function("YEAR", Integer.class, effectiveDate), year),
                builder.equal(builder.function("MONTH", Integer.class, effectiveDate), month) ));
        
        List<Order> orderList = new ArrayList();
        orderList.add(builder.asc(propertyId));
        orderList.add(builder.asc(leaseId));
        orderList.add(builder.asc(effectiveDate));
        orderList.add(builder.asc(actionDate));
        criteria.orderBy(orderList);
        
        List<RentalActivity> rentalSummary = em.createQuery(criteria).getResultList();
        return rentalSummary;
    }

    @Override
    @Transactional(readOnly = true)
    public List<String> findYearMonthList(int withinYears, boolean desc) {
        final boolean noLimit;
        final Date limit;
        if (withinYears > 0) {
            noLimit = false;
            Calendar c = Calendar.getInstance();
            c.add(Calendar.YEAR, -withinYears);
            limit = c.getTime();
        }
        else {
            noLimit = true;
            limit = null;
        }
        LOG.debug("Limit withinYears: " + withinYears + ", limit="+ limit);
        List<String> summary = (List<String>) HibernateUtil.doWithinTransaction(sessionFactory, session -> {
            Query query = session.createQuery(""
                    + "select concat(year(t.effectivedate),'-',month(t.effectivedate)), t.effectivedate "
                    + "from Transaction t "
                    + (noLimit ? "" : "where t.effectivedate >= :limit ")
                    + "group by concat(year(t.effectivedate),'-',month(t.effectivedate)) "
                    + "order by t.effectivedate "
                    + (desc ? "desc" : "") );
            if (!noLimit){
                query.setParameter("limit", limit);
            }
             List<Object[]> result = query.list();
            return result.stream().map(a -> {return a[0];}).collect(Collectors.toList());
        });
        
        return summary;
    }
    
}
