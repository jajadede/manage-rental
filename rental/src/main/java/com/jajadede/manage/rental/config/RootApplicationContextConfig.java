/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.config;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import javax.sql.DataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author Jerry
 */
@Configuration
@ComponentScan("com.jajadede.manage.rental")
@EnableTransactionManagement
//@PropertySource({ "classpath:hibernate.properties" })
@Profile("dev")
public class RootApplicationContextConfig {

    private static final Logger LOG = LogManager.getLogger(RootApplicationContextConfig.class.getName());
    private static final String HIBERNATE_PROPERTIES_FILE = "hibernate.properties";
    
    private DataSource dataSource;
    private SessionFactory sessionFactory;
    private PlatformTransactionManager platformTransactionManager;
    private Properties properties;
            
    /// Hibernate config start
    @Bean
    public SessionFactory sessionFactory() throws IOException  {
        if (sessionFactory != null) return sessionFactory;
        LOG.debug("start building sessionFactory...");
        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
        sessionFactoryBean.setDataSource(dataSource()); 
        sessionFactoryBean.setPackagesToScan("com.jajadede.manage.rental.domain");
        sessionFactoryBean.setHibernateProperties(hibernateProperties());
        sessionFactoryBean.afterPropertiesSet();
        SessionFactory sf = sessionFactoryBean.getObject();
        LOG.debug("end building sessionFactory...");
        sessionFactory = sf;
        return sessionFactory;
    }
 
    @Bean
    public PlatformTransactionManager hibernateTransactionManager() throws IOException {
        if (platformTransactionManager != null) return platformTransactionManager;
        platformTransactionManager = new HibernateTransactionManager(sessionFactory());
        return platformTransactionManager;
    }
       
    @Bean
    public DataSource dataSource() {
        if (dataSource != null) return dataSource;
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setUrl(hibernateProperties().getProperty("jdbc.url"));
        ds.setDriverClassName(hibernateProperties().getProperty("jdbc.driverClassName"));
        ds.setUsername(hibernateProperties().getProperty("jdbc.username"));
        ds.setPassword(hibernateProperties().getProperty("jdbc.password"));
        
        // TODO remove the auto db refresh after test
        Connection connection = null;
        try {
            connection = ds.getConnection();
//            ScriptUtils.executeSqlScript(connection, new EncodedResource(new ClassPathResource("db/sql/mysql-create-table.sql"), StandardCharsets.UTF_8));
//            ScriptUtils.executeSqlScript(connection, new EncodedResource(new ClassPathResource("db/sql/mysql-insert-data.sql"), StandardCharsets.UTF_8));
        } catch (SQLException ex) {
            LOG.error(ex);
        } finally {
            if (connection != null) try {
                connection.close();
            } catch (SQLException ex) {
                LOG.error(ex);
            }
        } 
        dataSource = ds;
        return dataSource;
    }
    
    private Properties hibernateProperties(){
        if (properties != null) return properties;
        Properties hibernateProperties = new Properties();
        try (InputStream stream = new ClassPathResource(HIBERNATE_PROPERTIES_FILE).getInputStream()) {
            hibernateProperties.load(stream);
        }
        catch(Exception e) {
            LOG.error(e);
        }
        properties = hibernateProperties;
        LOG.info("Finished loading file: " + HIBERNATE_PROPERTIES_FILE);
        return properties;
    }
    /// Hibernate config end
            

//    /// in mememory db
//        @Bean
//    public DataSource dataSource() {
//        try {
//            EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
//            EmbeddedDatabase db = builder
//                            .setType(EmbeddedDatabaseType.H2)
////                            .addScript("db/sql/create-table.sql")
////                            .addScript("db/sql/insert-data.sql")
//                            .build();
//            LOG.debug("dataSource bean is up " + db);
//            return db;
//        } catch (Exception e) {
//            LOG.error("Failed to create datasource bean", e);
//        }
//
//        return null;
//    }
//    
//    private Properties hibernateProperties(){
//        Properties hibernateProperties = new Properties();
//        hibernateProperties.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
//        hibernateProperties.put("hibernate.hbm2ddl.auto", "create-drop");
////        hibernateProperties.put("hibernate.show_sql", true);
//        hibernateProperties.put("hibernate.max_fetch_depth", 3);
//        hibernateProperties.put("hibernate.jdbc.batch_size", 10);
//        hibernateProperties.put("hibernate.jdbc.fetch_size", 50);
//        hibernateProperties.put("hibernate.current_session_context_class", "org.hibernate.context.internal.ThreadLocalSessionContext");
//        return hibernateProperties;
//    }     
}
