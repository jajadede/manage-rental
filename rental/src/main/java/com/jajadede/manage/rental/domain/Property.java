/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.SortNatural;

/**
 *
 * @author Jerry
 */
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class, 
        property = "id")
@XmlRootElement
@Entity
@Table(name="property",
        uniqueConstraints=
        @UniqueConstraint(columnNames={"ADDRESS_ID", "CODE"}, name = "UQ_PROPERTY_ADDRESS"))
@NamedQueries({
        @NamedQuery(name=Property.PROPERTY_FIND_BY_ID,
                query="select p from Property p "
                        + "left join fetch p.rents "
                        + "left join fetch p.address "
                        + "where p.id = :ID")
        ,@NamedQuery(name=Property.PROPERTY_FIND_ALL,
                query="select distinct p from Property p "
                        + "left join fetch p.rents "
                        + "left join fetch p.address ")
        ,@NamedQuery(name=Property.PROPERTY_COUNT_ALL,
                query="select count(p) from Property p ")          
})
public class Property extends AbstractEntity {

    private static final Logger LOG = LogManager.getLogger(Property.class.getName());
    
    public static final String PROPERTY_FIND_BY_ID = "Property.findById";
    public static final String PROPERTY_FIND_ALL = "Property.findAll";
    public static final String PROPERTY_COUNT_ALL = "Property.countAll";

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name="address_id", nullable=false, foreignKey = @ForeignKey(name = "FK_PROPERTY_ADDRESS"))
    private Address address;
    @Column(name="code", length=50)
    private String code;
    @Column(name="description", length=250)
    private String description;
    @OneToMany(mappedBy="property", cascade=CascadeType.ALL, orphanRemoval=true) @SortNatural   
    private List<Rent> rents = new ArrayList<>();

    public List<Rent> getRents() {
        return rents;
    }

    public void setRents(List<Rent> rents) {
        this.rents = rents;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash = 17 * hash + Objects.hashCode(getAddress());
        hash = 17 * hash + Objects.hashCode(getCode());
        hash = 17 * hash + Objects.hashCode(getDescription());
        hash = 17 * hash + Objects.hashCode(getRents());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        // super class checked type already, casting is safe here
        final Property other = (Property) obj;
        // use method version of accessors for hibernate's proxies efficiency        
        if (!Objects.equals(this.getCode(), other.getCode())) {
            return false;
        }
        if (!Objects.equals(this.getDescription(), other.getDescription())) {
            return false;
        }
        if (!Objects.equals(this.getAddress(), other.getAddress())) {
            return false;
        }
        return true;
    }

    public String toShortString() {
        StringBuilder sb = new StringBuilder();
        if (code != null) sb.append(code).append(" ");
        if (address != null) sb.append(address.toShortString());
        return sb.toString();
    }

    public String toLongString() {
        StringBuilder sb = new StringBuilder();
        if (description != null) sb.append(description).append(" ");
        if (code != null) sb.append("(").append(code).append(") ");
        if (address != null) sb.append(address.toString());
        return sb.toString();
    }    
    
    @Override
    public String toString() {
        return jsonString();
    }
    
    public String jsonString() {
        StringBuilder sb = new StringBuilder();    
        sb.append("{")
                .append("\"id\":\"").append(id).append("\"")
                .append(",").append("\"version\":\"").append(version).append("\"")
                .append(",").append("\"code\":\"").append(code).append("\"")
                .append(",").append("\"description\":\"").append(description).append("\"");
        
        // address
        sb.append(",");
        sb.append("\"address\":");
        if (address != null) {
            sb.append(address.jsonString());
        }
        else {
            sb.append("null");
        }
         
        //rents        
        sb.append(",");
        sb.append("\"rents\":");        
        if (rents != null) {
            try {
                rents.iterator();
                sb.append("[");
                boolean isFirst = true;
                for (Rent entry : rents) {
                    if (!isFirst) {
                        sb.append(",");
                    }
                    sb.append(entry.jsonString());           
                    isFirst = false;
                }
                sb.append("]");
            } catch (Exception e) {
                LOG.error("tenants is not ready.");
                sb.append("\"lazily initialized\"");
            }            
        } else {
            sb.append("null");
        }        
                
        sb.append("}").toString();
        return sb.toString();        
    }    
    
    @Override
    public void transferChangeTo(Object t) {
        Property other = (Property) t;
        other.code = this.code;
        other.description = this.description;
        this.address.transferChangeTo(other.address);
        if (rents != null) {
            mergeRentsChangesTo(other);
        }
    }
    
    private void mergeRentsChangesTo(Property other) {
        Map<Long, Rent> rentsMap = new TreeMap<>();
        Map<Long, Rent> otherRentsMap = new TreeMap<>();
        for (Rent rent: rents) {
            rentsMap.put(rent.getId(), rent);
        }
        for (Rent rent: other.rents) {
            otherRentsMap.put(rent.getId(), rent);
        }
        
        for(Long otherKey : otherRentsMap.keySet()) {
            Rent rent = rentsMap.get(otherKey);
            Rent otherRent = otherRentsMap.get(otherKey);
            if (rent == null) {
                // rent is removed
                otherRentsMap.remove(otherKey);
            }
            else {
                // copy changes over, remove after to shorten the map
                rent.transferChangeTo(otherRent);
                rentsMap.remove(otherKey);
            }
        }
        // add the remianing new rents to other othermap
        if (rentsMap.size()>0) {
            otherRentsMap.putAll(rentsMap);
        }
    }
}
