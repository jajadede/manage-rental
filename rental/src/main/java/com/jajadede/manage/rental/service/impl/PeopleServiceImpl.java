/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.service.impl;

import com.jajadede.manage.rental.domain.People;
import com.jajadede.manage.rental.domain.repository.PeopleRepository;
import com.jajadede.manage.rental.service.PeopleService;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Jerry
 */
@Service ("peopleService")
public class PeopleServiceImpl implements PeopleService {
    private static final Logger LOG = LogManager.getLogger(PeopleServiceImpl.class.getName());
    
    @Autowired
    private PeopleRepository peopleRepository;
     
    @Override
    public void update(People people) {
        peopleRepository.update(people);
    }

    @Override
    public List<People> findAll() {
        return peopleRepository.findAll();
    }

    @Override
    public People findById(Long id) {
        return peopleRepository.findById(id);
    }

    @Override
    public People create(People people) {
        peopleRepository.create(people);      
        return people;
    }

    @Override
    public void delete(People people) {
        peopleRepository.delete(people);
    }

    @Override
    public Long countAll() {
        return peopleRepository.countAll();
    }

    @Override
    public List<People> findAllWithNoActiveLease() {
        return peopleRepository.findAllWithNoActiveLease();
    }

    @Override
    public Long countAllWithNoActiveLease() {
        return peopleRepository.countAllWithNoActiveLease();
    }
  
}
