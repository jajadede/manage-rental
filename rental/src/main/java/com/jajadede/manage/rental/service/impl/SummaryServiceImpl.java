package com.jajadede.manage.rental.service.impl;

import com.jajadede.manage.rental.domain.repository.SummaryRepository;
import com.jajadede.manage.rental.service.SummaryService;
import com.jajadede.manage.rental.wrapper.RentalActivity;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Jerry
 */
@Service("summaryService")
public class SummaryServiceImpl implements SummaryService {
    private static final Logger LOG = LogManager.getLogger(SummaryServiceImpl.class.getName());
    
    @Autowired
    SummaryRepository summaryRepository;
    
    @Override
    public List<RentalActivity> findByYearMonth(int year, int month) {
        return summaryRepository.findByYearMonth(year, month);
    }

    @Override
    public List<String> findYearMonthList(int withinYears, boolean desc) {
        return summaryRepository.findYearMonthList(withinYears, desc);
    }
    
}
