/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.domain.repository;

import com.jajadede.manage.rental.domain.People;
import java.util.List;

/**
 *
 * @author Jerry
 */
public interface PeopleRepository {
    People findById(Long id);
    List <People> findAll();
    People create(People people);
    void update(People people);
    void delete(People people);
    Long countAll();
    List <People> findAllWithNoActiveLease();
    Long countAllWithNoActiveLease();
}
