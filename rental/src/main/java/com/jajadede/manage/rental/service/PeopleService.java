/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.service;

import com.jajadede.manage.rental.domain.People;
import com.jajadede.manage.rental.domain.Rent;
import java.util.List;

/**
 *
 * @author Jerry
 */
public interface PeopleService {
    List <People> findAll();
    People findById(Long id);
    People create(People people);
    void update(People people);
    void delete(People people);
    Long countAll();  
    List <People> findAllWithNoActiveLease();
    Long countAllWithNoActiveLease();    
}
