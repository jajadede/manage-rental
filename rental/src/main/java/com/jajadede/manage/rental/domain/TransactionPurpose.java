package com.jajadede.manage.rental.domain;

/**
 *
 * @author Jerry
 */
public enum TransactionPurpose {
    RENT            ("Rent"),
    DEPOSIT         ("Deposit"),
    MAINTENANCE     ("Maintenance"),
    REFUND          ("Refund");
    
    private final String value;
    
    TransactionPurpose(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }     
}
