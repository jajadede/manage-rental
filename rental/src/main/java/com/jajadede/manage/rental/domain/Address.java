/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
@XmlRootElement
@Entity
@Table(name = "address")
@NamedQueries({
    @NamedQuery(name = Address.ADDRESS_FIND_BY_ID,
            query = "select a from Address a "
                    + "left join fetch a.properties "
                    + "where a.id =: ID")
    ,@NamedQuery(name = Address.ADDRESS_FIND_ALL,
            query = "select a from Address a ")
    ,@NamedQuery(name = Address.ADDRESS_COUNT_LONELY,
            query = "select count(a) from Address a "
                    + "left join a.properties prop "
                    + "left join a.peoples ppl "                    
                    + "where prop is null and ppl is null ")    
})
public class Address extends AbstractEntity {

    private static final Logger LOG = LogManager.getLogger(Address.class.getName());

    public static final String ADDRESS_FIND_BY_ID = "Address.findById";
    public static final String ADDRESS_FIND_ALL = "Address.findAll";
    public static final String ADDRESS_COUNT_LONELY = "Address.countLonely";

    @Column(name = "city", length = 25)
    private String city;
    @Column(name = "number", length = 10)
    private String number;
    @Column(name = "postcode", length = 10)
    private String postcode;
    @Column(name = "province", length = 10)
    private String province;
    @Column(name = "street", length = 25)
    private String street;
    @Column(name = "unit", length = 10)
    private String unit;
//    @Fetch(FetchMode.JOIN)
    @OneToMany(mappedBy = "address", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Property> properties = new ArrayList<>();   
    @OneToMany(mappedBy = "address", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<People> peoples = new ArrayList<>();

    public List<People> getPeoples() {
        return peoples;
    }

    public void setPeoples(List<People> peoples) {
        this.peoples = peoples;
    }

    @XmlTransient
    public List<Property> getProperties() {
        return properties;
    }

    public void setProperties(List<Property> properties) {
        this.properties = properties;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash = 83 * hash + Objects.hashCode(getCity());
        hash = 83 * hash + Objects.hashCode(getNumber());
        hash = 83 * hash + Objects.hashCode(getPostcode());
        hash = 83 * hash + Objects.hashCode(getProvince());
        hash = 83 * hash + Objects.hashCode(getStreet());
        hash = 83 * hash + Objects.hashCode(getUnit());
        hash = 83 * hash + Objects.hashCode(getProperties());
        hash = 83 * hash + Objects.hashCode(getPeoples());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        // super class checked type already, casting is safe here
        final Address other = (Address) obj;
        // use method version of accessors for hibernate's proxies efficiency
        if (!Objects.equals(this.getCity(), other.getCity())) {
            return false;
        }
        if (!Objects.equals(this.getNumber(), other.getNumber())) {
            return false;
        }
        if (!Objects.equals(this.getPostcode(), other.getPostcode())) {
            return false;
        }
        if (!Objects.equals(this.getProvince(), other.getProvince())) {
            return false;
        }
        if (!Objects.equals(this.getStreet(), other.getStreet())) {
            return false;
        }
        if (!Objects.equals(this.getUnit(), other.getUnit())) {
            return false;
        }
        return true;
    }

    public boolean equalsByValue(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (obj instanceof Address) {
            Address other = (Address) obj;
            return this.toString().equals(other.toString());
        }
        return false;
    }

    public String toShortString() {
        StringBuilder sb = new StringBuilder();
        if (unit != null && !unit.isEmpty()) {
            sb.append(unit).append("-");
        }
        if (number != null && !number.isEmpty()) {
            sb.append(number);
        }
        if (street != null && !street.isEmpty()) {
            sb.append(" ").append(street.split(" ")[0]);
        }
        return sb.toString();
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(this.toShortString());

        if (city != null && !city.isEmpty()) {
            sb.append(", ").append(city);
        }
        if (province != null && !province.isEmpty()) {
            sb.append(", ").append(province);
        }
        if (postcode != null && !postcode.isEmpty()) {
            sb.append(", ").append(postcode);
        }

        return sb.toString();
    }

    public String jsonString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{")
                .append("\"id\":\"").append(id).append("\"")
                .append(",").append("\"version\":\"").append(version).append("\"")
                .append(",").append("\"unit\":\"").append(unit).append("\"")
                .append(",").append("\"number\":\"").append(number).append("\"")
                .append(",").append("\"street\":\"").append(street).append("\"")
                .append(",").append("\"city\":\"").append(city).append("\"")
                .append(",").append("\"postcode\":\"").append(postcode).append("\"")
                .append(",").append("\"province\":\"").append(province).append("\"");

        //properties
        sb.append(",");
        sb.append("\"properties\":");
        if (properties != null) {
            try {
                properties.iterator();
                sb.append("[");
                boolean isFirst = true;
                for (Property property : properties) {
                    if (!isFirst) {
                        sb.append(",");
                    }
                    sb.append("{");
                    sb.append("\"id\":\"").append(property.getId()).append("\"");
                    sb.append("}");
                    isFirst = false;
                }
                sb.append("]");
            } catch (Exception e) {
                LOG.error("properties is not ready.");
                sb.append("\"lazily initialized\"");
            }            
        } else {
            sb.append("null");
        }
        // peoples
        sb.append(",");
        sb.append("\"peoples\":");
        if (peoples != null) {
            try {
                peoples.iterator();
                sb.append("[");
                boolean isFirst = true;
                for (People p : peoples) {
                    if (!isFirst) {
                        sb.append(",");
                    }
            sb.append("{");
                    sb.append("\"id\":\"").append(p.getId()).append("\"");
            sb.append("}");           
                    isFirst = false;
                }
                sb.append("]");
            } catch (Exception e) {
                LOG.error("peoples is not ready.");
                sb.append("\"lazily initialized\"");
            }            
        } else {
            sb.append("null");
        }        
        sb.append("}").toString();
        return sb.toString();
    }

    @Override
    public void transferChangeTo(Object t) {
        Address other = (Address) t;
        other.city = this.city;
        other.number = this.number;
        other.postcode = this.postcode;
        other.province = this.province;
        other.street = this.street;
        other.unit = this.unit;
    }
}
