/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.domain.repository;

import com.jajadede.manage.rental.domain.Rent;
import java.util.List;

/**
 *
 * @author Jerry
 */
public interface RentRepository {
    List <Rent> findAll();
    Rent findById(Long id);
    Rent create(Rent rent);
    void update(Rent rent);
    void delete(Rent rent);
//    List<Rent> findAllRelatedByOneId(Long id);
    int findAllSizeRelatedByOneId(Long id);
}
