/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.jajadede.manage.rental.util.HibernateUtil;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author Jerry
 */

@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class, 
        property = "id")
@XmlRootElement
@Entity
@Table(name="rent",
        uniqueConstraints=
        @UniqueConstraint(columnNames={"PROPERTY_ID", "EFFECTIVE_DATE"}, name = "UQ_RENT_PROPERTY"))
@NamedQueries({
        @NamedQuery(name=Rent.RENT_FIND_BY_ID,
                query="select r from Rent r "
                        + "where r.id = :ID")   
})
public class Rent extends AbstractEntity implements Comparable<Rent> {
    
    private static final Logger LOG = LogManager.getLogger(Rent.class.getName());
    public static final String RENT_FIND_BY_ID = "Rent.findById";

    @Fetch(FetchMode.JOIN)
    @ManyToOne
    @JoinColumn(name="property_id", foreignKey = @ForeignKey(name = "FK_RENT_PROPERTY"))
    private Property property;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    @Column(name="effective_date")
    private Date effectiveDate;
    @Column(name="rent")
    private BigDecimal rent;

    @XmlTransient
    public Property getProperty() {
        return property;
    }

    public void setProperty(Property property) {
        this.property = property;
    }


    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public BigDecimal getRent() {
        return rent;
    }

    public void setRent(BigDecimal rent) {
        this.rent = rent;
    }

    @Override
    public String toString() {
        return "Effective " + effectiveDate + ", rent " + rent;
    }
    
    public String toShortString() {
        return effectiveDate + " $" + rent;
    }    
    
    public String jsonString() {
        StringBuilder sb = new StringBuilder();    
        sb.append("{")
                .append("\"id\":\"").append(id).append("\"")
                .append(",").append("\"version\":\"").append(version).append("\"")
                .append(",").append("\"rent\":\"").append(rent).append("\"")
                .append(",").append("\"effectiveDate\":\"").append(effectiveDate).append("\"");
         
        //property
        sb.append(",");
        sb.append("\"property\":");
        if (property != null) {
            sb.append("{");
            sb.append("\"id\":\"").append(property.getId()).append("\"");
            sb.append("}");
        }
        else {
            sb.append("null");
        }
        sb.append("}").toString();
        return sb.toString();
    }
    
    @Override
    public void transferChangeTo(Object t) {
        Rent other = (Rent) t;
        other.rent = this.rent;
        other.effectiveDate = this.effectiveDate;
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash = 89 * hash + Objects.hashCode(getProperty());
        hash = 89 * hash + Objects.hashCode(getEffectiveDate());
        hash = 89 * hash + Objects.hashCode(getRent());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)){
            return false;
        }
        // super class checked type already, casting is safe here
        final Rent other = (Rent) obj;
        // use method version of accessors for hibernate's proxies efficiency
        if (!Objects.equals(this.getEffectiveDate(), other.getEffectiveDate())) {
            return false;
        }
        if (!Objects.equals(this.getRent(), other.getRent())) {
            return false;
        }
        if (!Objects.equals(this.getProperty(), other.getProperty())) {
            return false;
        }        
        return true;
    }

    @Override
    public int compareTo(Rent o) {
        return HibernateUtil.compareDates(this.getEffectiveDate(), o.getEffectiveDate());
    }
    
}
