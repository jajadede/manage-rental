package com.jajadede.manage.rental.validator;

import com.jajadede.manage.rental.domain.People;
import com.jajadede.manage.rental.service.AddressService;
import com.jajadede.manage.rental.service.PeopleService;
import java.util.regex.Pattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author Jerry
 */
@Component
public class PeopleValidator implements Validator {

    private static final Logger LOG = LogManager.getLogger(PeopleValidator.class.getName());

    public static final String EMAIL_PATTERN = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
    
    @Autowired
    AddressService addressService;
    @Autowired
    PeopleService peopleService;

    @Override
    public boolean supports(Class<?> clazz) {
        return People.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        People people = (People) target;

        // common check
        validateNickname(people, errors);
        validateEmail(people, errors);
        validateEitherEmailOrPhoneIsNotEmpty(people, errors);
        
        // check against db
    }

    private void validateNickname(People people, Errors errors) {
        String checker = (people.getNickname()== null ? "" : people.getAddress().getNumber());
        if (people.getNickname()== null || people.getNickname().isEmpty()) {
            errors.rejectValue(
                    "nickname",
                    "validator.PeopleValidator.nickname.NotEmpty.message");
            LOG.debug("Error found, validator.PeopleValidator.nickname.NotEmpty.message is set.");
        }
    }

    private void validateEmail(People people, Errors errors) {
        if (people.getEmail() != null && !people.getEmail().isEmpty()
                && !Pattern.matches(EMAIL_PATTERN,people.getEmail())) {
            errors.rejectValue(
                    "email",
                    "validator.PeopleValidator.email.message");
            LOG.debug("Error found, validator.PeopleValidator.email.message is set.");
        }
    }

    private void validateEitherEmailOrPhoneIsNotEmpty(People people, Errors errors) {
        String checker = (people.getEmail()== null ? "" : people.getEmail())
                + (people.getCell() == null ? "" : people.getCell());
        if (checker.isEmpty()) {
            errors.rejectValue(
                    "email",
                    "validator.PeopleValidator.EitherEmailOrPhoneIsNotEmpty.message");
            errors.rejectValue(
                    "cell",
                    "validator.PeopleValidator.EitherEmailOrPhoneIsNotEmpty.message");
            LOG.debug("Error found, validator.PeopleValidator.EitherEmailOrPhoneIsNotEmpty.message is set.");            
        }
    }    
}
