package com.jajadede.manage.rental.wrapper;

/**
 *
 * @author Jerry
 */
public class SummaryChoice {
    private String yearMonth;

    public String getYearMonth() {
        return yearMonth;
    }

    public void setYearMonth(String yearMonth) {
        this.yearMonth = yearMonth;
    }
    
    
}
