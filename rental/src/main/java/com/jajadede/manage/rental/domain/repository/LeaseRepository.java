package com.jajadede.manage.rental.domain.repository;

import com.jajadede.manage.rental.domain.Event;
import com.jajadede.manage.rental.domain.Lease;
import java.util.List;

/**
 *
 * @author Jerry
 */
public interface LeaseRepository {
    Lease findById(Long id);
    List <Lease> findAll();
    Lease create(Lease lease);
    void update(Lease lease);
    void delete(Lease lease);
    Long countAll();
    void deleteEventById(Long eventId);
    void addEvent(Event event);
    /**
     * Delete the event and also update the status of the related lease
     * @param event 
     */
    void deleteEvent(Event event);
    Event findEventById(Long eventId);
}
