/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.service;

import com.jajadede.manage.rental.domain.Address;
import java.util.List;

/**
 *
 * @author Jerry
 */
public interface AddressService {
    List <Address> findAll();
    Address findById(Long id);
    Address create(Address address);
    void update(Address address);
    void delete(Address address);
    Long countLonely();
    List <Address> findAllWithNoPeople();
    List <Address> findAllWithNoProperty();  
    Long countAllWithNoPeople();
    Long countAllWithNoProperty();    
}
