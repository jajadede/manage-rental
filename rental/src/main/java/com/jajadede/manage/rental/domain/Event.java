/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.jajadede.manage.rental.util.HibernateUtil;
import java.util.Date;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author Jerry
 */
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class, 
        property = "id")
@Entity
@Table (name = "event")
public class Event extends AbstractEntity implements Comparable<Event> {
    
    @ManyToOne @JoinColumn(name="lease_id", foreignKey = @ForeignKey(name = "FK_EVENT_LEASE"))
    private Lease lease;    
    @Column (name = "action", length=20) @Enumerated(EnumType.STRING)   
    private Action action;
    @Column (name = "action_date") @Temporal(TemporalType.DATE) @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date actiondate;
    @OneToOne(mappedBy = "event", cascade = CascadeType.ALL,orphanRemoval = true/*,fetch = FetchType.LAZY*/) 
    private Transaction transaction;

    @Override
    public int compareTo(Event other) {
        return HibernateUtil.compareDates(this.getActiondate(), other.getActiondate());
    }
    
    public void addTransaction(Transaction transaction) {
        transaction.setEvent(this);
        this.transaction = transaction;
    }

    public void removeTransaction() {
        if (this.transaction != null) {
            this.transaction.setEvent(null);
            this.transaction = null;
        }
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash = 17 * hash + Objects.hashCode(this.getAction());
        hash = 17 * hash + Objects.hashCode(this.getActiondate());
        hash = 17 * hash + Objects.hashCode(this.getTransaction());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        // super class checked type already, casting is safe here
        final Event other = (Event) obj;
        // use method version of accessors for hibernate's proxies efficiency
        if (!Objects.equals(this.getLease(), other.getLease())) {
            return false;
        }
        if (this.getAction() != other.getAction()) {
            return false;
        }
        if (!Objects.equals(this.getActiondate(), other.getActiondate())) {
            return false;
        }
        if (!Objects.equals(this.getTransaction(), other.getTransaction())) {
            return false;
        }
        return true;
    }

    public Lease getLease() {
        return lease;
    }

    public void setLease(Lease lease) {
        this.lease = lease;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public Date getActiondate() {
        return actiondate;
    }

    public void setActiondate(Date actiondate) {
        this.actiondate = actiondate;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }
    
    @Override
    public String toString() {       
        return jsonString();
    }

    public String toLongString() {
        StringBuilder sb = new StringBuilder();
        if (actiondate != null) sb.append(actiondate).append(" ");        
        if (action != null) sb.append(action.getKey()).append(" ");
        if (transaction != null) sb.append(transaction.toLongString(false));       
        return sb.toString(); 
    }
    
    public String toShortString() {
        StringBuilder sb = new StringBuilder();
        if (actiondate != null) sb.append(actiondate).append(" ");
        if (action != null) sb.append(action.getKey()).append(" ");
//        if (transaction != null) sb.append(transaction.toShortString());
        return sb.toString();
    }
    
    public String jsonString() {
        StringBuilder sb = new StringBuilder();    
        sb.append("{")
                .append("\"id\":\"").append(id).append("\"")
                .append(",").append("\"version\":\"").append(version).append("\"")
                .append(",").append("\"action\":\"").append(action).append("\"")
                .append(",").append("\"actionDate\":\"").append(actiondate).append("\"");
        
        // lease
        sb.append(",");
        sb.append("\"lease\":");
        if (lease != null) {
            sb.append("{");
            sb.append("\"id\":\"").append(lease.getId()).append("\"");
            sb.append("}");
        }
        else {
            sb.append("null");
        }
         
        //transaction
        sb.append(",");
        sb.append("\"transaction\":");
        if (transaction != null) {
            sb.append(transaction.jsonString());
        }
        else {
            sb.append("null");
        }
        
        sb.append("}").toString();
        return sb.toString();        
    }   
    
    @Override
    public void transferChangeTo(Object t) {
        Event other = (Event) t;
        other.action = this.action;
        other.actiondate = this.actiondate;
        getTransaction().transferChangeTo(other.getTransaction());
    }     
}
