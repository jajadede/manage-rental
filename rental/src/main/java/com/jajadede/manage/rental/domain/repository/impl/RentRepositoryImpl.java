/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.domain.repository.impl;

import com.jajadede.manage.rental.domain.Property;
import com.jajadede.manage.rental.domain.Rent;
import com.jajadede.manage.rental.domain.repository.RentRepository;
import com.jajadede.manage.rental.util.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Jerry
 */
@Repository("rentRepository")
@Transactional
public class RentRepositoryImpl implements RentRepository {
    private static final Logger LOG = LogManager.getLogger(RentRepositoryImpl.class.getName());
    
    @Autowired
    private SessionFactory sessionFactory; 
    
    @Override
    @Transactional(readOnly = true)    
    public List<Rent> findAll() {
        Query query = sessionFactory.getCurrentSession().getNamedQuery("SELECT * FROM rent");
        List<Rent> result;
        try {
            List queryResult = query.list();
         result = (List<Rent>) queryResult;   
        } 
        catch (Exception e) {
            LOG.error(e);
            result = new ArrayList<>();
        }
        LOG.debug("Got result from querying all rents: " + result.toString());
        return result;        
    }

    @Override
    @Transactional(readOnly = true)    
    public Rent findById(Long id) {
        return (Rent) sessionFactory.getCurrentSession().getNamedQuery(Rent.RENT_FIND_BY_ID).setParameter("ID", id).uniqueResult();
    }

    @Override
    public Rent create(Rent rent) {
        sessionFactory.getCurrentSession().save(rent);        
        LOG.debug("Created rent with new id: " + rent.getId());
        return rent;    
    }

    @Override
    public void update(Rent rent) {
        sessionFactory.getCurrentSession().merge(rent);
        LOG.debug("Updated rent with id: " + rent.getId());    
    }

    @Override
    public void delete(Rent rent) {
        HibernateUtil.doWithinTransaction(sessionFactory, session ->{
            Rent deletingRent = session.get(Rent.class, rent.getId());
            session.delete(deletingRent);
            return null; 
        });
        LOG.debug("Deleted rent with id: " + rent.getId());
    }

//    @Override
//    public List<Rent> findAllRelatedByOneId(Long id) {
//        List<Rent> rents = (List<Rent>)HibernateUtil.doWithinTransaction(sessionFactory, session ->{
//            Rent rent = session.get(Rent.class, id);
//            Property property = rent.getProperty();            
//            List<Rent> result = property.getRents();
//            session.flush();
//            return result; 
//        });
//        return rents;        
//    }
    
    @Override
    public int findAllSizeRelatedByOneId(Long id) {
        int size = (int)HibernateUtil.doWithinTransaction(sessionFactory, session ->{
            Rent rent = session.get(Rent.class, id);
            Property property = rent.getProperty();            
            List<Rent> result = property.getRents();            
            return result.size(); 
        });
        return size;        
    }    
    
}
