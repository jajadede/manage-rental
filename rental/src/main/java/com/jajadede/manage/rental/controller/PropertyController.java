package com.jajadede.manage.rental.controller;

import com.jajadede.manage.rental.domain.Address;
import com.jajadede.manage.rental.domain.Property;
import com.jajadede.manage.rental.domain.Rent;
import com.jajadede.manage.rental.service.AddressService;
import com.jajadede.manage.rental.service.PropertyService;
import com.jajadede.manage.rental.validator.PropertyValidator;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PropertyController {

    private static final Logger LOG = LogManager.getLogger(PropertyController.class.getName());

    @Autowired
    private PropertyService propertyService;    
    @Autowired
    private AddressService addressService;

    @Autowired
    private PropertyValidator propertyValidator;
    
    @RequestMapping("/property/list")
    public String list(Model model) {
        model.addAttribute("properties", propertyService.findAll());
        return "properties";
    }

    @RequestMapping(value = "/property/add", method = RequestMethod.GET)
    public String addFormGet(Model model) {
        Property newProperty = new Property();
        newProperty.setAddress(new Address());
        List<Rent> rents = new ArrayList<>();
        Rent newRent = new Rent();
        newRent.setEffectiveDate(new Date());
        rents.add(newRent);
        newProperty.setRents(rents);
        model.addAttribute("newProperty", newProperty);
        model.addAttribute("addresses", addressService.findAllWithNoPeople());
        return "propertyAdd";
    }

    @RequestMapping(value = "/property/add", method = RequestMethod.POST)
    public String addFormProcess(
            @ModelAttribute("newProperty") @Valid Property newProperty,
            BindingResult result,
            Model model) {
        LOG.debug("newProperty got from request: " + newProperty);
        if (result.hasErrors()) {
            model.addAttribute("newProperty", newProperty);
            model.addAttribute("addresses", addressService.findAllWithNoPeople());
            return "propertyAdd";
        }
        try {
            propertyService.create(newProperty);
            model.addAttribute("createSuccess", "");
        }
        catch (Exception e) {
            LOG.error(e);
            model.addAttribute("createError", e.getMessage());
        }           
        return list(model);

    }

    @RequestMapping(value = "/property/edit", method = RequestMethod.GET)
    public String editFormGet(@RequestParam("id") Long propertyId, Model model) {
        model.addAttribute("property", propertyService.findById(propertyId));
        return "property";
    }
    
    @RequestMapping(value = "/property/edit", method = RequestMethod.POST)
    public String editFormProcess(
            @ModelAttribute("property") @Valid Property property,              
            BindingResult result, 
            Model model) {
        if (result.hasErrors()) {
            model.addAttribute("property", property);
            return "property";
        }
        try {
            propertyService.update(property);
            model.addAttribute("updateSuccess", "");
        }
        catch (Exception e) {
            LOG.error(e);
            model.addAttribute("updateError", e.getMessage());
        }
        return list(model);
    }

    @RequestMapping(value = "/property/delete", method = RequestMethod.GET)
    public String deletePropertyById(@RequestParam("id") Long propertyId, Model model) {
        try {
            Property property = propertyService.findById(propertyId);
            propertyService.delete(property);
            model.addAttribute("deleteSuccess", "");
        }
        catch (DataIntegrityViolationException e){
            LOG.error(e);
            model.addAttribute("deleteError", "this record is in use by lease. Delete the related lease first.");            
        }
        catch (Exception e) {
            LOG.error(e);
            model.addAttribute("deleteError", e.getMessage());
        }        
        return list(model);
    }     

    @InitBinder
    public void initialseBinder(WebDataBinder binder) {
        binder.addValidators(propertyValidator);
        binder.setAllowedFields( //TODO complate the white list of fileds
                //				"address", 
                //				"city", 
                //				"number", 
                //				"postcode",
                //				"province", 
                //				"street", 
                //				"unit", 
                //				"code", 
                //				"description", 
                //				"id",
                //                                "rent",
                //				"language",
                "*"
        );
    }
}
