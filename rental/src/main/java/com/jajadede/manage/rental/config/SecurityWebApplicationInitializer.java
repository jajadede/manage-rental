package com.jajadede.manage.rental.config;

import org.springframework.context.annotation.Profile;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

@Profile("dev")
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

}
