/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.util;

import com.jajadede.manage.rental.domain.AbstractEntity;

/**
 *
 * @author Jerry
 */
public interface Transferable<T> {
    void transferChangeTo (T t);
}
