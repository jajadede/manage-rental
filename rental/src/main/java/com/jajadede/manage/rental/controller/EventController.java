package com.jajadede.manage.rental.controller;

import com.jajadede.manage.rental.domain.Action;
import com.jajadede.manage.rental.domain.Event;
import com.jajadede.manage.rental.domain.Lease;
import com.jajadede.manage.rental.domain.Transaction;
import com.jajadede.manage.rental.domain.TransactionMethod;
import com.jajadede.manage.rental.service.LeaseService;
import com.jajadede.manage.rental.validator.EventValidator;
import java.util.Date;
import javax.validation.Valid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Jerry
 */
@Controller
public class EventController {
    private static final Logger LOG = LogManager.getLogger(EventController.class.getName());
    
    @Autowired
    EventValidator eventValidator;
    
    @Autowired
    LeaseService leaseService;
    
    @RequestMapping(value = "/lease/eventdelete", method = RequestMethod.GET)
    public String deleteById(@RequestParam("eventId") Long eventId, Model model) {
        try {
            leaseService.deleteEventById(eventId);
            model.addAttribute("deleteSuccess", "");
        }
        catch (Exception e) {
            LOG.error(e);
            model.addAttribute("deleteError", e.getMessage());
        }        
        model.addAttribute("leases", leaseService.findAll());
        return "leases";
    }  

    @RequestMapping(value = "/lease/eventadd", method = RequestMethod.GET)
    public String addEventFormGet(Model model) {               
        model.addAttribute("newEvent", createNewEvent());          
        addEventFormModelSetup(model);
        return "leases";
    } 
    
    @RequestMapping(value = "/lease/eventadd", method = RequestMethod.POST)
    public String addEventFormProcess(
            @ModelAttribute("newEvent") @Valid Event newEvent, 
            BindingResult result, 
            Model model) {
        if (result.hasErrors()){
            model.addAttribute("newEvent", newEvent);
            addEventFormModelSetup(model);
            return "leases";           
        }
        try {
            newEvent.setActiondate(new Date());
            leaseService.addEvent(newEvent);
            model.addAttribute("createSuccess", "");
        }
        catch (Exception e) {
            LOG.error(e);
            model.addAttribute("createError", e.getMessage());
        }
        model.addAttribute("leases", leaseService.findAll());
        return "leases";
    } 
    
    @InitBinder
    public void initialseBinder(WebDataBinder binder) {
        binder.addValidators(eventValidator);
    }

    private Event createNewEvent() {
        Event newEvent = new Event();
        newEvent.addTransaction(new Transaction());
        newEvent.setLease(new Lease());
        return newEvent;
    }

    private void addEventFormModelSetup(Model model) {
        model.addAttribute("leases", leaseService.findAll()); // lease id
        model.addAttribute("actions", Action.values()); // action       
        model.addAttribute("methods", TransactionMethod.values()); // method           
    }

}
