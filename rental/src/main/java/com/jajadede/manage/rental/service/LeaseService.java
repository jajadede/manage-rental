/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.service;

import com.jajadede.manage.rental.domain.Event;
import com.jajadede.manage.rental.domain.Lease;
import java.util.List;

/**
 *
 * @author Jerry
 */
public interface LeaseService {
    List <Lease> findAll();
    Lease findById(Long id);
    Lease create(Lease lease);
    void update(Lease lease);
    void delete(Lease lease);
    Long countAll();
    void deleteEventById(Long eventId);
    void addEvent(Event event);
    /**
     * Delete the event and also update status of the related lease
     * @param event 
     */
    void deleteEvent(Event event);
}
