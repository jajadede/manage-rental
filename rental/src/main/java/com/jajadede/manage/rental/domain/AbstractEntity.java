/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.domain;

import com.jajadede.manage.rental.util.Transferable;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

/**
 *
 * @author Jerry
 */
@MappedSuperclass
public abstract class AbstractEntity implements Serializable, Transferable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false)
    protected Long id;
    
    @Version
    @Column(name = "version")
    protected int version;
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id){
        this.id = id;
    }

    @Override
    public String toString() {
        return "AbstractEntity{" + "id=" + id + ", version=" + version + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.getId());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractEntity other = (AbstractEntity) obj;
        // use method version of accessors for hibernate's proxies efficiency
        if (!Objects.equals(this.getId(), other.getId())) {
            return false;
        }
        return true;
    }

//    @Override
//    public int compareTo(Object t) {
//        AbstractEntity other = (AbstractEntity) t;
//        return (this.getId().intValue() - other.getId().intValue());
//    }
            
}
