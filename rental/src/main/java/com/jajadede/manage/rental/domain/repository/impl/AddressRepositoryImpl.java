/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.domain.repository.impl;

import com.jajadede.manage.rental.domain.Address;
import com.jajadede.manage.rental.domain.repository.AddressRepository;
import com.jajadede.manage.rental.util.HibernateUtil;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Jerry
 */
@Transactional
@Repository ("addressRepository")
public class AddressRepositoryImpl implements AddressRepository {
//    @Autowired
//    private NamedParameterJdbcTemplate jdbcTemplate;
    
    @Autowired
    private SessionFactory sessionFactory;
    
    private static final Logger LOG = LogManager.getLogger(AddressRepositoryImpl.class.getName());
    
    @Override
    @Transactional(readOnly = true)
    public Address findById(Long id) {
        Address result = (Address) HibernateUtil.doWithinTransaction(sessionFactory, (Session session) -> {
            return session.getNamedQuery(Address.ADDRESS_FIND_BY_ID).setParameter("ID", id).uniqueResult();
        });
        return result;
    }

    @Override
    @Transactional(readOnly = true)    
    public List<Address> findAll() {
        List<Address> result = (List<Address>) HibernateUtil.doWithinTransaction(sessionFactory, (Session session) -> {
            return session.getNamedQuery(Address.ADDRESS_FIND_ALL).list();
        });          
        return result;
    }

    @Override
    public Address create(Address address) {        
        HibernateUtil.doWithinTransaction(sessionFactory, (Session session) -> {
            return session.save(address);
        });
        LOG.debug("Created new address with id: " + address.getId());
        return address;
    }

    @Override
    public void update(Address address) {
        HibernateUtil.doWithinTransaction(sessionFactory, (Session session) -> {
            return session.merge(address);
        });        
        LOG.debug("Updated address with id: " + address.getId()); 
    }

    @Override
    public void delete(Address address) {
        HibernateUtil.doWithinTransaction(sessionFactory, (Session session) -> {
            session.delete(address);
            return null;
        });       
        LOG.debug("Deleted address with id: " + address.getId());        
    }

    @Override
    public Long countLonely() {        
        Long result = (Long) HibernateUtil.doWithinTransaction(sessionFactory, (Session session) -> {
            return session.getNamedQuery(Address.ADDRESS_COUNT_LONELY).uniqueResult();
        });
        return result;
    }  

    @Override
    public List<Address> findAllWithNoPeople() {
        List<Address> result = (List<Address>) HibernateUtil.doWithinTransaction(sessionFactory, (Session session) -> {
            Query query = session.createQuery("select a from Address a "
                    + "left join fetch a.peoples ppl "
                    + "where ppl is null");
            return query.list();
        });          
        return result;        
    }

    @Override
    public Long countAllWithNoPeople() {
        Long result = (Long) HibernateUtil.doWithinTransaction(sessionFactory, (Session session) -> {
            Query query = session.createQuery("select count(a) from Address a "
                    + "left join a.peoples ppl "
                    + "where ppl is null");
            return query.uniqueResult();
        });          
        return result;        
    }    
    
    @Override
    public List<Address> findAllWithNoProperty() {
        List<Address> result = (List<Address>) HibernateUtil.doWithinTransaction(sessionFactory, (Session session) -> {
            Query query = session.createQuery("select a from Address a "
                    + "left join fetch a.properties prop "
                    + "where prop is null");
            return query.list();
        });          
        return result;        
    }
    
    @Override
    public Long countAllWithNoProperty() {
        Long result = (Long) HibernateUtil.doWithinTransaction(sessionFactory, (Session session) -> {
            Query query = session.createQuery("select count(a) from Address a "
                    + "left join a.properties prop "
                    + "where prop is null");
            return query.uniqueResult();
        });          
        return result;        
    }    
}
