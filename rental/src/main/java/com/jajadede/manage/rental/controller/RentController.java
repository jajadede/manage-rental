package com.jajadede.manage.rental.controller;

import com.jajadede.manage.rental.domain.Property;
import com.jajadede.manage.rental.domain.Rent;
import com.jajadede.manage.rental.exception.DeletingLastRentException;
import com.jajadede.manage.rental.service.PropertyService;
import com.jajadede.manage.rental.validator.RentValidator;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Jerry
 */
@Controller
public class RentController {
    private static final Logger LOG = LogManager.getLogger(RentController.class.getName());
    
    @Autowired
    RentValidator rentValidator;
    
    @Autowired
    PropertyService propertyService;
    
    @RequestMapping(value = "/property/rentdelete", method = RequestMethod.GET)
    public String deleteRentById(@RequestParam("rentId") Long rentId, Model model) {
        try {
            propertyService.deleteRentByRentId(rentId);
            model.addAttribute("deleteSuccess", "");
        }
        catch (DeletingLastRentException e) {
            LOG.error(e);
            model.addAttribute("deleteLastRentError", "");
        }
        catch (Exception e) {
            LOG.error(e);
            model.addAttribute("deleteError", e.getMessage());
        }
        model.addAttribute("properties", propertyService.findAll());        
        return "properties";
    }  

    @RequestMapping(value = "/property/rentadd", method = RequestMethod.GET)
    public String addRentByPropertyIdRequest(Model model) {
        List<Property> properties = propertyService.findAll();
        model.addAttribute("properties", properties);
        Property newProperty = new Property();
        Rent newRent = new Rent();
        newRent.setEffectiveDate(new Date());
        newRent.setProperty(newProperty);        
        model.addAttribute("newRent", newRent);          
        return "properties";
    } 
    
    @RequestMapping(value = "/property/rentadd", method = RequestMethod.POST)
    public String addRentByPropertyIdProcess(
            @ModelAttribute("newRent") @Valid Rent newRent, 
            BindingResult result, 
            Model model) {
        if (result.hasErrors()){
            model.addAttribute("newRent", newRent);
            model.addAttribute("properties", propertyService.findAll());
            return "properties";           
        }
        try {
            Long propertyId = newRent.getProperty().getId();
            propertyService.addRentByPropertyId(propertyId, newRent);
            model.addAttribute("createSuccess", "");            
        }
        catch (Exception e) {
            LOG.error(e);
            model.addAttribute("createError", e.getMessage());
        }
        model.addAttribute("properties", propertyService.findAll());
        return "properties";
    } 
    
    @InitBinder
    public void initialseBinder(WebDataBinder binder) {
        binder.addValidators(rentValidator);

    }
}
