package com.jajadede.manage.rental.service;

import com.jajadede.manage.rental.wrapper.RentalActivity;
import java.util.List;

/**
 *
 * @author Jerry
 */
public interface SummaryService {
    List<RentalActivity> findByYearMonth(int year, int month);
    List<String> findYearMonthList(int withinYears, boolean desc);    
}
