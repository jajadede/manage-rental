/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.domain.repository;

import com.jajadede.manage.rental.domain.Property;
import java.util.List;

/**
 *
 * @author Jerry
 */
public interface PropertyRepository {
    List <Property> findAll();
    Property findById(Long id);
    Property create(Property property);
    void update(Property property);
    void delete(Property property);
    Long countAll();
    List <Property> findAllWithNoActiveLease();
    Long countAllWithNoActiveLease();
}
