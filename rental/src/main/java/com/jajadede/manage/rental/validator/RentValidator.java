package com.jajadede.manage.rental.validator;

import com.jajadede.manage.rental.domain.Property;
import com.jajadede.manage.rental.domain.Rent;
import com.jajadede.manage.rental.service.PropertyService;
import java.math.BigDecimal;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author Jerry
 */
@Component
public class RentValidator implements Validator {

    private static final Logger LOG = LogManager.getLogger(RentValidator.class.getName());

    @Autowired
    PropertyService propertyService;

    @Override
    public boolean supports(Class<?> clazz) {
        return Rent.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Rent rentObj = (Rent) target;

        validateRent(rentObj, errors);
        validateUniquePropertyIdAndRentEffectiveDate(rentObj, errors);
    }

    private void validateUniquePropertyIdAndRentEffectiveDate(Rent rentObj, Errors errors) {
        Property property = (Property) rentObj.getProperty();
        if (property == null) {
            errors.rejectValue(
                    "",
                    "validator.RentValidator.CreatePropertyFirst.message");
            LOG.debug("Error found, validator.RentValidator.CreatePropertyFirst.message is set.");
            return;
        }
        Property dbProperty = propertyService.findById(property.getId());
        for (Rent r : dbProperty.getRents()) {
            LOG.trace("rent:" + rentObj + ", r=" + r);
            if (r.getEffectiveDate().equals(rentObj.getEffectiveDate())) {
                errors.rejectValue(
                        "effectiveDate",
                        "validator.RentValidator.UniquePropertyIdAndRentEffectiveDate.message");
                LOG.debug("Error found, validator.RentValidator.UniquePropertyIdAndRentEffectiveDate.message is set.");
            }
        }

    }

    private void validateRent(Rent rentObj, Errors errors) {
        if (rentObj.getRent() == null) {
            errors.rejectValue(
                    "rent",
                    "validator.RentValidator.RentIsNotNull.message");
            LOG.debug("Error found, validator.RentValidator.RentIsNotNull.message is set.");
            return;
        }
        if (rentObj.getRent().compareTo(BigDecimal.ZERO) <= 0) {
            errors.rejectValue(
                    "rent",
                    "validator.RentValidator.RentIsGreaterThanZero.message");
            LOG.debug("Error found, validator.RentValidator.RentIsGreaterThanZero.message is set.");
            return;
        }
    }

}
