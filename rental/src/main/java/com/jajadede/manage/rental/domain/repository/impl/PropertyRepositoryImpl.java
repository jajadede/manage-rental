/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.domain.repository.impl;

import com.jajadede.manage.rental.domain.Property;
import com.jajadede.manage.rental.domain.repository.AddressRepository;
import com.jajadede.manage.rental.domain.repository.PropertyRepository;
import com.jajadede.manage.rental.util.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Jerry 
 */
@Repository("propertyRepository")
@Transactional
public class PropertyRepositoryImpl implements PropertyRepository {
    private static final Logger LOG = LogManager.getLogger(PropertyRepositoryImpl.class.getName()); 
    
    @Autowired
    private SessionFactory sessionFactory;     
    @Autowired
    private AddressRepository addressRepository;
    
    @Override
    @Transactional(readOnly = true)
    public List<Property> findAll() {
        List<Property> result = (List<Property>) HibernateUtil.doWithinTransaction(sessionFactory, session -> {            
            Query query = session.getNamedQuery(Property.PROPERTY_FIND_ALL);
            List<Property> list;
            try {
                List queryResult = query.list();
             list = (List<Property>) queryResult;   
            } 
            catch (Exception e) {
                LOG.error(e);
                list = new ArrayList<>();
            }    
            return list;
        }); 
        LOG.debug("Got result properties from querying all properties: " + result);
        return result;
    }

    @Override
    @Transactional(readOnly = true)    
    public Property findById(Long id) {
        Property result = (Property) HibernateUtil.doWithinTransaction(sessionFactory, session -> {            
            return (Property) session.getNamedQuery(Property.PROPERTY_FIND_BY_ID).setParameter("ID", id).uniqueResult();
        }); 
        return result;
    }

    @Override
    public Property create(Property property) {  
        Long result = (Long) HibernateUtil.doWithinTransaction(sessionFactory, session -> {
            session.save(property);
            return property.getId();
        });
        LOG.debug("Created property with new id: " + result);
        return property;
    }

    @Override
    public void update(Property property) {
        Long result = (Long) HibernateUtil.doWithinTransaction(sessionFactory, session -> {
            session.merge(property);
            return property.getId();
        });
        LOG.debug("Updated property with id: " + result);
    }

    @Override
    public void delete(Property property) {
        Long result = (Long) HibernateUtil.doWithinTransaction(sessionFactory, session -> {
            Property db = session.get(Property.class, property.getId());
            session.delete(db);
            return db.getId();
        });
        LOG.debug("Deleted property with id: " + result);
    }

    @Override
    public Long countAll() {
        Long result = (Long) HibernateUtil.doWithinTransaction(sessionFactory, session -> {
            Query query = session.getNamedQuery(Property.PROPERTY_COUNT_ALL);
            return query.uniqueResult();
        });        
        return result;
    }

    @Override
    public List<Property> findAllWithNoActiveLease() {
        List<Property> result = (List<Property>) HibernateUtil.doWithinTransaction(sessionFactory, session -> {
            Query query = session.createQuery("select p from Property p "
                    + "where p.id not in (  "
                    + "select l.property.id from Lease l "
                    + "where l.status = 'ACTIVE') ");
            return query.list();
        });
        LOG.debug("Got result from findAllWithNoActiveLease: " + result);
        return result;         
    }

    @Override
    public Long countAllWithNoActiveLease() {
        Long result = (Long) HibernateUtil.doWithinTransaction(sessionFactory, session -> {
            Query query = session.createQuery("select count(p) from Property p "
                    + "where p.id not in (  "
                    + "select l.property.id from Lease l "
                    + "where l.status = 'ACTIVE')  ");
            return (Long) query.uniqueResult();
        });  
        LOG.debug("Got result from countAllWithNoActiveLease: " + result);        
        return result;
    }

}
