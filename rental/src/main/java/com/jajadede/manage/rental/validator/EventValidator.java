package com.jajadede.manage.rental.validator;

import com.jajadede.manage.rental.domain.Action;
import com.jajadede.manage.rental.domain.Event;
import com.jajadede.manage.rental.domain.Lease;
import com.jajadede.manage.rental.domain.LeaseStatus;
import com.jajadede.manage.rental.service.AddressService;
import com.jajadede.manage.rental.service.LeaseService;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author Jerry
 */
@Component
public class EventValidator implements Validator {

    private static final Logger LOG = LogManager.getLogger(EventValidator.class.getName());
    
    @Autowired
    AddressService addressService;
    @Autowired
    LeaseService leaseService;

    @Override
    public boolean supports(Class<?> clazz) {
        return Event.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Event event = (Event) target;

        // common check        
        
        // check against db
        validateAction(event, errors);
        validateActionDate(event, errors);
    }

    private void validateAction(Event event, Errors errors) {
        Lease lease = leaseService.findById(event.getLease().getId());
        List<Event> events = lease.getEvents();
//        Collections.sort(events, (one, other) -> {
//            return (int)(one.getActiondate().getTime() - other.getActiondate().getTime());
//        });  // events are expected to be sorted by now
        LOG.debug("events to validate against: " + events);
        if (events.size() == 0 && event.getAction() != Action.RESERVE) {
            if ((event.getAction() == Action.RESERVE) && (events.get(0).getAction() == Action.RESERVE)) {
                errors.rejectValue(
                        "action",
                        "validator.EventValidator.action.ReserveFirst.message");
                LOG.debug("Error found, validator.EventValidator.action.ReserveFirst.message is set.");
            }            
        }
        if (events.size() >= 1) {
            if ((event.getAction() == Action.RESERVE) && (events.get(0).getAction() == Action.RESERVE)) {
                errors.rejectValue(
                        "action",
                        "validator.EventValidator.action.OnlyOneReserveAllowed.message");
                LOG.debug("Error found, validator.EventValidator.action.OnlyOneReserveAllowed.message is set.");
            }
        }
        if (events.size() >= 2) {
            if (lease.getStatus() == LeaseStatus.INACTIVE) {
                errors.rejectValue(
                        "action",
                        "validator.EventValidator.action.NoEventForInactiveLease.message");
                LOG.debug("Error found, validator.EventValidator.action.NoEventForInactiveLease.message is set.");                
            }
        }

    }
    
    private void validateActionDate(Event event, Errors errors) {
        Lease lease = leaseService.findById(event.getLease().getId());
        List<Event> events = lease.getEvents();
        if (events.stream().anyMatch(e -> e.getActiondate().equals(event.getActiondate()))) {
            errors.rejectValue(
                    "actiondate",
                    "validator.EventValidator.actiondate.NoDupAllowed.message");
            LOG.debug("Error found, validator.EventValidator.actiondate.NoDupAllowed.message is set.");            
        }
    }    
    
}
