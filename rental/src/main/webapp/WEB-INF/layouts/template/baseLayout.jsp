<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
        <!-- icons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">        
        <!-- Mobile View First -->
        <meta name="viewport" content="width=device-width, initial-scale=1">        
        <!-- Bootstrap 3 CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>-->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- other js -->
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.1/angular.min.js"></script>
        <!-- <script src="/rental/resources/js/controllers.js"></script> -->
        <!-- tile: title -->
        <title><tiles:insertAttribute name="title" /></title>
        <script>
            // controls the navi selection for current page
            $(document).ready(function () {
                    if ($('#navigation_tag').html() !== "") {
                    var currentpageId = '#' + $('#navigation_tag').html();
                    $(currentpageId).addClass('active');
                }
            });
        </script> 
    </head>
    <body>
        <!--    <div class="container">
                    <div class="pr-3 text-right">
                            <a href="?language=en">English</a>|<a href="?language=zh">简体中文</a>
                    </div>
            </div>-->      
        <div class="container">                
            <div class="jumbotron" style="margin-bottom:0;padding: 1rem 4rem">              
                <div class="header">
                    <h3 class="text-muted"><spring:message code="template.baseLayout.sitename"/></h3>
                </div>
                <h1>
                    <!-- tile: heading -->
                    <tiles:insertAttribute name="heading" />
                </h1>
                <p>
                    <!-- tile: tagline -->
                    <tiles:insertAttribute name="tagline" />
            </div>
            <!-- tile: navigation -->
            <tiles:insertAttribute name="navigation" />

            <!-- tile: access denied message -->
            <c:if test="${param.accessDenied != null}">
                ${param.accessDenied}
                <div class="alert alert-danger"><spring:message code="views.login.message.denied"/></div>
            </c:if>      
                
            <!-- tile: content -->
            <div id="navigation_tag" hidden="true"><tiles:insertAttribute name="navigation_tag" /></div>
            <tiles:insertAttribute name="content" />

            <div class="footer text-center">
                <!-- tile: footer -->
                <tiles:insertAttribute name="footer" />
            </div>
        </div>

    </body>
</html>