<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<nav class="navbar navbar-inverse" style="padding:0 1rem">
    <div class="container-fluid">
        <ul class="nav navbar-nav">
            <li id="homepage"><a href="<spring:url value="/"/>"><i class="material-icons" style="font-size:32px" title="<spring:message code="template.navigation.home"/>">home</i></a></li>
            <li id="propertypage" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="material-icons" style="font-size:32px" title="<spring:message code="template.navigation.property"/>">place</i><span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="<spring:url value="/property/list"/>"><spring:message code="template.navigation.property.drop1"/></a></li>                    
                    <li><a href="<spring:url value="/property/add"/>"><spring:message code="template.navigation.property.drop2"/></a></li>
                </ul>
            </li>
            <li id="peoplepage" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="material-icons" style="font-size:32px" title="<spring:message code="template.navigation.people"/>">person</i><span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="<spring:url value="/people/list"/>"><spring:message code="template.navigation.people.drop1"/></a></li>                    
                    <li><a href="<spring:url value="/people/add"/>"><spring:message code="template.navigation.people.drop2"/></a></li>
                </ul>
            </li>            
            <li id="leasepage" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="material-icons" style="font-size:32px" title="<spring:message code="template.navigation.lease"/>">note</i><span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="<spring:url value="/lease/list"/>"><spring:message code="template.navigation.lease.drop1"/></a></li>                    
                    <li><a href="<spring:url value="/lease/add"/>"><spring:message code="template.navigation.lease.drop2"/></a></li>
                </ul>
            </li> 
            <li id="aboutpage"><a href="<spring:url value="/about"/>"><i class="material-icons" style="font-size:32px" title="<spring:message code="tempalte.navigation.about"/>">info</i></a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <sec:authorize access="isAuthenticated()">
            <li><div style="padding:20px 0 0 0;color:#9d9d9d;font-size:18px"><spring:message code="template.baseLayout.authenticatedas"/><sec:authentication property="principal.username" /></div></li>
            </sec:authorize>             
            <li><a href="<spring:url value="/logout"/>"><i class="fa fa-sign-out" style="font-size:32px" title="<spring:message code="tempalte.navigation.logout"/>"></i></a></li>
        </ul>
    </div>
</nav>
        <hr />
