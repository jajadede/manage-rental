<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


    <h3><spring:message code="views.people.message.intro"/></h3>
    <form:form method="POST" modelAttribute="people" class="form-horizontal">
        <form:errors path="address" cssClass="alert alert-danger" element="div"/>
        <hr>
        <h4><spring:message code="views.people.form.label.addressinfo"/></h4>
        <form:hidden name="id" path="address.id"/>
        <div class="form-group ">
            <label class ="col-sm-5 control-label" for="unit"><spring:message code="views.people.form.label.address.unit"/></label>
            <div class ="col-sm-4">
                <form:input type="number" class="form-control" id="unit" placeholder="${address.unit}" name="unit" path="address.unit"/>
            </div>
        </div>               
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="number"><spring:message code="views.people.form.label.address.number"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="number" placeholder="${address.number}" name="number" path="address.number"/>
            </div>
        </div>   
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="street"><spring:message code="views.people.form.label.address.street"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="street" placeholder="${address.street}" name="street" path="address.street"/>
            </div> 
        </div>              
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="city"><spring:message code="views.people.form.label.address.city"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="city" placeholder="${address.city}" name="city" path="address.city"/>
            </div>
        </div>
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="province"><spring:message code="views.people.form.label.address.province"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="province" placeholder="${address.province}" name="province" path="address.province" />
            </div>  
        </div> 
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="postcode"><spring:message code="views.people.form.label.address.postcode"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="postcode" placeholder="${address.postcode}" name="postcode" path="address.postcode"/>
            </div>
        </div>            

        <hr>
        <h4><spring:message code="views.people.form.label.peopleinfo"/></h4>             
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="id"><spring:message code="views.people.form.label.id"/></label>
            <div class ="col-sm-4">
                <form:input type="number" class="form-control" id="id" placeholder="" name="id" path="id" disabled="true"/>
            </div>  
        </div>  
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="nickname"><spring:message code="views.people.form.label.nickname"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="nickname" placeholder="" name="nickname" path="nickname"/><form:errors path="nickname" cssClass="text-danger"/>
            </div>  
        </div>  
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="firstname"><spring:message code="views.people.form.label.firstname"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="firstname" placeholder="" name="firstname" path="firstname"/>
            </div> 
        </div>           
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="lastname"><spring:message code="views.people.form.label.lastname"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="lastname" placeholder="" name="lastname" path="lastname"/>
            </div> 
        </div>
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="cell"><spring:message code="views.people.form.label.cell"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="cell" placeholder="" name="cell" path="cell"/><form:errors path="cell" cssClass="text-danger"/>
            </div> 
        </div>
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="gender"><spring:message code="views.people.form.label.gender"/></label>
            <div class ="col-sm-4">
                <form:select class="form-control" id="gender" placeholder="" name="gender" path="gender">
                    <form:options items="${genders}" itemLabel="value"/>
                </form:select>                
            </div> 
        </div>  
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="email"><spring:message code="views.people.form.label.email"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="email" placeholder="" name="email" path="email"/><form:errors path="email" cssClass="text-danger"/>
            </div> 
        </div>
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="nationality"><spring:message code="views.people.form.label.nationality"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="nationality" placeholder="" name="nationality" path="nationality"/>
            </div> 
        </div>
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="position"><spring:message code="views.people.form.label.position"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="position" placeholder="" name="position" path="position"/>
            </div> 
        </div>
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="comment"><spring:message code="views.people.form.label.comment"/></label>
            <div class ="col-sm-4">
                <form:textarea rows="5" class="form-control" id="comment" placeholder="" name="comment" path="comment"/>
            </div> 
        </div>    

        <hr>
        <div class="form-group">
            <div class="col-sm-offset-5 col-sm-4">
                <button type="submit" class="btn btn-primary"><spring:message code="views.people.form.button.update"/></button>
            </div>
        </div>        
    </form:form>   
