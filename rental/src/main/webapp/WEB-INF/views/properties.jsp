<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />

<div class="container-fluid">  
    <c:if test="${deleteLastRentError != null}">
        <div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><spring:message code="views.property.delete.lastrent.error.message"/></div>
    </c:if>    
    <c:if test="${deleteError != null}">
        <div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><spring:message code="views.property.delete.error.message"/>${deleteError}</div>
    </c:if>
    <c:if test="${deleteSuccess != null}">
        <div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><spring:message code="views.property.delete.success.message"/></div>
    </c:if>
    <c:if test="${updateError != null}">
        <div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><spring:message code="views.property.update.error.message"/>${updateError}</div>
    </c:if>
    <c:if test="${updateSuccess != null}">
        <div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><spring:message code="views.property.update.success.message"/></div>
    </c:if>  
    <c:if test="${createError != null}">
        <div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><spring:message code="views.property.create.error.message"/>${createError}</div>
    </c:if>
    <c:if test="${createSuccess != null}">
        <div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><spring:message code="views.property.create.success.message"/></div>
    </c:if>     
    
    <c:if test="${newRent!=null}">
        <div class="form-group form-inline input-group-sm">
            <form:form method="POST" modelAttribute="newRent" class="form-horizontal" action="${baseURL}/property/rentadd">            
                <form:errors path="*" cssClass="alert alert-danger" element="div"/>                
                <label class ="control-label" for="propertyid"><spring:message code="views.properties.form.label.rent.create"/></label>
                <form:select id="propertyid" path="property.id" class="form-control" tilte="Property Id">
                    <c:forEach items="${properties}" var="property">
                        <form:option value="${property.id}">${property.id}</form:option>
                    </c:forEach>
                </form:select>
                <form:input type="date" title="Effective Date" path="effectiveDate" class="form-control"></form:input>
                <form:input type="number" title="Rent" path="rent" class="form-control"></form:input>                            
                <form:button type="submit" title="Add New Rent" class="form-control btn btn-primary" >+</form:button>      
            </form:form>  
        </div>                    
    </c:if>
    
        <p>
            <input class="form-control" id="filterKey" type="text" placeholder="<spring:message code="views.properties.input.search"/>">
        </p>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th><spring:message code="views.properties.table.head.id"/></th>
                    <th><spring:message code="views.properties.table.head.address"/></th>
                    <th><spring:message code="views.properties.table.head.code"/></th>
                    <th><spring:message code="views.properties.table.head.description"/></th>
                    <th>
                        <spring:message code="views.properties.table.head.rent"/>
                        <c:if test="${newRent==null}">
                            &nbsp; 
                            <a role="button" title="Add New Rent" class="btn-xs btn-primary" href="${baseURL}/property/rentadd" >+</a>
                        </c:if>                    
                    </th>
                    <th><spring:message code="views.properties.table.head.action"/></th>
                </tr>
            </thead>
            <tbody id="tableToSearch">
                <c:forEach items="${properties}" var="property">
                    <tr>
                        <td>
                            <a role="button" class="btn-xs btn-danger" href="${baseURL}/property/delete?id=${property.id}" title="<spring:message code="views.property.form.button.delete"/>" id="deleteproperty${property.id}" onclick="return popConfirmationBox('<spring:message code="views.property.form.button.delete.confirm"/>', 'deleteproperty${property.id}')" >-</a>
                            &nbsp; 
                            ${property.id}
                        </td>
                        <td><a href="#!" title="" data-toggle="popover" data-content="${property.address}">${property.address.toShortString()}</a></td>
                        <td>${property.code}</td>
                        <td>${property.description}</td>
                        <td>
                            <c:if test="${property.rents != null}">
                                <c:set var="startIndex" value="0"/>
                                <c:if test="${property.rents.size() > 2}">
                                    ...<br>
                                    <c:set var="startIndex" value="${property.rents.size()-2}"/>
                                </c:if>								
                                <c:forEach items="${property.rents}" var="rent" varStatus="idx">
                                    <c:if test="${idx.index >= startIndex}">
                                        <a role="button" title="Delete" class="btn-xs btn-danger" href="${baseURL}/property/rentdelete?rentId=${rent.id}">-</a>
                                        &nbsp; 
                                        <a href="#!" title="" data-toggle="popover" data-content="${rent}">${rent.toShortString()}</a>
                                        <br>
                                    </c:if>
                                </c:forEach>
                            </c:if>                  
                        </td>
                        <td>
                            <a role="button" class="btn-sm btn-warning" href="${baseURL}/property/edit?id=${property.id}"><spring:message code="views.property.form.button.update"/></a>                        
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    <br>
</div>

<script>
    $(document).ready(function () {
        $("#filterKey").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#tableToSearch tr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
        });
        $('[data-toggle="popover"]').popover(); 
    });
    
/**
* Display a confirmation message box to validate if we want to continue.
*
* @param message String to display
* @param tagId String id of the tag that must display the message.
*
* @return Boolean (confirmation)
*/  
function popConfirmationBox(message, tagId){
    var confirmation = true;
    if (typeof tagId === 'string' && document.activeElement.id.toUpperCase() === tagId.toUpperCase()) {
        if (typeof message === 'string' && message.length > 0) {
            confirmation = window.confirm(message);
        }
    }
    return confirmation;
}    
</script>
