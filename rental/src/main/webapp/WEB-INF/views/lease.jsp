<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />

    <h3><spring:message code="views.lease.form.message"/></h3>
    <form:form method="POST" modelAttribute="lease" class="form-horizontal">
        <form:errors path="*" cssClass="alert alert-danger" element="div"/>
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="id"><spring:message code="views.lease.form.label.id"/></label>
            <div class ="col-sm-4">
                <form:input type="number" class="form-control" id="id" placeholder="" name="id" path="id" disabled="true"/>
            </div>  
        </div>
        <hr>
        <h4><spring:message code="views.lease.form.label.propertyinfo"/></h4>
        <div class="form-group ">
            <label class ="col-sm-5 control-label" for="property.id"><spring:message code="views.lease.form.label.property"/></label>
            <div class ="col-sm-4">
                <form:select id="selectOldProperty" path="property.id" class="form-control"><form:errors path="property" cssClass="text-danger"/>
                    <form:option value=""></form:option>
                    <c:if test="${properties != null && properties.size()>0}">
                        <c:forEach items="${properties}" var="oldProperty" >
                            <form:option value="${oldProperty.id}"
                                      id="property${oldProperty.id}"
                                      longstring="${oldProperty.toLongString()}"
                                      >
                                ${oldProperty.toShortString()}
                            </form:option>
                        </c:forEach>
                    </c:if>            
                </form:select>
                <div id="propertydetail"></div>
            </div>
        </div>
            
        <hr>    
        <h4><spring:message code="views.lease.form.label.tenantsinfo"/></h4>
        
        <%--<form:hidden path="tenantsSet" />--%>
        <c:if test="${lease.tenants != null && lease.tenants.size()>0}">
            <c:forEach items="${lease.tenants}" var="tenant" varStatus="idx">
                <div class="form-group ">
                    <label class ="col-sm-5 control-label" for="selectTenant-${idx.index}"><spring:message code="views.lease.form.label.tenant"/></label>
                    <div class ="col-sm-4">
                        <form:select id="selectTenant-${idx.index}" path="tenants[${idx.index}].id" class="form-control">
                            <form:option value=""></form:option>                    
                            <c:if test="${peoples != null && peoples.size()>0}">
                                <c:forEach items="${peoples}" var="people" >
                                    <form:option value="${people.id}"
                                              id="peopledetail-${people.id}"
                                              longname="${people.getLongName()}"
                                              >
                                        ${people.nickname}
                                    </form:option>
                                </c:forEach>
                            </c:if>            
                        </form:select>
                        <div id="detail-selectTenant-${idx.index}"></div>
                    </div>
                </div>                
            </c:forEach>
        </c:if>          
            
        <hr>
        <h4><spring:message code="views.lease.form.label.terminfo"/></h4>                        
        <div class="form-group ">
            <label class ="col-sm-5 control-label" for="effectivedate"><spring:message code="views.lease.form.label.effectivedate"/></label>
            <div class ="col-sm-4">
                <form:input type="date" class="form-control" id="effectivedate" placeholder="" name="effectivedate" path="effectivedate"/>
            </div>
        </div>               
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="number"><spring:message code="views.lease.form.label.enddate"/></label>
            <div class ="col-sm-4">
                <form:input type="date" class="form-control" id="enddate" placeholder="" name="enddate" path="enddate"/>
            </div>
        </div>   
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="status"><spring:message code="views.lease.form.label.status"/></label>
            <div class ="col-sm-4">
                <c:forEach items="${statuses}" var="sts">
                    <label class="radio-inline disabled" >
                    <form:radiobutton path="status" value="${sts}" disabled="true" />${sts.value}
                    </label>
                </c:forEach>                
            </div> 
        </div>              
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="comment"><spring:message code="views.lease.form.label.comment"/></label>
            <div class ="col-sm-4">
                <form:textarea rows="5" class="form-control" id="comment" placeholder="" name="comment" path="comment"/>
            </div>
        </div>
            
        <hr>    
        <h4><spring:message code="views.lease.form.label.eventsinfo"/></h4>
        <%--<form:hidden path="eventsSet" />--%>
        <c:if test="${lease.events != null && lease.events.size()>0}">
            <div class="table-responsive">
            <table class="table">
                <thead>
                <th><spring:message code="views.lease.form.label.event.actiondate"/></th>
                <th><spring:message code="views.lease.form.label.event.action"/></th>
                <th><spring:message code="views.lease.form.label.event.transaction.amount"/></th>      
                <th><spring:message code="views.lease.form.label.event.transaction.method"/></th>
                <th><spring:message code="views.lease.form.label.event.transaction.effectivedate"/></th>
                <th><spring:message code="views.lease.form.label.event.transaction.comment"/></th>                             
            </thead>
            <tbody>
                <c:forEach items="${lease.events}" var="event" varStatus="idx">
                    <form:hidden path="events[${idx.index}].id" />
                    <form:hidden path="events[${idx.index}].transaction.id" />                
                    <form:hidden path="events[${idx.index}].transaction.purpose" />                 
                <tr>
                    <td><form:input style="width:150px;" type="date" class="form-control" id="actiondate" placeholder="" name="actiondate" path="events[${idx.index}].actiondate"/></td>
                    <td><form:select style="width:160px" id="action" path="events[${idx.index}].action" class="form-control"><form:options items="${actions}" itemLabel="value"></form:options></form:select></td>
                    <td><form:input style="width:80px" type="number" class="form-control" id="transaction.amount" placeholder="" name="transaction.amount" path="events[${idx.index}].transaction.amount"/></td>
                    <td><form:select style="width:100px" id="transaction.method" path="events[${idx.index}].transaction.method" class="form-control"><form:options items="${methods}" itemLabel="value"></form:options></form:select></td>
                    <td><form:input style="width:150px;" type="date" class="form-control" id="transaction_effectivedate" name="transaction_effectivedate" path="events[${idx.index}].transaction.effectivedate" /></td>
                    <td><form:textarea style="width:100px" rows="1" class="form-control" id="transaction.comment" name="transaction.amount" path="events[${idx.index}].transaction.comment" /></td>
                </tr>
                </c:forEach>                
            </tbody>
            </table>
            </div>
        </c:if>
        
        <hr>
        <div class="form-group">
            <div class="col-sm-offset-5 col-sm-4">
                <button type="submit" class="btn btn-primary"><spring:message code="views.lease.form.button.submit"/></button>
            </div>
        </div>

    </form:form>
        
<script>
    $(document).ready(function () {
        updatePropertyDetail();
        $('[id|=selectTenant]').each(function(){
            updateTenantDetail(this);
        });
        $('#selectOldProperty').change(function (){
            updatePropertyDetail();
        });
        $('[id|=selectTenant]').change(function (){
            updateTenantDetail(this);
        });       
        function updatePropertyDetail(){
            var currSelection = $('#selectOldProperty').val(); //id
            var currOptionSelector = "#property" + currSelection; //property{id}            
            if (currSelection==='') {
                $('#propertydetail').html('');               
            }else{
                $('#propertydetail').html($(currOptionSelector).attr('longstring'));
            }            
        }
        function updateTenantDetail(element) {
            var currSelection = $(element).val(); // 
            var div_id = '#detail-'+$(element).attr('id');
            var option_id = '#peopledetail-' + currSelection;
            
            if (currSelection==='') {
                $(div_id).html('');               
            }else{
                $(div_id).html($(option_id).attr('longname'));
            }            
        }
    });
</script>    
