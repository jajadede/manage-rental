<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />

<div class="container-fluid">
    <c:if test="${deleteError != null}">
        <div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><spring:message code="views.lease.delete.error.message"/>${deleteError}</div>
    </c:if>
    <c:if test="${deleteSuccess != null}">
        <div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><spring:message code="views.lease.delete.success.message"/></div>
    </c:if>
    <c:if test="${updateError != null}">
        <div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><spring:message code="views.lease.update.error.message"/>${updateError}</div>
    </c:if>
    <c:if test="${updateSuccess != null}">
        <div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><spring:message code="views.lease.update.success.message"/></div>
    </c:if>  
    <c:if test="${createError != null}">
        <div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><spring:message code="views.lease.create.error.message"/>${createError}</div>
    </c:if>
    <c:if test="${createSuccess != null}">
        <div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><spring:message code="views.lease.create.success.message"/></div>
    </c:if>     
    
    <c:if test="${newEvent!=null}">
        <div class="form-group form-inline input-group-sm">
            <form:form method="POST" modelAttribute="newEvent" class="form-horizontal" action="${baseURL}/lease/eventadd">                
                <form:errors path="*" cssClass="alert alert-danger" element="div"/>                               
                <label class ="control-label" for="leaseid"><spring:message code="views.leases.form.label.event.create"/></label>
                <form:select id="leaseid" path="lease.id" class="form-control">
                    <c:forEach items="${leases}" var="lease">
                        <form:option value="${lease.id}">${lease.id}</form:option>
                    </c:forEach>
                </form:select>
                <form:select style="width:200px" id="action" path="action" class="form-control">
                    <form:options items="${actions}" itemLabel="value"/>
                </form:select>
                <form:select style="width:100px" id="transaction.method" path="transaction.method" class="form-control">
                    <form:options items="${methods}"/>
                </form:select>				
                <form:input style="width:100px" type="number" title="Amount" path="transaction.amount" class="form-control"></form:input>
                <form:hidden path="actiondate" />	
                <form:hidden path="transaction.effectivedate" />
                <form:hidden path="transaction.purpose" />
                <form:hidden path="transaction.comment" />
                <form:button type="submit" title="Add New Event" class="form-control btn btn-primary" >+</form:button>      
            </form:form>  
        </div>                    
    </c:if>
    <p>
        <input class="form-control" id="filterKey" type="text" placeholder="<spring:message code="views.leases.input.search"/>">
    </p>    
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="pill" href="#ActiveLease"><spring:message code="views.leases.table.tab.active"/></a></li>
        <li><a data-toggle="pill" href="#InactiveLease"><spring:message code="views.leases.table.tab.inactive"/></a></li>
    </ul>
    <div class="tab-content">
        <div id="ActiveLease" class="tab-pane fade in active">

            <p><table class="table table-bordered">
                <thead>
                    <tr>
                        <th><spring:message code="views.leases.table.head.id"/></th>
                        <th><spring:message code="views.leases.table.head.proprty"/></th>
                        <th><spring:message code="views.leases.table.head.tenants"/></th>
                        <th><spring:message code="views.leases.table.head.term"/></th>
                        <th>
                            <spring:message code="views.leases.table.head.events"/>
                            <c:if test="${newEvent==null}">
                                &nbsp; 
                                <a role="button" title="Add New Event" class="btn-xs btn-primary" href="${baseURL}/lease/eventadd" >+</a>
                            </c:if>                    
                        </th>
                        <th><spring:message code="views.leases.table.head.action"/></th>
                    </tr>
                </thead>
                <tbody id="tableToSearch">
                    <c:forEach items="${leases}" var="lease">
                        <c:if test="${lease.status == 'ACTIVE'}">
                            <tr>
                                <td>
                                    <a role="button" class="btn-xs btn-danger" id="deletelease${lease.id}" href="${baseURL}/lease/delete?id=${lease.id}" title="<spring:message code="views.lease.form.button.delete"/>" onclick="return popConfirmationBox('<spring:message code="views.lease.form.button.delete.confirm"/>', 'deletelease${lease.id}')">-</a>
                                    &nbsp; 
                                    ${lease.id}
                                </td>
                                <td><a href="#!" title="" data-toggle="popover" data-content="${lease.property.toLongString()}">${lease.property.toShortString()}</a></td>
                                <td>${lease.toTenantsShortString()}</td>
                                <td>
                                    <div class="                             
                                         <c:choose>
                                             <c:when test="${lease.status == 'ACTIVE'}">text-success</c:when>
                                             <c:otherwise>text-danger</c:otherwise>
                                         </c:choose>" 
                                         >${lease.toTermString()}</div>
                                </td>
                                <td>
                                    <c:if test="${lease.events != null}">
                                        <c:set var="startIndex" value="0"/>
                                        <c:if test="${lease.events.size() > 2}">
                                            ...<br>
                                            <c:set var="startIndex" value="${lease.events.size()-2}"/>
                                        </c:if>
                                        <c:forEach items="${lease.events}" var="event" varStatus="inx">
                                            <c:if test="${inx.index >= startIndex}">
                                                <a role="button" title="Delete" class="btn-xs btn-danger" href="${baseURL}/lease/eventdelete?eventId=${event.id}">-</a>
                                                &nbsp; 
                                                <a href="#!" title="" data-toggle="popover" data-content="${event.toLongString()}">${event.toShortString()}</a>
                                                <br>
                                            </c:if>
                                        </c:forEach>
                                    </c:if>                    
                                </td>
                                <td>
                                    <a role="button" class="btn-sm btn-warning" href="${baseURL}/lease/edit?id=${lease.id}"><spring:message code="views.lease.form.button.update"/></a>                        
                                </td>
                            </tr>
                        </c:if>
                    </c:forEach>
                </tbody>
            </table></p>
        </div>
        <div id="InactiveLease" class="tab-pane fade">
            <p><table class="table table-bordered">
                <thead>
                    <tr>
                        <th><spring:message code="views.leases.table.head.id"/></th>
                        <th><spring:message code="views.leases.table.head.proprty"/></th>
                        <th><spring:message code="views.leases.table.head.tenants"/></th>
                        <th><spring:message code="views.leases.table.head.term"/></th>
                        <th>
                            <spring:message code="views.leases.table.head.events"/>
                            <c:if test="${newEvent==null}">
                                &nbsp; 
                                <a role="button" title="Add New Event" class="btn-xs btn-primary" href="${baseURL}/lease/eventadd" >+</a>
                            </c:if>                    
                        </th>
                        <th><spring:message code="views.leases.table.head.action"/></th>
                    </tr>
                </thead>
                <tbody id="tableToSearch">
                    <c:forEach items="${leases}" var="lease">
                        <c:if test="${lease.status == 'INACTIVE'}">
                            <tr>
                                <td>
                                    <a role="button" class="btn-xs btn-danger" id="deletelease${lease.id}" href="${baseURL}/lease/delete?id=${lease.id}" title="<spring:message code="views.lease.form.button.delete"/>" onclick="return popConfirmationBox('<spring:message code="views.lease.form.button.delete.confirm"/>', 'deletelease${lease.id}')">-</a>
                                    &nbsp; 
                                    ${lease.id}
                                </td>
                                <td><a href="#!" title="" data-toggle="popover" data-content="${lease.property.toLongString()}">${lease.property.toShortString()}</a></td>
                                <td>${lease.toTenantsShortString()}</td>
                                <td>
                                    <div class="                             
                                         <c:choose>
                                             <c:when test="${lease.status == 'ACTIVE'}">text-success</c:when>
                                             <c:otherwise>text-danger</c:otherwise>
                                         </c:choose>" 
                                         >${lease.toTermString()}</div>
                                </td>
                                <td>
                                    <c:if test="${lease.events != null}">
                                        <c:set var="startIndex" value="0"/>
                                        <c:if test="${lease.events.size() > 2}">
                                            ...<br>
                                            <c:set var="startIndex" value="${lease.events.size()-2}"/>
                                        </c:if>
                                        <c:forEach items="${lease.events}" var="event" varStatus="inx">
                                            <c:if test="${inx.index >= startIndex}">
                                                <a role="button" title="Delete" class="btn-xs btn-danger" href="${baseURL}/lease/eventdelete?eventId=${event.id}">-</a>
                                                &nbsp; 
                                                <a href="#!" title="" data-toggle="popover" data-content="${event.toLongString()}">${event.toShortString()}</a>
                                                <br>
                                            </c:if>
                                        </c:forEach>
                                    </c:if>                    
                                </td>
                                <td>
                                    <a role="button" class="btn-sm btn-warning" href="${baseURL}/lease/edit?id=${lease.id}"><spring:message code="views.lease.form.button.update"/></a>                        
                                </td>
                            </tr>
                        </c:if>
                    </c:forEach>
                </tbody>
            </table></p>
        </div>
    </div>

    <br>
</div>

<script>
    $(document).ready(function () {
        $("#filterKey").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#tableToSearch tr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
        });
        $('[data-toggle="popover"]').popover();
    });

    /**
     * Display a confirmation message box to validate if we want to continue.
     *
     * @param message String to display
     * @param tagId String id of the tag that must display the message.
     *
     * @return Boolean (confirmation)
     */
    function popConfirmationBox(message, tagId) {
        var confirmation = true;
        if (typeof tagId === 'string' && document.activeElement.id.toUpperCase() === tagId.toUpperCase()) {
            if (typeof message === 'string' && message.length > 0) {
                confirmation = window.confirm(message);
            }
        }
        return confirmation;
    }
</script>
