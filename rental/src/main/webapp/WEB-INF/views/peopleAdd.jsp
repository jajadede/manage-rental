<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


    <h3><spring:message code="views.people.form.add.message"/></h3>
    <form:form method="POST" modelAttribute="newPeople" class="form-horizontal">
        <form:errors path="*" cssClass="alert alert-danger" element="div"/>
        <hr>
        <h4><spring:message code="views.people.form.label.addressinfo"/></h4>

        <div class="form-group ">
            <label class ="col-sm-5 control-label" for="unit"><spring:message code="views.people.form.label.address.existing"/></label>
            <div class ="col-sm-4">
                <form:select id="selectOldAddress" path="address.id" class="form-control">
                    <form:option value=""></form:option>
                    <c:if test="${addresses != null && addresses.size()>0}">
                        <c:forEach items="${addresses}" var="oldAddress" >
                            <form:option value="${oldAddress.id}"
                                      id="address${oldAddress.id}"
                                      unit="${oldAddress.unit}"
                                      number="${oldAddress.number}"
                                      street="${oldAddress.street}"
                                      city="${oldAddress.city}"
                                      province="${oldAddress.province}"
                                      postcode="${oldAddress.postcode}"
                                      >
                                ${oldAddress}
                            </form:option>
                        </c:forEach>
                    </c:if>            
                </form:select>
            </div>
        </div> 

        <div class="form-group ">
            <label class ="col-sm-5 control-label" for="unit"><spring:message code="views.people.form.label.address.unit"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="unit" placeholder="" name="unit" path="address.unit"/>
            </div>
        </div>               
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="number"><spring:message code="views.people.form.label.address.number"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="number" placeholder="" name="number" path="address.number"/>
            </div>
        </div>   
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="street"><spring:message code="views.people.form.label.address.street"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="street" placeholder="" name="street" path="address.street"/>
            </div> 
        </div>              
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="city"><spring:message code="views.people.form.label.address.city"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="city" placeholder="" name="city" path="address.city"/>
            </div>
        </div>
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="province"><spring:message code="views.people.form.label.address.province"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="province" placeholder="" name="province" path="address.province"/>
            </div>  
        </div> 
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="postcode"><spring:message code="views.people.form.label.address.postcode"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="postcode" placeholder="" name="postcode" path="address.postcode"/>
            </div>
        </div>            

        <hr>
        <h4><spring:message code="views.people.form.label.peopleinfo"/></h4>             
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="id"><spring:message code="views.people.form.label.id"/></label>
            <div class ="col-sm-4">
                <form:input type="number" class="form-control" id="id" placeholder="" name="id" path="id" disabled="true"/>
            </div>  
        </div>  
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="nickname"><spring:message code="views.people.form.label.nickname"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="nickname" placeholder="" name="nickname" path="nickname"/><form:errors path="nickname" cssClass="text-danger"/>
            </div>  
        </div>  
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="firstname"><spring:message code="views.people.form.label.firstname"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="firstname" placeholder="" name="firstname" path="firstname"/>
            </div> 
        </div>           
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="lastname"><spring:message code="views.people.form.label.lastname"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="lastname" placeholder="" name="lastname" path="lastname"/>
            </div> 
        </div>
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="cell"><spring:message code="views.people.form.label.cell"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="cell" placeholder="" name="cell" path="cell"/><form:errors path="cell" cssClass="text-danger"/>
            </div> 
        </div>
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="gender"><spring:message code="views.people.form.label.gender"/></label>
            <div class ="col-sm-4">
                <form:select class="form-control" id="gender" placeholder="" name="gender" path="gender">
                    <form:options items="${genders}" itemLabel="value"/>
                </form:select>                
            </div> 
        </div>  
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="email"><spring:message code="views.people.form.label.email"/></label>
            <div class ="col-sm-4">
                <form:input type="email" class="form-control" id="email" placeholder="" name="email" path="email"/><form:errors path="email" cssClass="text-danger"/>
            </div> 
        </div>
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="nationality"><spring:message code="views.people.form.label.nationality"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="nationality" placeholder="" name="nationality" path="nationality"/>
            </div> 
        </div>
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="position"><spring:message code="views.people.form.label.position"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="position" placeholder="" name="position" path="position"/>
            </div> 
        </div>
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="comment"><spring:message code="views.people.form.label.comment"/></label>
            <div class ="col-sm-4">
                <form:textarea rows="5" class="form-control" id="comment" placeholder="" name="comment" path="comment"/>
            </div> 
        </div>            
            
        <hr>
        <div class="form-group">
            <div class="col-sm-offset-5 col-sm-4">
                <button type="submit" class="btn btn-primary"><spring:message code="views.people.form.button.submit"/></button>
            </div>
        </div>

    </form:form>

<script>
    // controls the navi selection for current page
    $(document).ready(function () {
        $('#selectOldAddress').change(function (){
            var currSelection = $('#selectOldAddress').val(); //id
            var currOptionSelector = "#address" + currSelection; //address{id}            
            if (currSelection==='') {
                $('#unit').val('');
                $('#number').val('');
                $('#street').val('');
                $('#city').val('');
                $('#province').val('');
                $('#postcode').val('');                
            }else{
                $('#unit').val($(currOptionSelector).attr('unit'));
                $('#number').val($(currOptionSelector).attr('number'));
                $('#street').val($(currOptionSelector).attr("street"));
                $('#city').val($(currOptionSelector).attr("city"));
                $('#province').val($(currOptionSelector).attr('province'));
                $('#postcode').val($(currOptionSelector).attr('postcode'));  
            }
        });

    });
    

</script>    
