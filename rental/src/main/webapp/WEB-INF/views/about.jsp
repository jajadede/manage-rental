<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

            <div class="row ">
                <div class="col-sm-3 ">
                    <h3>References Books</h3>
                    <div class="thumbnail">
                        <img class="img-fluid" src="<spring:url value="/resources/images/ref_spring_app_for_enterprise.jpg"/>" alt="Spring: Developing Java Applications for the Enterprise" >                   
                        <a href="https://www.amazon.ca/Spring-Developing-Java-Applications-Enterprise-ebook/dp/B06XCRW6SK/ref=sr_1_1?ie=UTF8&qid=1538905454&sr=8-1&keywords=spring+developing+java+applications+for+the+enterprise">Spring: Developing Java Applications for the Enterprise <br>By Ravi Kant Soni...</a>
                    </div>
                    <div class="thumbnail">
                        <img class="img-fluid" src="<spring:url value="/resources/images/ref_pro_spring5.jpg"/>" alt="Pro Spring 5">                   
                        <a href="https://www.amazon.ca/Pro-Spring-Depth-Guide-Framework/dp/1484228073">Pro Spring 5 <br>By Iuliana Cosmina ...</a>
                    </div> 
                    <div class="thumbnail">
                        <img class="img-fluid" src="<c:out value="${pageContext.servletContext.contextPath}" />/resources/images/ref_beginning_hibernate.jpg" alt="Beginning Hibernate" >
                        <a href="https://www.amazon.ca/Beginning-Hibernate-Joseph-B-Ottinger-ebook/dp/B01MRIXZGP/ref=sr_1_1?s=books&ie=UTF8&qid=1538792121&sr=1-1&keywords=beginning+hibernate">Beginning Hibernate <br>By Joseph B. Ottinger...</a>
                    </div> 
                    <div class="thumbnail">
                        <img class="img-fluid" src="<spring:url value="/resources/images/ref_agile_java.jpg"/>" alt="Agile Java Design and Development" >
                        <a href="https://www.amazon.ca/Spring-Hibernate-Data-Modeling-REST-ebook/dp/B00S5D2I5O/ref=sr_1_1?s=books&ie=UTF8&qid=1538792319&sr=1-1&keywords=agile+Java+Design+and+Development">Agile Java Design and Development <br>By Amritendu De</a>
                    </div>                    
                </div>                
                <div class="col-sm-6 ">
                    <h3>Difference between admin and user?</h3>
                    <ul>
                        <li>Normal user cannot delete or edit</li>
                    </ul>
                    <h3>How to create a new lease?</h3>
                    <ul>
                        <li>Create a property in the property->add page or click the '+' sign in home page.</li>
                        <li>Create a people in the people->add page or click the '+' sign in the home page.</li>
                        <li>Create a lease using the above two record in the Lease->Add page or click the '+' sign in the home page.</li>
                        <li>Create additional events in the Lease->List page, go edit after create to change the effective date.</li>
                        <li>Click the 'Update' next to the newly created lease to see the detail of the lease, including all its events. The Lease->List page only shows the newest two events.</li>
                        <li>Go back to the home page, select the year-month in the dropdown that the above effective is in. Click Go to see those transactions happened within the same month. </li>
                    </ul>                                    
                    <hr>
                    <h3>How to see all the events(including those from other leases both active and inactive) in a month?</h3>
                    <ul>
                        <li>Go to the home page, select the year-month in the dropdown. Those options are created from the events effective dates within one year from today in descending order. </li>
                        <li>Click Go to see those transactions happened within the same month. </li>
                    </ul>                                    
                    <hr>                      
                    <h3>Purpose of this site</h3>
                    <ul>
                        <li>To practice what I learned.</li>
                        <li>To make this application available at any place that has internet.</li>
                        <li>To solve the headache of managing a long list in the excel sheet.</li>
                        <li>To be able to be expanded in the future. For example, pdf creation, mailing...</li>
                    </ul>                                    
                    <hr>
                    <h3>Develop environment</h3>
                    <ul>
                        <li>Win 7 (x64)</li>
                        <li>NetBeans IDE 8.2</li>
                        <li>Maven 3.5.4</li>
                        <li>Java 1.8</li>
                        <li>Tomcat 9.0.11</li>
                        <li>MySQL Workbench 8.0(MySQL 8.0.12)</li>
                    </ul>
                    <hr>
                    <h3>Knowledge practiced</h3>
                    <ul>
                        <li>Spring MVC</li>
                        <li>HTML</li>
                        <li>CSS</li>
                        <li>Ajax</li>
                        <li>JSP</li>
                        <li>Hibernate</li>
                    </ul>
                    <hr>
                    <h3>Basic expectations</h3>
                    <ul class="fa-ul">
                        <li><i class="fa-li fa fa-check-square-o" style="font-size:24px"></i>CRUD properties</li>
                        <li><i class="fa-li fa fa-check-square-o" style="font-size:24px"></i>CRUD people</li>
                        <li><i class="fa-li fa fa-check-square-o" style="font-size:24px"></i>CRUD lease</li>
                    </ul>
                    <hr>
                    <h3>Future improvements</h3>
                    <ul class="fa-ul">
                        <li><i class="fa-li fa fa-square-o" style="font-size:24px"></i>Lease pdf creation</li>
                        <li><i class="fa-li fa fa-square-o" style="font-size:24px"></i>Maintenance logging</li>
                        <li><i class="fa-li fa fa-square-o" style="font-size:24px"></i>Tax return related report generation</li>
                        <li><i class="fa-li fa fa-square-o" style="font-size:24px"></i>To whom letter generation</li>
                        <li><i class="fa-li fa fa-square-o" style="font-size:24px"></i>Receipt generation</li>
                        <li><i class="fa-li fa fa-square-o" style="font-size:24px"></i>File mailing</li>
                        <li><i class="fa-li fa fa-square-o" style="font-size:24px"></i>Address GPS mapping</li>
                    </ul>
                </div>
                <div class="col-sm-3 ">
                    <h3>References Sites</h3>
                    <ul>
                        <li><a href="https://www.google.ca/">Google</a></li>
                        <li><a href="https://docs.spring.io/spring/docs/current/spring-framework-reference/">Spring Framework Doc</a></li>
                        <li><a href="http://docs.jboss.org/hibernate/orm/current/userguide/html_single/Hibernate_User_Guide.html">Hibernate ORM User Guide</a></li>
                        <li><a href="https://poi.apache.org/">Apache POI</a></li>
                        <li><a href="https://mvnrepository.com/"> mvnrepository</a></li>
                        <li><a href="https://www.w3schools.com/bootstrap/">Bootstrap 3</a></li>
                        <li><a href="https://www.w3schools.com/icons/default.asp">Icons</a></li>
                        <li><a href="https://freedns.afraid.org/">Free DNS</a></li>
                    </ul> 
                </div>
            </div>
    