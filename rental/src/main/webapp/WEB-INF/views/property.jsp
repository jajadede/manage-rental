<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


    <h3><spring:message code="views.property.message.intro"/></h3>
    <form:form method="POST" modelAttribute="property" class="form-horizontal">
        <form:errors path="address" cssClass="alert alert-danger" element="div"/>
        <hr>
        <h4><spring:message code="views.property.form.label.addressinfo"/></h4>
        <form:hidden name="id" path="address.id"/>
        <div class="form-group ">
            <label class ="col-sm-5 control-label" for="unit"><spring:message code="views.property.form.label.address.unit"/></label>
            <div class ="col-sm-4">
                <form:input type="number" class="form-control" id="unit" placeholder="${property.address.unit}" name="unit" path="address.unit"/>
            </div>
        </div>               
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="number"><spring:message code="views.property.form.label.address.number"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="number" placeholder="${property.address.number}" name="number" path="address.number"/>
            </div>
        </div>   
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="street"><spring:message code="views.property.form.label.address.street"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="street" placeholder="${property.address.street}" name="street" path="address.street"/>
            </div> 
        </div>              
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="city"><spring:message code="views.property.form.label.address.city"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="city" placeholder="${property.address.city}" name="city" path="address.city"/>
            </div>
        </div>
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="province"><spring:message code="views.property.form.label.address.province"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="province" placeholder="${property.address.province}" name="province" path="address.province" />
            </div>  
        </div> 
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="postcode"><spring:message code="views.property.form.label.address.postcode"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="postcode" placeholder="${property.address.postcode}" name="postcode" path="address.postcode"/>
            </div>
        </div>            

        <hr>
        <h4><spring:message code="views.property.form.label.propertyinfo"/></h4>             
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="id"><spring:message code="views.property.form.label.id"/></label>
            <div class ="col-sm-4">
                <form:input type="number" class="form-control" id="id" placeholder="${property.id}" name="id" disabled="true" path="id"/>
            </div>  
        </div>  
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="code"><spring:message code="views.property.form.label.code"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="code" placeholder="${property.code}" name="code" path="code"/><form:errors path="code" cssClass="text-danger"/>
            </div>  
        </div>  
        <div class="form-group">
            <label class ="col-sm-5 control-label" for="description"><spring:message code="views.property.form.label.description"/></label>
            <div class ="col-sm-4">
                <form:input type="text" class="form-control" id="description" placeholder="${property.description}" name="description" path="description"/>
            </div> 
        </div>

        <hr>
        <h4><spring:message code="views.property.form.label.rentinfo"/></h4>
        <c:if test="${property.rents != null}">
            <c:forEach items="${property.rents}" var="rent" varStatus="idx">
                <form:hidden path="rents[${idx.index}].id" />
                <div class="form-group">
                    <label class ="col-sm-3 control-label" for="rent"><spring:message code="views.property.form.label.rent.rent"/></label>
                    <div class ="col-sm-2">
                        <form:input type="number" class="form-control" placeholder="${rent.rent}" name="rent" path="rents[${idx.index}].rent"/><form:errors path="rents[${idx.index}].rent" cssClass="text-danger"/>
                    </div>
                    <label class ="col-sm-3 control-label" for="effectiveDate"><spring:message code="views.property.form.label.rent.effectdate"/></label>
                    <div class ="col-sm-3">
                        <form:input type="date" class="form-control" name="effectiveDate" path="rents[${idx.index}].effectiveDate"/><form:errors path="rents[${idx.index}].effectiveDate" cssClass="text-danger"/>
                    </div> 
                </div>             
            </c:forEach>
        </c:if>
        <hr>
        <div class="form-group">
            <div class="col-sm-offset-5 col-sm-4">
                <button type="submit" class="btn btn-primary"><spring:message code="views.property.form.button.update"/></button>
            </div>
        </div>        
    </form:form>   
