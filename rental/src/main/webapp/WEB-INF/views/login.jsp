<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="container-fluid">
    <br/>
    <table class="table table-bordered">
        <thead class="thead-light ">
            <tr>
                <th><spring:message code="views.login.message.signin"/></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><c:url var="loginUrl" value="/login" />
                    <form action="${loginUrl}" method="post">
                        <c:if test="${param.error != null}">
                            <div class="alert alert-danger"><spring:message code="views.login.message.invalid"/></div>
                        </c:if>
                        <c:if test="${param.logout != null}">
                            <div class="alert alert-success"><spring:message code="views.login.message.logout"/></div>
                        </c:if>
                        <c:if test="${param.accessDenied !=null}">
                            <div class="alert alert-danger"><spring:message code="views.login.message.denied"/></div>
                        </c:if>
                        <div class="form-group">
                            <input type="text" class="form-control" id="userId" name="userId" placeholder="<spring:message code="views.login.input.username"/>" required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="password" name="password" placeholder="<spring:message code="views.login.input.password"/>" required>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block"><spring:message code="views.login.button.login"/></button>
                    </form></td>
            </tr>
        </tbody>
    </table>
    <br/>
</div>
