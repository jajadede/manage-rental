<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />

<div class="container-fluid">
    <c:if test="${deleteError != null}">
        <div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><spring:message code="views.people.delete.error.message"/>${deleteError}</div>
    </c:if>
    <c:if test="${deleteSuccess != null}">
        <div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><spring:message code="views.people.delete.success.message"/></div>
    </c:if>
    <c:if test="${updateError != null}">
        <div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><spring:message code="views.people.update.error.message"/>${updateError}</div>
    </c:if>
    <c:if test="${updateSuccess != null}">
        <div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><spring:message code="views.people.update.success.message"/></div>
    </c:if>  
    <c:if test="${createError != null}">
        <div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><spring:message code="views.people.create.error.message"/>${createError}</div>
    </c:if>
    <c:if test="${createSuccess != null}">
        <div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><spring:message code="views.people.create.success.message"/></div>
    </c:if> 
        <p>
            <input class="form-control" id="filterKey" type="text" placeholder="<spring:message code="views.peoples.input.search"/>">
        </p>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th><spring:message code="views.peoples.table.head.id"/></th>
                <th><spring:message code="views.peoples.table.head.nickname"/></th>
                <th><spring:message code="views.peoples.table.head.cell"/></th>
                <th><spring:message code="views.peoples.table.head.email"/></th>
                <th><spring:message code="views.peoples.table.head.address"/></th>
                <th><spring:message code="views.peoples.table.head.action"/></th>
            </tr>
        </thead>
        <tbody id="tableToSearch">
            <c:forEach items="${peoples}" var="people">
                <tr>
                    <td>
                        <a role="button" class="btn-xs btn-danger" href="${baseURL}/people/delete?id=${people.id}" title="<spring:message code="views.people.form.button.delete"/>" id="deletepeople${people.id}" onclick="return popConfirmationBox('<spring:message code="views.people.form.button.delete.confirm"/>', 'deletepeople${people.id}')" >-</a>
                        &nbsp; 
                        ${people.id}
                    </td>
                    <td><a href="#!" title="" data-toggle="popover" data-content="${people.getFullname()}">${people.nickname}</a></td>
                    <td>${people.cell}</td>
                    <td>${people.email}</td>
                    <td><a href="#!" title="" data-toggle="popover" data-content="${people.address}">${people.address.toShortString()}</a></td>
                    <td>
                        <a role="button" class="btn-sm btn-warning" href="${baseURL}/people/edit?id=${people.id}"><spring:message code="views.people.form.button.update"/></a>                        
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <br>
</div>

<script>
    $(document).ready(function (){
        $("#filterKey").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#tableToSearch tr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
        });
        $('[data-toggle="popover"]').popover(); 
    });
    
/**
* Display a confirmation message box to validate if we want to continue.
*
* @param message String to display
* @param tagId String id of the tag that must display the message.
*
* @return Boolean (confirmation)
*/  
function popConfirmationBox(message, tagId){
    var confirmation = true;
    if (typeof tagId === 'string' && document.activeElement.id.toUpperCase() === tagId.toUpperCase()) {
        if (typeof message === 'string' && message.length > 0) {
            confirmation = window.confirm(message);
        }
    }
    return confirmation;
}    
</script>
