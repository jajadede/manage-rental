<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />

<section class="container-fluid">
    <div class="container-fluid">

        <div class="row container-fluid">
            <div class="col-sm-4 container-fluid">
                <h3>
                    <spring:message code="views.welcome.properties.title"/>
                    &nbsp; 
                    <a role="button" title="<spring:message code="template.navigation.property.drop2"/>" class="btn-sm btn-primary" href="<spring:url value="/property/add"/>" >+</a>
                </h3>
                <p>
                    <spring:message code="views.welcome.properties.num.properties"/>${numProperties}
                    <br><spring:message code="views.welcome.properties.num.address.forProperty"/>${numAddressWithNoPeople}                        
                    <br><spring:message code="views.welcome.properties.num.lonely.address"/>${numLonelyAddress}  
                </p>

            </div>                
            <div class="col-sm-4 container-fluid">
                <h3>
                    <spring:message code="views.welcome.people.title"/>
                    &nbsp; 
                    <a role="button" title="<spring:message code="template.navigation.people.drop2"/>" class="btn-sm btn-primary" href="<spring:url value="/people/add"/>" >+</a>
                </h3>
                <p>
                    <spring:message code="views.welcome.people.num.people"/>${numPeople}
                    <br><spring:message code="views.welcome.properties.num.address.forPeople"/>${numAddressWithNoProperty}
                    <br><spring:message code="views.welcome.properties.num.lonely.address"/>${numLonelyAddress}
                </p>  

            </div>
            <div class="col-sm-4 container-fluid">
                <h3>
                    <spring:message code="views.welcome.lease.title"/>
                    &nbsp; 
                    <a role="button" title="<spring:message code="template.navigation.lease.drop2"/>" class="btn-sm btn-primary" href="<spring:url value="/lease/add"/>" >+</a>
                </h3>
                <p>
                    <spring:message code="views.welcome.lease.num.lease"/>${numLease}
                    <br><spring:message code="views.welcome.properties.num.properties.forNewLease"/>${numPropertyWithNoActiveLease}                        
                    <br><spring:message code="views.welcome.people.num.people.forNewLease"/>${numPeopleWithNoActiveLease}
                </p>  

            </div>
        </div>

        <hr>
        <h3><spring:message code="views.welcome.summary.yearmonth"/></h3>
        <form:form method="POST" modelAttribute="summaryChoice"  class="form-horizontal" action="${baseURL}/summary/findByYearMonth">
            <form:errors path="*" cssClass="alert alert-danger" element="div"/>
            <div class="col-sm-2">
            <form:select path="yearMonth" class="form-control">
                <%--<form:option value=""/>--%>
                <c:if test="${yearMonthList != null && yearMonthList.size()>0}">
                    <c:forEach items="${yearMonthList}" var="item">
                        <form:option value="${item}"/>
                    </c:forEach>
                </c:if>
                
            </form:select>
            </div>
            <button type="submit" class="btn btn-primary" ><spring:message code="views.welcome.summary.yearmonth.submit"/></button>
        </form:form>
        <c:if test="${rentalActivityList != null }">    
            <div class="table-responsive">
            <table class="table">
                <thead>
                <th><spring:message code="views.welcome.summary.yearmonth.table.head.propertyId"/></th>
                <th><spring:message code="views.welcome.summary.yearmonth.table.head.leaseId"/></th>
                <th><spring:message code="views.welcome.summary.yearmonth.table.head.actionDate"/></th>      
                <th><spring:message code="views.welcome.summary.yearmonth.table.head.action"/></th>
                <th><spring:message code="views.welcome.summary.yearmonth.table.head.method"/></th>
                <th><spring:message code="views.welcome.summary.yearmonth.table.head.effectiveDate"/></th> 
                <th><spring:message code="views.welcome.summary.yearmonth.table.head.amount"/></th>
            </thead>
            <tbody>
                <c:forEach items="${rentalActivityList}" var="rentalActivity" varStatus="idx">               
                <tr>
                    <td>${rentalActivity.propertyId}</td>
                    <td>${rentalActivity.leaseId}</td>
                    <td>${rentalActivity.actionDate}</td>
                    <td>${rentalActivity.action.value}</td>
                    <td>${rentalActivity.method.value}</td>
                    <td>${rentalActivity.effectiveDate}</td>
                    <td>${rentalActivity.amount}</td>
                </tr>
                </c:forEach>                
            </tbody>
            </table>                
            </div>
        </c:if>
    </div>
</section>