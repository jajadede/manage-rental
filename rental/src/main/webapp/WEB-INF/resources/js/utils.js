$(document).ready(function () {
    // current page highlight
    if ($('#navigation_tag').html() !== "") {
        var currentpageId = '#' + $('#navigation_tag').html();
        $(currentpageId).addClass('active');
    }    
    // filter bar control
    $("#filterKey").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#tableToSearch tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
        });
    });
});
