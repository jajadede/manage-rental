drop table property if exists;
create table property (
id varchar(25) primary key,
address_id varchar(50),
code varchar(50),
description varchar(250),
rent decimal
);
drop table address if exists;
create table address (
id varchar (25) primary key,
city varchar (50),
number varchar (50),
postcode varchar (7),
province varchar (2),
street varchar (25),
unit varchar (5)
);
