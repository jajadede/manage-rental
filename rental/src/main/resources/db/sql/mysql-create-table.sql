drop table if exists rent;
drop table if exists property;
drop table if exists address;
create table address (
id int not null auto_increment
,version int not null default 0
,city varchar (25)
,number varchar (10)
,postcode varchar (10)
,province varchar (10)
,street varchar (25)
,unit varchar (10)
,primary key (id)
)engine=innodb;
create table property (
id int not null auto_increment
,version int not null default 0
,code varchar(50)
,description varchar(250)
,address_id int not null
,constraint UQ_PROPERTY_ADDRESS unique (address_id, code)
,foreign key FK_PROPERTY_ADRESS (address_id) references address(id)
,primary key (id)
)engine=innodb;
create table rent (
id int not null auto_increment
,version int not null default 0
,property_id int not null
,effective_date date
,rent decimal
,constraint UQ_RENT_PROPERTY unique(property_id, effective_date)
,foreign key FK_RENT_PROPERTY (property_id) references property(id)
,primary key (id)
)engine=innodb;