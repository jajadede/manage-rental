/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.domain.repository.impl;

import com.jajadede.manage.rental.config.TestRepositoryConfig;
import com.jajadede.manage.rental.domain.Address;
import com.jajadede.manage.rental.domain.Gender;
import com.jajadede.manage.rental.domain.People;
import com.jajadede.manage.rental.domain.repository.AddressRepository;
import com.jajadede.manage.rental.domain.repository.PeopleRepository;
import com.jajadede.manage.rental.util.DataSets;
import com.jajadede.manage.rental.util.RepositoryTestExecutionListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author Jerry
 */
//@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestRepositoryConfig.class})
@TestExecutionListeners({RepositoryTestExecutionListener.class})
@ActiveProfiles("test")
public class PeopleRepositoryImplIT extends AbstractTransactionalJUnit4SpringContextTests{
    private static final Logger LOG = LogManager.getLogger(PeopleRepositoryImplIT.class.getName());
    private static final String DATASET_FILE_NAME = "/com/jajadede/manage/rental/domain/repository/impl/PeopleRepositoryImplIT.xls";

    @Autowired
    private PeopleRepository peopleRepository;
    @Autowired
    private AddressRepository addressRepository;    
    
    public PeopleRepositoryImplIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of findAll method, of class PeopleRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)    
    @Test
    public void testFindAll() {
        System.out.println("findAll");
        int expResult = 2;
        int result = peopleRepository.findAll().size();
        assertEquals(expResult, result);
    }

    /**
     * Test of findById method, of class PeopleRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testFindById() {
        System.out.println("findById");
        String expResult = "Kite";
        String result = peopleRepository.findById(2L).getFirstname();
        assertEquals(expResult, result);
    }

    /**
     * Test of create method, of class PeopleRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testCreate() {
        System.out.println("create");
        Address address = new Address();
        address.setNumber("333");
        address.setStreet("Third Ave");
        People people = new People();
        people.setNickname("Cathy");
        people.addAddress(address);
        
        People result = peopleRepository.create(people);        
        assertNotNull(result.getId()); 
        assertEquals("Cathy", result.getNickname());
        assertEquals("Third Ave", result.getAddress().getStreet());
        assertNotNull(result.getAddress().getId());
    }

    /**
     * Test of update method, of class PeopleRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testUpdate() {
        System.out.println("update");
        People people = new People();
        people.setId(1L);
        people.setNickname("Junit");
        people.setGender(Gender.FEMALE);
        people.setAddress(new Address());
        people.getAddress().setId(1L);
        people.getAddress().setCity("Test City");
        peopleRepository.update(people);
        people = peopleRepository.findById(people.getId());
        assertEquals(2, peopleRepository.findAll().size());
        assertNotNull(people.getAddress());
        assertEquals("Junit", people.getNickname());
        assertEquals(2, addressRepository.findAll().size());
        assertEquals("Test City", people.getAddress().getCity());
    }

    /**
     * Test of delete method, of class PeopleRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testDelete() {
        System.out.println("delete");
        People people = new People();
        people.setId(1L);        
        peopleRepository.delete(people);
        assertEquals(null, peopleRepository.findById(1L));
        assertEquals(1L, peopleRepository.countAll().longValue());
        assertEquals(2, addressRepository.findAll().size());
    }

    /**
     * Test of countAll method, of class PeopleRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testCountAll() {
        System.out.println("countAll");
        Long expResult = 2L;
        Long result = peopleRepository.countAll();
        assertEquals(expResult, result);
    }
    
}
