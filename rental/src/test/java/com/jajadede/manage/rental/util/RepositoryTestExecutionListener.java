/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.util;

import org.dbunit.IDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.util.fileloader.XlsDataFileLoader;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.TestExecutionListener;

/**
 *
 * @author Jerry
 */
public class RepositoryTestExecutionListener  implements TestExecutionListener {
    private IDatabaseTester databaseTester;

    @Override
    public void beforeTestClass(TestContext testContext) throws Exception {
//        TestExecutionListener.super.beforeTestClass(testContext); 
    }

    @Override
    public void prepareTestInstance(TestContext testContext) throws Exception {
//        TestExecutionListener.super.prepareTestInstance(testContext);
    }

    @Override
    public void beforeTestMethod(TestContext testContext) throws Exception {
        DataSets dataSetAnnotation = testContext.getTestMethod().getAnnotation(DataSets.class);
        if (dataSetAnnotation != null) {
            String dataSetName = dataSetAnnotation.value();
            if (!dataSetName.isEmpty()) {
                ApplicationContext ctx = testContext.getApplicationContext();
                databaseTester = (IDatabaseTester) ctx.getBean("databaseTester");
                XlsDataFileLoader loader = (XlsDataFileLoader) ctx.getBean("xlsDataFileLoader");
                IDataSet dataset = loader.load(dataSetName);
                databaseTester.setDataSet(dataset);
                databaseTester.onSetup();
            }
        }        
    }

    @Override
    public void beforeTestExecution(TestContext testContext) throws Exception {
//        TestExecutionListener.super.beforeTestExecution(testContext); 
    }

    @Override
    public void afterTestExecution(TestContext testContext) throws Exception {
//        TestExecutionListener.super.afterTestExecution(testContext);
    }

    @Override
    public void afterTestMethod(TestContext testContext) throws Exception {
        if (databaseTester != null) {
            databaseTester.onTearDown();
        }            
    }

    @Override
    public void afterTestClass(TestContext testContext) throws Exception {
//        TestExecutionListener.super.afterTestClass(testContext); 
    }
    
    
}
