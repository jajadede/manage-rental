/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.domain;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Jerry
 */
//@Ignore
public class LeaseTest {
    
    public LeaseTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of prepareSetsBeforeSave method, of class Lease.
     */
    @Test
    public void testPrepareSetsBeforeSave() {
        System.out.println("prepareSetsBeforeSave");
        Lease instance = new Lease();
        instance.prepareSetsBeforeSave();
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of prepareListsAfterLoad method, of class Lease.
     */
    @Test
    public void testPrepareListsAfterLoad() {
        System.out.println("prepareListsAfterLoad");
        Lease instance = new Lease();
        instance.prepareListsAfterLoad();
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getCurrentrent method, of class Lease.
     */
    @Test
    public void testGetCurrentrent() {
        System.out.println("getCurrentrent");
        Lease instance = new Lease();
        BigDecimal expResult = null;
        BigDecimal result = instance.getCurrentrent();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of setCurrentrent method, of class Lease.
     */
    @Test
    public void testSetCurrentrent() {
        System.out.println("setCurrentrent");
        BigDecimal currentrent = null;
        Lease instance = new Lease();
        instance.setCurrentrent(currentrent);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of toTermString method, of class Lease.
     */
    @Test
    public void testToTermString() throws ParseException {
        System.out.println("toTermString");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Lease instance = new Lease();
        instance.setEffectivedate(format.parse("2018-09-01"));
        instance.setEnddate(format.parse("2018-12-31"));
        Event e = new Event();
        e.setAction(Action.CANCEL_RESERVATION);
        instance.addEvent(e);
        e.setActiondate(format.parse("2018-07-01"));
        assertEquals("0/4", instance.toTermString());        
        e.setActiondate(format.parse("2018-08-01"));
        assertEquals("0/4", instance.toTermString());         
        e.setActiondate(format.parse("2018-08-31"));
        assertEquals("0/4", instance.toTermString());        
        e.setActiondate(format.parse("2018-09-01"));
        assertEquals("1/4", instance.toTermString());         
        e.setActiondate(format.parse("2018-09-30"));
        assertEquals("1/4", instance.toTermString());
        e.setActiondate(format.parse("2018-10-01"));
        assertEquals("2/4", instance.toTermString());
        e.setActiondate(format.parse("2018-12-01"));
        assertEquals("4/4", instance.toTermString());  
        e.setActiondate(format.parse("2018-12-31"));
        assertEquals("4/4", instance.toTermString());    
        e.setActiondate(format.parse("2019-01-01"));
        assertEquals("5/4", instance.toTermString());  
        instance.removeEvent(e);
        assertEquals("0/4", instance.toTermString());          
    }
    
}
