/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.domain.repository.impl;

import com.jajadede.manage.rental.config.TestRepositoryConfig;
import com.jajadede.manage.rental.domain.Address;
import com.jajadede.manage.rental.domain.repository.AddressRepository;
import com.jajadede.manage.rental.util.DataSets;
import com.jajadede.manage.rental.util.RepositoryTestExecutionListener;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author Jerry
 */
//@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestRepositoryConfig.class})
@TestExecutionListeners({RepositoryTestExecutionListener.class})
@ActiveProfiles("test")
public class AddressRepositoryImplIT extends AbstractTransactionalJUnit4SpringContextTests{
    
    private static final Logger LOG = LogManager.getLogger(AddressRepositoryImplIT.class.getName());       
    private static final String DATASET_FILE_NAME = "/com/jajadede/manage/rental/domain/repository/impl/AddressRepositoryImplIT.xls";

    @Autowired
    private AddressRepository addressRepository;
        
    public AddressRepositoryImplIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {              
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    /**
     * Test of create method, of class AddressRepositoryImpl.
     * testing with following two entries for each 'test' case
     * 100 Fairy Road, Kitchener, ON, H0H0H0 with id=10
     * 200 University Street, Water, ON, I0I0I0 with id=11
     */
    @DataSets(DATASET_FILE_NAME)
    @Test    
    public void testCreate() {
        LOG.info("create...");
        Address address = new Address();
        address.setCity("city");
        address.setStreet("street");
        address.setNumber("300");
        address.setUnit("24");
        address.setProvince("ON");
        address.setPostcode("H0H0H0");
        Address address2 = new Address();        
        addressRepository.create(address);
        addressRepository.create(address2); // test create with new class
        Long expected = 13L;
        Long actual = address2.getId();
        assertEquals(expected, actual);
    }    
    
    /**
     * Test of update method, of class AddressRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testUpdate() {
        LOG.info("update...");
        Address address = addressRepository.findById(10L);
        address.setCity("city2");
//        List<Property> properties = new ArrayList();
//        Property p1 = new Property();
//        Property p2 = new Property();
//        properties.add(p1);
//        properties.add(p2);
//        address.setProperties(properties);
        address.setId(10L);    
        addressRepository.update(address);     
//        sessionFactory.getCurrentSession().flush();
        Object expected = "city2";
        Object actual = addressRepository.findById(10L).getCity();
        assertEquals(expected, actual);
    }   
    
    /**
     * Test of findById method, of class AddressRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testFindById() {
        LOG.info("findById...");   
        Long id = 10L;        
        Address found = addressRepository.findById(id);
        String expResult = "H0H0H0";
        String result = found.getPostcode();
        assertEquals(expResult, result);        
    }
    
    /**
     * Test of findAll method, of class AddressRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testFindAll() { 
        LOG.info("findAll...");
        List<Address> found = addressRepository.findAll();
        for (Address a : found) {
            LOG.info("address: " + a + " with id=" + a.getId());
        }
        Integer expResult = 2;
        Integer result = found.size();
        assertEquals(expResult, result);        
    }
    
    /**
     * Test of delete method, of class AddressRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testDelete() {
        LOG.info("delete...");        
        Address address = new Address();
        address.setId(10L);
        addressRepository.delete(address);
        Integer result = addressRepository.findAll().size();
        Integer expResult = 1;
        assertEquals(expResult, result);        
    }    
}
