/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.config;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import javax.sql.DataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dbunit.DataSourceDatabaseTester;
import org.dbunit.util.fileloader.XlsDataFileLoader;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;

/**
 *
 * @author Jerry
 */
@Configuration
@ComponentScan(basePackages = { "com.jajadede.manage.rental" }
//        ,excludeFilters = @ComponentScan.Filter(type = FilterType.REGEX, pattern = "com.jajadede.manage.rental.config.*")
)
//@EnableJpaRepositories(basePackages = {"com.jajadede.manage.rental.repository"})
@EnableTransactionManagement
@Profile("test")
public class TestRepositoryConfig implements TransactionManagementConfigurer {
    private static final Logger LOG = LogManager.getLogger(TestRepositoryConfig.class.getName());
    private static boolean USE_MEMORY_DB = true;
    
    @Bean (name = "databaseTester")
    public DataSourceDatabaseTester dataSourceDatabaseTester() {
        return new DataSourceDatabaseTester(dataSource());
    }    
    
    @Bean (name = "xlsDataFileLoader")
    public static XlsDataFileLoader xlsDataFileLoader() {
        return new XlsDataFileLoader();
    }
    
    @Bean
    public DataSource dataSource() {
        if (!USE_MEMORY_DB) return dataSource2();
        try {
            EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
            EmbeddedDatabase db = builder
                            .setType(EmbeddedDatabaseType.H2)
//                            .addScript("db/sql/create-table.sql")
//                            .addScript("db/sql/insert-data.sql")
                            .build();
            LOG.debug("dataSource bean is up " + db);
            return db;
        } catch (Exception e) {
            LOG.error("Failed to create datasource bean", e);
        }

        return null;
    }
    
//    @Bean
//    public JpaVendorAdapter japVendorAdapter() {
//        return new HibernateJpaVendorAdapter();
//}
    
        /// Hibernate config start
    @Bean
    public SessionFactory sessionFactory() throws IOException  {
        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
        sessionFactoryBean.setDataSource(dataSource()); 
        sessionFactoryBean.setPackagesToScan("com.jajadede.manage.rental.domain");
        sessionFactoryBean.setHibernateProperties(hibernateProperties());
        sessionFactoryBean.afterPropertiesSet();
        SessionFactory sf = sessionFactoryBean.getObject();
        return sf;        
    }
    
    
//    @Bean
//    public SessionFactory sessionFactory() throws IOException  {
//        SessionFactory sessionFactory = entityManagerFactory().unwrap(SessionFactory.class);
//        return sessionFactory;
//    }
 
//    @Bean
//    private EntityManagerFactory entityManagerFactory() {
//        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
//        factoryBean.setPackagesToScan("com.jajadede.manage.rental.domain");
//        factoryBean.setDataSource(dataSource());
//        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
//        factoryBean.setJpaProperties(hibernateProperties());
//        factoryBean.afterPropertiesSet();        
//        return factoryBean.getNativeEntityManagerFactory();
//    }
    
//    @Bean
//    private PlatformTransactionManager platformTransactionManager() throws IOException {
//        return new JpaTransactionManager(entityManagerFactory());
//    }
    
    @Bean
    public HibernateTransactionManager hibernateTransactionManager() throws IOException {
        return new HibernateTransactionManager(sessionFactory());
    }
    
    private Properties hibernateProperties(){
        if (!USE_MEMORY_DB) return hibernateProperties2();
        Properties hibernateProperties = new Properties();
        hibernateProperties.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        hibernateProperties.put("hibernate.hbm2ddl.auto", "create-drop");
        hibernateProperties.put("hibernate.show_sql", true);
        hibernateProperties.put("hibernate.max_fetch_depth", 3);
        hibernateProperties.put("hibernate.jdbc.batch_size", 10);
        hibernateProperties.put("hibernate.jdbc.fetch_size", 50);
        hibernateProperties.put("hibernate.current_session_context_class", "org.hibernate.context.internal.ThreadLocalSessionContext");
        return hibernateProperties;
    }    

    @Override
    public PlatformTransactionManager annotationDrivenTransactionManager() {
        try {
            return hibernateTransactionManager();
        } catch (IOException ex) {
            LOG.error(ex);
        }
        return null;
    }



    /// the real db incase it is needed
    private Properties hibernateProperties2(){
        Properties hibernateProperties = new Properties();
        hibernateProperties.put("hibernate.dialect", "org.hibernate.dialect.MySQL8Dialect"); 
        hibernateProperties.put("hibernate.hbm2ddl.auto", "create");
        hibernateProperties.put("hibernate.show_sql", true);
        hibernateProperties.put("hibernate.format_sql", true);
        hibernateProperties.put("hhibernate.use_sql_comments", true);
        hibernateProperties.put("hibernate.id.new_generator_mappings", false);
        hibernateProperties.put("hibernate.max_fetch_depth", 3);
        hibernateProperties.put("hibernate.jdbc.batch_size", 10);
        hibernateProperties.put("hibernate.jdbc.fetch_size", 50);
        hibernateProperties.put("hibernate.current_session_context_class", "org.hibernate.context.internal.ThreadLocalSessionContext");
        hibernateProperties.put("jdbc.driverClassName", "com.mysql.cj.jdbc.Driver");
        hibernateProperties.put("jdbc.url", "jdbc:mysql://localhost:3306/db_rental_test?useUnicode=yes&autoReconnect=true&useSSL=false");
        hibernateProperties.put("jdbc.username", "rentaluser");
        hibernateProperties.put("jdbc.password", "rentalpass");
        return hibernateProperties;
    }
    
    private DataSource dataSource2() {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setUrl(hibernateProperties2().getProperty("jdbc.url"));
        ds.setDriverClassName(hibernateProperties2().getProperty("jdbc.driverClassName"));
        ds.setUsername(hibernateProperties2().getProperty("jdbc.username"));
        ds.setPassword(hibernateProperties2().getProperty("jdbc.password"));
        
        // TODO remove the auto db refresh after test
//        Connection connection = null;
//        try {
//            connection = ds.getConnection();
//            ScriptUtils.executeSqlScript(connection, new EncodedResource(new ClassPathResource("db/sql/mysql-create-table.sql"), StandardCharsets.UTF_8));
//            ScriptUtils.executeSqlScript(connection, new EncodedResource(new ClassPathResource("db/sql/mysql-insert-data.sql"), StandardCharsets.UTF_8));
//        } catch (SQLException ex) {
//            LOG.error(ex);
//        } finally {
//            if (connection != null) try {
//                connection.close();
//            } catch (SQLException ex) {
//                LOG.error(ex);
//            }
//        } 
        return ds;
    }    
}