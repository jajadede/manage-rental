/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.domain.repository.impl;

import com.jajadede.manage.rental.config.TestRepositoryConfig;
import com.jajadede.manage.rental.domain.repository.SummaryRepository;
import com.jajadede.manage.rental.util.DataSets;
import com.jajadede.manage.rental.util.HibernateUtil;
import com.jajadede.manage.rental.util.RepositoryTestExecutionListener;
import com.jajadede.manage.rental.wrapper.RentalActivity;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author Jerry
 */
//@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestRepositoryConfig.class})
@TestExecutionListeners({RepositoryTestExecutionListener.class})
@ActiveProfiles("test")
public class SummaryRepositoryImplIT extends AbstractTransactionalJUnit4SpringContextTests{
    private static final Logger LOG = LogManager.getLogger(SummaryRepositoryImplIT.class.getName());
    
    private static final String DATASET_FILE_NAME = "/com/jajadede/manage/rental/domain/repository/impl/SummaryRepositoryImplIT.xls";

    @Autowired
    private SummaryRepository summaryRepository;
    
    @Autowired
    private SessionFactory sessionFactory;
    
    public SummaryRepositoryImplIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of findByYearMonth method, of class SummaryRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testFindByYearMonth() {
        System.out.println("findByYearMonth");
        Integer year = 2018;
        Integer month = 1;        
        List<RentalActivity> rentalSummary = summaryRepository.findByYearMonth(year, month);
        LOG.debug("summary: \n" + rentalSummary.stream().map(RentalActivity::toString).collect(Collectors.joining("\n")));
        assertEquals(4, rentalSummary.size());
    }

    /**
     * Test of findYearMonthList method, of class SummaryRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testFindYearMonthList() {
        System.out.println("findYearMonthList");
        int withinYears = 1;
        Calendar c = Calendar.getInstance();
        c.add(Calendar.YEAR, -withinYears);
        Date limit = c.getTime();
        LOG.debug("Limit withinYears: " + withinYears + ", limit="+ limit);
        List<String> summary = summaryRepository.findYearMonthList(withinYears, true);
        LOG.debug("summary: \n" + 
                summary.stream().map(Object::toString).collect(Collectors.joining("\n"))  
        ); 
        assertEquals(12, summary.size());
    }

//    @DataSets(DATASET_FILE_NAME)
//    @Test
    public void testGetRentalSummaryForYearMonth()  {
        Integer year = 2017;
        Integer month = 1;
        List<Object[]> summary = (List<Object[]>) HibernateUtil.doWithinTransaction(sessionFactory, session -> {
            List<Object []> result = session.createQuery(""
                    + "select p.id, l.id, e.action, e.actiondate, t.purpose, t.effectivedate, t.amount "
                    + "from Lease l "
                    + "join l.property p "
                    + "join l.tenantsSet lt "
                    + "join l.eventsSet e "
                    + "join e.transaction t "
                    + "where year(t.effectivedate)=:year and month(t.effectivedate)=:month")
                    .setParameter("year", year)
                    .setParameter("month", month)
                    .list();
            return result;
        });
        LOG.debug("summary: \n" + 
                summary.stream().
                        map(array -> {return Arrays.asList(array).stream().map(Object::toString).collect(Collectors.joining(","));}).
                        collect(Collectors.joining("\n"))  
        );
    }
    
}
