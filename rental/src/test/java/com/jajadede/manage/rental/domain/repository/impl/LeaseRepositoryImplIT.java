/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.domain.repository.impl;

import com.jajadede.manage.rental.config.TestRepositoryConfig;
import com.jajadede.manage.rental.domain.Action;
import com.jajadede.manage.rental.domain.Event;
import com.jajadede.manage.rental.domain.Lease;
import com.jajadede.manage.rental.domain.LeaseStatus;
import com.jajadede.manage.rental.domain.People;
import com.jajadede.manage.rental.domain.Property;
import com.jajadede.manage.rental.domain.Transaction;
import com.jajadede.manage.rental.domain.TransactionMethod;
import com.jajadede.manage.rental.domain.repository.LeaseRepository;
import com.jajadede.manage.rental.util.DataSets;
import com.jajadede.manage.rental.util.RepositoryTestExecutionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author Jerry
 */
//@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestRepositoryConfig.class})
@TestExecutionListeners({RepositoryTestExecutionListener.class})
@ActiveProfiles("test")
public class LeaseRepositoryImplIT extends AbstractTransactionalJUnit4SpringContextTests{
    private static final Logger LOG = LogManager.getLogger(LeaseRepositoryImplIT.class.getName());
    private static final String DATASET_FILE_NAME = "/com/jajadede/manage/rental/domain/repository/impl/LeaseRepositoryImplIT.xls";
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    private LeaseRepository instance;
    
    public LeaseRepositoryImplIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of findAll method, of class LeaseRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)    
    @Test
    public void testFindAll() {
        System.out.println("findAll");
        List<Lease> result = instance.findAll();
        assertEquals(2, result.size());
    }

    /**
     * Test of findById method, of class LeaseRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testFindById() {
        System.out.println("findById");
        Lease result = instance.findById(1L);
        assertEquals("1,2", result.getEvents().stream().map(e -> {return e.getId().toString();}).collect(Collectors.joining(",")) );
        result = instance.findById(2L);
        assertEquals("3,4,5", result.getEvents().stream().map(e -> {return e.getId().toString();}).collect(Collectors.joining(",")) );
    }

    /**
     * Test of create method, of class LeaseRepositoryImpl.
     */
    @Test
    public void testCreate() {
        System.out.println("create");
        Lease lease = new Lease();
        Property property = new Property();
        property.setId(1L);
        People people = new People();
        people.setId(1L);
        Event event = new Event();
        event.addTransaction(new Transaction());
        lease.setProperty(property);
        lease.getTenants().add(people);        
        lease.addEvent(event);
        Lease result = instance.create(lease);
        assertNotNull(result.getId());
    }

    /**
     * Test of update method, of class LeaseRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testUpdate() {
        System.out.println("update");
        Date today = new Date();
        Lease lease = instance.findById(1L);
        lease.setStatus(LeaseStatus.ACTIVE);
        lease.setEnddate(today);
        instance.update(lease);
        lease = instance.findById(1L);
        assertEquals(LeaseStatus.ACTIVE, lease.getStatus());
        assertEquals(sdf.format(today), lease.getEnddate().toString());
    }

    /**
     * Test of delete method, of class LeaseRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testDelete() {
        System.out.println("delete");
        Lease lease = new Lease();
        lease.setId(1L);        
        instance.delete(lease);
        assertEquals(1L, instance.countAll().longValue());
    }

    /**
     * Test of countAll method, of class LeaseRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testCountAll() {
        System.out.println("countAll");
        Long result = instance.countAll();
        assertEquals(2L, result.longValue());
    }

    /**
     * Test of deleteEventById method, of class LeaseRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testDeleteEventById() {
        System.out.println("deleteEventById");
        instance.deleteEventById(1L);
        assertEquals(1, instance.findById(1L).getEvents().size());
    }

    /**
     * Test of addEvent method, of class LeaseRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testAddEvent() {
        System.out.println("addEvent");
        Date today = new Date();
        Event event = new Event();
        event.addTransaction(new Transaction());
        event.setAction(Action.PAY_RENT);
        event.setActiondate(today);
        event.getTransaction().setMethod(TransactionMethod.CASH);
        Lease lease = new Lease();
        lease.setId(1L);
        lease.addEvent(event);
        instance.addEvent(event);
        lease = instance.findById(1L);
        assertEquals(3, lease.getEvents().size());
        assertNotNull(lease.getEvents().get(2).getId());
        assertEquals(Action.PAY_RENT, lease.getEvents().get(2).getAction());
        assertEquals(sdf.format(today), lease.getEvents().get(2).getActiondate().toString());
        assertEquals(TransactionMethod.CASH, lease.getEvents().get(2).getTransaction().getMethod());        
    }

    /**
     * Test of deleteEvent method, of class LeaseRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testDeleteEvent() {
        System.out.println("deleteEvent");
        Event event = new Event();
        event.setId(1L);
        Lease lease = new Lease();
        lease.setStatus(LeaseStatus.ACTIVE);
        lease.addEvent(event);
        instance.deleteEvent(event);
        Lease leasedb = instance.findById(1L);
        assertEquals(1, leasedb.getEvents().size());
        assertEquals(LeaseStatus.ACTIVE, leasedb.getStatus());
    }

    /**
     * Test of findEventById method, of class LeaseRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testFindEventById() {
        System.out.println("findEventById");
        Event result = instance.findEventById(1L);
        assertEquals(Action.RESERVE, result.getAction());
        assertEquals("2016-12-15", result.getActiondate().toString());
        assertEquals(1L, result.getLease().getId().longValue());
    }
    
}
