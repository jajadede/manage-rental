/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jajadede.manage.rental.domain.repository.impl;

import com.jajadede.manage.rental.config.TestRepositoryConfig;
import com.jajadede.manage.rental.domain.Address;
import com.jajadede.manage.rental.domain.Property;
import com.jajadede.manage.rental.domain.repository.PropertyRepository;
import com.jajadede.manage.rental.util.DataSets;
import com.jajadede.manage.rental.util.RepositoryTestExecutionListener;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author Jerry
 */
//@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestRepositoryConfig.class})
@TestExecutionListeners({RepositoryTestExecutionListener.class})
@ActiveProfiles("test")
public class PropertyRepositoryImplIT extends AbstractTransactionalJUnit4SpringContextTests{
    private static final Logger LOG = LogManager.getLogger(PropertyRepositoryImplIT.class.getName());
    
    private static final String DATASET_FILE_NAME = "/com/jajadede/manage/rental/domain/repository/impl/PropertyRepositoryImplIT.xls";
    
    @Autowired
    private PropertyRepository instance;
    
    public PropertyRepositoryImplIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of findAll method, of class PropertyRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testFindAll() {
        System.out.println("findAll");
        List<Property> result = instance.findAll();
        assertEquals(2, result.size());
    }

    /**
     * Test of findById method, of class PropertyRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testFindById() {
        System.out.println("findById");
        Property result = instance.findById(1L);
        assertNotNull(result);
        assertEquals("111", result.getCode());
        assertEquals("desc111",result.getDescription());
        assertEquals(1L,result.getAddress().getId().longValue());
    }

    /**
     * Test of create method, of class PropertyRepositoryImpl.
     */
    @Test
    public void testCreate() {
        System.out.println("create");
        Address a = new Address();
        a.setId(2L);
        Property property = new Property();
        property.setAddress(a);
        property.setCode("333");
        property.setDescription("desc333");
        Property result = instance.create(property);
        assertNotNull(result.getId());
        assertEquals("333", result.getCode());
        assertEquals("desc333", result.getDescription());
    }

    /**
     * Test of update method, of class PropertyRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testUpdate() {
        System.out.println("update");
        Property property = instance.findById(1L);
        property.setCode("888");
        property.setDescription("desc888");
        property.getAddress().setStreet("Eighth Street");
        Long addressId = property.getAddress().getId();        
        instance.update(property);
        property = instance.findById(1L);
        assertEquals(addressId, property.getAddress().getId());
        assertEquals("Eighth Street", property.getAddress().getStreet());
        assertEquals("888", property.getCode());
        assertEquals("desc888", property.getDescription());        
    }

    /**
     * Test of delete method, of class PropertyRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testDelete() {
        System.out.println("delete");
        Property property = new Property();
        property.setId(2L);
        instance.delete(property);
        property = instance.findById(2L);
        assertNull(property);
        assertEquals(1L, instance.countAll().longValue());        
    }

    /**
     * Test of countAll method, of class PropertyRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testCountAll() {
        System.out.println("countAll");
        Long result = instance.countAll();
        assertEquals(2L, result.longValue());
    }

    /**
     * Test of findAllWithNoActiveLease method, of class PropertyRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testFindAllWithNoActiveLease() {
        System.out.println("findAllWithNoActiveLease");
        List<Property> result = instance.findAllWithNoActiveLease();
        assertEquals(1, result.size());
    }

    /**
     * Test of countAllWithNoActiveLease method, of class PropertyRepositoryImpl.
     */
    @DataSets(DATASET_FILE_NAME)
    @Test
    public void testCountAllWithNoActiveLease() {
        System.out.println("countAllWithNoActiveLease");
        Long result = instance.countAllWithNoActiveLease();
        assertEquals(1L, result.longValue());
    }
    
}
